<?php

namespace Magecomp\Ordercomment\Observer;

use Magecomp\Ordercomment\Helper\Data\Ordercomment;

class AddOrdercommentToOrder implements \Magento\Framework\Event\ObserverInterface
{
    public function execute( \Magento\Framework\Event\Observer $observer )
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $observer->getEvent()->getOrder();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');
		 
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/shippingcomment.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        $order = $observer->getEvent()->getOrder();

        $quote = $observer->getEvent()->getQuote();
        if($quote->getData(Ordercomment::COMMENT_FIELD_NAME)){
            $logger->info('Customer order '.$quote->getId().' comment Magecomp\Ordercomment\Observer: Comment - '.$quote->getData(Ordercomment::COMMENT_FIELD_NAME));
            $order->setData(Ordercomment::COMMENT_FIELD_NAME, $quote->getData(Ordercomment::COMMENT_FIELD_NAME));
            //$order->setCustomerNote($quote->getData(Ordercomment::COMMENT_FIELD_NAME));
            $order->addStatusHistoryComment($quote->getData(Ordercomment::COMMENT_FIELD_NAME));

            if($isCheckoutLoggerEnabled) {
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $logger->info('Customer quote id '.$quote->getId().' comment Magecomp\Ordercomment\Observer: Comment - '.$quote->getData(Ordercomment::COMMENT_FIELD_NAME));
            }
        }
    }
}
