<?php

namespace Anshu\SCdata\Block\ConfigurableProduct\Product\View\Type;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;

class Configurable
{
    protected $jsonEncoder;
    protected $jsonDecoder;
    protected $_productRepository;
    //protected $_attributes;
    protected $stockRegistry;
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	    //\Magento\Catalog\Block\Product\View\Attributes $attributes,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder
    ) {
        $this->jsonDecoder = $jsonDecoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->_productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        
	//$this->_attributes = $attributes;
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function aroundGetJsonConfig(
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
        \Closure $proceed
    )
    {

        $sname = [];
        $sdescription = [];
        $ssku = [];
        $sadditionaldata = [];
        $sshortdescription = [];
        $measurementsold=[];
        $finalprice=[];
        $surl = [];
        $sqty =[];
        $sminqty=[];
        $sleadtime = [];
        $backorderleadtime=[];
        $measize = [];
        $config = $proceed();
        $config = $this->jsonDecoder->decode($config);

        foreach ($subject->getAllowProducts() as $prod) {
            $id = $prod->getId();
            $product = $this->getProductById($id);
            $sname[$id] = $product->getName();
            $sdescription[$id] = $product->getDescription();
            $sshortdescription[$id] = $product->getShortDescription();
            $surl[$id] = $product->getProductUrl();
            $ssku[$id] = $product->getSku();
            $stockItem = $product->getExtensionAttributes()->getStockItem();
            $sqty[$id] = $stockItem->getQty();
            
            $sminqty[$id] = $stockItem->getMinSaleQty();

            $sleadtime[$id] = $product->getBackorderLeadTime();
            $measurementsold[$id] = $product->getBaseUnit();
            $finalprice[$id] = $product->getFinalPrice();
            $measize[$id] = $product->getMeasurementSoldInSize();
            $backorderleadtime[$id] = $product->getBackorderLeadTime()?$product->getBackorderLeadTime():'';
//	    foreach($this->_attributes->getAdditionalData() as $attribute) {
//				if(($attribute->getIsVisible() && $attribute->getIsVisibleOnFront()) ) {
//					$code = $attribute->getAttributeCode();
//					$value = (string)$attribute->getFrontend()->getValue($product);
//					$sadditionaldata[$id] = [
//						'value' => $value
//					];
//				}
//        	}
//	    $sadditionaldata[$id] = $sadditionaldata;
        }
        $config['sname'] = $sname;
        $config['sdescription'] = $sdescription;
        $config['ssku'] = $ssku;
        $config['sshortdescription'] = $sshortdescription;
        $config['surl'] = $surl;
        $config['sqty'] = $sqty;
        $config['sminqty'] = $sminqty;
        $config['sleadtime'] = $sleadtime;
        $config['measurementsold'] = $measurementsold;
        $config['finalprice'] = $finalprice;
        $config['measize'] = $measize;
        $config['backorderleadtime'] = $backorderleadtime;
//	$config['sadditionaldata'] = $sadditionaldata;
        
        return $this->jsonEncoder->encode($config);
    }
}
