define([
    'jquery',
    'jquery/ui',
    'magento-swatch.renderer'
], function ($) {

    $.widget('anshu.SwatchRenderer', $.mage.SwatchRenderer, {
        /**
         * Event for swatch options
         *
         * @param {Object} $this
         * @param {Object} $widget
         * @private
         */
        _OnClick: function ($this, $widget) {
            var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                    $wrapper = $this.parents('.' + $widget.options.classes.attributeOptionsWrapper),
                    $label = $parent.find('.' + $widget.options.classes.attributeSelectedOptionLabelClass),
                    attributeId = $parent.attr('attribute-id'),
                    $input = $parent.find('.' + $widget.options.classes.attributeInput);

            if ($widget.inProductList) {
                $input = $widget.productForm.find(
                        '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                        );
            }

            if ($this.hasClass('disabled')) {
                return;
            }

            if ($this.hasClass('selected')) {
//                $parent.removeAttr('option-selected').find('.selected').removeClass('selected');
//                $input.val('');
//                $label.text('');
//                $this.attr('aria-checked', false);
            } else {
                $parent.attr('option-selected', $this.attr('option-id')).find('.selected').removeClass('selected');
                $label.text($this.attr('option-label'));
                $input.val($this.attr('option-id'));
                $input.attr('data-attr-name', this._getAttributeCodeById(attributeId));
                $this.addClass('selected');
                var flag = 0;
                $('.product-options-wrapper .swatch-attribute').each(function () {
                    var attr = $(this).attr('option-selected');
                    if (typeof attr == "undefined")
                    {
                        flag = 1;
                    }
                });
                $widget._toggleCheckedAttributes($this, $wrapper);
                $widget._Rebuild();
                if (flag == 0)
                {
                    // Custom Code starts
                    var iname = $widget.options.jsonConfig.sname[this.getProduct()];
                    var idescription = $widget.options.jsonConfig.sdescription[this.getProduct()];
                    var isku = $widget.options.jsonConfig.ssku[this.getProduct()];
                    var ishortdescription = $widget.options.jsonConfig.sshortdescription[this.getProduct()];
                    //var iadditionalinfo = $widget.options.jsonConfig.sadditionaldata[this.getProduct()];
                    var iurl = $widget.options.jsonConfig.surl[this.getProduct()];
                    var iqty = $widget.options.jsonConfig.sqty[this.getProduct()];
                    var ileadtime = $widget.options.jsonConfig.sleadtime[this.getProduct()];
                    var measurementsold = $widget.options.jsonConfig.measurementsold[this.getProduct()];
                    var finalprice = $widget.options.jsonConfig.finalprice[this.getProduct()];
                    var iminqty = $widget.options.jsonConfig.sminqty[this.getProduct()];
                    var measize = $widget.options.jsonConfig.measize[this.getProduct()];
                    var backorderleadtime = $widget.options.jsonConfig.backorderleadtime[this.getProduct()];
                    iqty = iqty / measize;
                    iqty = Math.floor(iqty);
//                     console.log(iminqty);
//            alert(iadditionalinfo);
                    $('.price-final_price .price-wrapper').attr('finalprice', finalprice);
                    if (idescription != '') {
                        $('[data-role="content"]').find('.description .value').html(idescription);
                    }
                    if (ishortdescription != '') {
                        $('.product-info-main').find('.overview .value').html(ishortdescription);
                    }
                    if (iname != '') {
                        $('[data-ui-id="page-title-wrapper"]').html(iname);
                    }
                    if(backorderleadtime!=''){
                        $('#backorder_lead_time').html(backorderleadtime);
                    } 
                    if (iminqty != '') {
                        if ($("input[type=number][name=qty].qty").val() > iminqty) {
                            $("input[type=number][name=qty].qty").val($("input[type=number][name=qty].qty").val());
                        } else {
                            $("input[type=number][name=qty].qty").val(iminqty);
                            $("input[type=number][name=qty].qty").attr('value', iminqty);
                            $("input[type=number][name=qty].qty").attr('min', iminqty);
                        }
                    }
                    //$('div.product-info-main .sku .value').html(isku);
                    $('div.product-info-main .sku .value').html('<a href="' + iurl + '">' + isku + '</a>');
                    var flag = 0;
                    $('.swatch-attribute').each(function () {
                        var attr = $(this).attr('option-selected');
                        if (typeof attr == "undefined")
                        {
                            flag = 1;
                        }
                    });
                    if (flag == 0) {
//                        $('.available-qty .availableqty').css('display', 'none');
//                        $('.available-qty .notavailableqty').css('display', 'none');
//                        $('.available-qty .limitavailableqty').css('display', 'none');
//                        if (ileadtime == '') {
//                            $('.available-qty .availableqty').css('display', 'block');
//                        } else if (iqty <= 0) {
//                            $('.available-qty .notavailableqty').css('display', 'block');
//                            $('.available-qty .notavailableqty span').text(ileadtime);
////                        } else if (iqty > 0 && iqty < 100) {
//                        } else {
//                            if (iname.indexOf('Wide Loop') != -1) {
//                                measurementsold = "yard";
//                            } else if (iname.indexOf('Straps') != -1 || iname.indexOf('Cable Ties') != -1 || iname.indexOf('ONE-WRAP&#174; Strap') != -1) {
//                                measurementsold = "strap";
//                            } else if (iname.indexOf('Rings') != -1) {
//                                measurementsold = "ring";
//                            } else {
//                                measurementsold = "roll";
//                            }
//                            $('.available-qty .limitavailableqty').css('display', 'block');
//                            $('.available-qty .limitavailableqty .leadtime').text(ileadtime);
//                            $('.available-qty .limitavailableqty .sqty').text(iqty);
//                            $('.available-qty .limitavailableqty .msis').text(measurementsold);
//                        }
                    }

//	    if(iadditionalinfo != ''){
//                $('[data-role="content"]').find('.additional-attributes').html(iadditionalinfo);
//            }
                    // Custom code ends

                    if ($widget.element.parents($widget.options.selectorProduct)
                            .find(this.options.selectorProductPrice).is(':data(mage-priceBox)')
                            ) {
                        $widget._UpdatePrice();
                    }

                    $widget._loadMedia();
                    $input.trigger('change');
                }
            }
        }

    });

    return $.anshu.SwatchRenderer;
});