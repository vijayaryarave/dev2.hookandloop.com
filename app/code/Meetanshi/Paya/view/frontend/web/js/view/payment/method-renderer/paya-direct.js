/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Ui/js/model/messages',
        'Magento_Checkout/js/action/redirect-on-success',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/url',
        'Magento_Vault/js/view/payment/vault-enabler'
    ],
    function ($, Component,
              additionalValidators,
              quote,
              customer,
              messageContainer,
              redirectOnSuccessAction,
              fullScreenLoader,
              url,
              VaultEnabler) {
        'use strict';
        var clientId, postbackUrl, environment, merchantId, authKey, salt, requestType,
            orderNumber, amount, preAuth, cardinfo, apiresponce, token, transactionId;

        return Component.extend({
            defaults: {
                template: 'Meetanshi_Paya/payment/paya',
                active: false
            },
            messageContainer: messageContainer,
            placeOrderHandler: null,
            validateHandler: null,
            initialize: function () {
                var self = this;

                self._super();
                this.vaultEnabler = new VaultEnabler();
                this.vaultEnabler.setPaymentCode(this.getVaultCode());
                return self;
            },
            isVaultEnabled: function () {
                return this.vaultEnabler.isVaultEnabled();
            },
            getVaultCode: function () {
                return window.checkoutConfig.paya_ccVaultCode;
            },
            getCode: function () {
                return 'paya';
            },

            isActive: function () {
                return true;
            },

            initObservable: function () {
                this.loadPayaJs(function () {
                });
                this._super()
                    .observe('active');

                return this;
            },

            context: function () {
                return this;
            },

            getPayaLogoUrl: function () {
                return window.checkoutConfig.paya_imageurl;
            },

            getPayaInstructions: function () {
                return window.checkoutConfig.paya_instructions;
            },

            isShowLegend: function () {
                return true;
            },

            setPlaceOrderHandler: function (handler) {
                this.placeOrderHandler = handler;
            },

            setValidateHandler: function (handler) {
                this.validateHandler = handler;
            },
            loadPayaJs: function (callback) {
                $.getScript('https://www.sagepayments.net/pay/1.0.2/js/pay.min.js', function () {
                    callback();
                });
            },
            getData: function () {
                var data = {
                    'method': this.getCode(),
                    'additional_data': {
                        'paya_cardinfo': JSON.stringify(cardinfo),
                        'paya_apiresponce': JSON.stringify(apiresponce),
                        'transactionId': transactionId,
                        'save_card': $('#paya_enable_vault').prop("checked"),
                        'token': token
                    }
                };
                data['additional_data'] = _.extend(data['additional_data'], this.additionalData);
                this.vaultEnabler.visitAdditionalData(data);
                return data;
            },
            getAddress: function (address) {
                var _address = {
                    name: address.firstname + " " + address.lastname
                };
                if (address.street[0]) {
                    _address['address'] = address.street[0];
                }
                if (address.city) {
                    _address['city'] = address.city;
                }
                if (address.region) {
                    _address['state'] = address.region;
                }
                if (address.postcode) {
                    _address['postalCode'] = address.postcode;
                }
                if (address.postcode) {
                    _address['country'] = address.countryId;
                }
                return _address;
            },
            placeOrderPaya: function () {
                $('.action-update').click();
                if (this.validateHandler() && additionalValidators.validate()) {
                    var mainPaya = this;
                    this.isPlaceOrderActionAllowed(false);

                    require(['jquery', 'PayJS/Core', 'PayJS/Request', 'PayJS/Response', 'PayJS/Formatting'],
                        function ($, $CORE, $REQUEST, $RESPONSE, $FORMATTING) {


                            $.ajax({
                                type: 'GET',
                                data: {
                                    form_key: $.cookie('form_key')
                                },
                                url: url.build("paya/checkout/request"),
                                dataType: "json",
                                success: function (response) {
                                    if (response.success) {

                                        var clientId, postbackUrl, environment, merchantId, authKey, salt, requestType,
                                            orderNumber, amount, preAuth;

                                        clientId = response.clientId;
                                        postbackUrl = response.postbackUrl;
                                        environment = response.environment;
                                        merchantId = response.merchantId;
                                        authKey = response.authKey;
                                        salt = response.salt;
                                        requestType = response.requestType;
                                        orderNumber = response.orderNumber;
                                        amount = response.amount;
                                        preAuth = response.preAuth;


                                        mainPaya.isPlaceOrderActionAllowed(true);

                                        $CORE.Initialize({
                                            clientId: clientId,
                                            postbackUrl: postbackUrl,
                                            environment: environment,
                                            merchantId: merchantId,
                                            authKey: authKey,
                                            salt: salt,
                                            requestType: requestType,
                                            orderNumber: orderNumber,
                                            amount: amount,
                                            preAuth: preAuth
                                        });

                                        $CORE.setBilling(mainPaya.getAddress(quote.billingAddress()));
                                        if (!quote.isVirtual()) {
                                            $CORE.setShipping(mainPaya.getAddress(quote.shippingAddress()));
                                        }
                                        if (customer.isLoggedIn()) {
                                            $CORE.setCustomer({
                                                email: customer.customerData.email
                                            });
                                        }

                                        var cc = $("#payment_form_paya #paya_cc_number").val();
                                        var expYear = $("#payment_form_paya #paya_expiration_yr").val();
                                        expYear = expYear.toString();


                                        var exp = $("#payment_form_paya #paya_expiration").val() + expYear.substr(expYear.length - 2);
                                        var cvv = $("#payment_form_paya #paya_cc_cid").val();

                                        exp = $FORMATTING.formatExpirationDateInput(exp, '/');
                                        cc = $FORMATTING.formatCardNumberInput(cc, '-');

                                        cvv = cvv.replace(/\D/g, '');
                                        $("#payment_form_paya #paya_cc_cid").val(cvv);

                                        $REQUEST.doPayment(cc, exp, cvv, function (resp) {

                                            $RESPONSE.tryParse(resp);

                                            if ($RESPONSE.getTransactionSuccess()) {
                                                cardinfo = $RESPONSE.getPaymentDetails();
                                                apiresponce = $RESPONSE.getResponse();
                                                transactionId = apiresponce.transactionId;

                                                if ($('#paya_enable_vault').prop("checked")) {
                                                    mainPaya.isPlaceOrderActionAllowed(false);

                                                    $REQUEST.doVault(cc, exp, function (vaultResponse) {

                                                        vaultResponse = JSON.parse(vaultResponse);

                                                        token = vaultResponse.gatewayResponse.vaultResponse.data;

                                                        if (vaultResponse.gatewayResponse.vaultResponse.status) {
                                                            mainPaya.realPlaceOrder();
                                                        } else {
                                                            mainPaya.realPlaceOrder();
                                                        }

                                                    });
                                                } else {
                                                    mainPaya.realPlaceOrder();
                                                }

                                            } else {

                                                mainPaya.messageContainer.addErrorMessage({
                                                    message: $RESPONSE.getMessage()
                                                });

                                                fullScreenLoader.stopLoader(true);
                                                mainPaya.isPlaceOrderActionAllowed(true);
                                            }
                                        })
                                    } else {
                                        self.messageContainer.addErrorMessage({
                                            message: response.message
                                        });
                                        fullScreenLoader.stopLoader(true);
                                        mainPaya.isPlaceOrderActionAllowed(true);
                                    }

                                },
                                error: function (err) {

                                    self.messageContainer.addErrorMessage({
                                        message: "An error occurred on the server. Please try to place the order again."
                                    });

                                    fullScreenLoader.stopLoader(true);
                                    mainPaya.isPlaceOrderActionAllowed(true);
                                }
                            });
                        }
                    );
                }
            },
            realPlaceOrder: function () {
                var self = this;
                this.getPlaceOrderDeferredObject()
                    .fail(
                        function () {
                            self.isPlaceOrderActionAllowed(true);
                            fullScreenLoader.stopLoader(true);
                        }
                    ).done(
                    function () {
                        self.afterPlaceOrder();

                        if (self.redirectAfterPlaceOrder) {
                            redirectOnSuccessAction.execute();
                        }
                    }
                );
            },

            placeOrder: function () {
                $('.action-update').click(); 
                if($('.billing-address-form:visible').length == 0)
                {
                    if (this.validateHandler() && additionalValidators.validate()) {
                        fullScreenLoader.startLoader(true);
                        this.placeOrderPaya();
                    }
                }  else
                {
                    var firstname = $("input[name=firstname]").val();
                    var lastname = $("input[name=lastname]").val();
                    var street = $("input[name='street[0]']").val();
                    var city = $("input[name=city]").val();
                    var regionid = $("input[name=region_id]").val();
                    var postcode = $("input[name=postcode]").val();
                    var countryid = $("input[name=country_id]").val();
                    var telephone = $("input[name=telephone]").val();

                    if((firstname == '') || (lastname == '') || (street = '') || (city == '') || (regionid = '') || (postcode = '') || (countryid == '') || (telephone = ''))
                    {
                        $('.action-update').click();
                    }
                }    
                return false;
            }

        });
    }
);
