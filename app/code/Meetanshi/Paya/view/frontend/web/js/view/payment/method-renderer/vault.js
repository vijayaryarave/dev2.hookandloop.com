define([
    'jquery',
    'Magento_Ui/js/model/messages',
    'Magento_Checkout/js/action/redirect-on-success',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'mage/url',
    'Magento_Vault/js/view/payment/method-renderer/vault'
], function ($,
             messageContainer,
             redirectOnSuccessAction,
             fullScreenLoader,
             customer,
             quote,
             url,
             VaultComponent) {
    'use strict';

    var clientId, postbackUrl, environment, merchantId, authKey, salt, requestType,
        orderNumber, amount, preAuth, cardinfo, apiresponce, token, transactionId;

    return VaultComponent.extend({
        defaults: {
            template: 'Meetanshi_Paya/payment/formVault'
        },

        getMaskedCard: function () {
            return this.details.maskedCC;
        },

        getExpirationDate: function () {
            return this.details.expirationDate;
        },

        getCardType: function () {
            return this.details.type;
        },

        getToken: function () {
            var pblicHash = this.publicHash;
            var tok = '';
            $.ajax({
                type: 'GET',
                data: {
                    form_key: $.cookie('form_key'),
                    pblicHash: pblicHash
                },
                url: url.build("paya/checkout/getToken"),
                dataType: "json",
                async:false,
                showLoader: true,
                success: function (response) {
                    if (response.success) {
                        tok = response.token;
                    }else{
                        this.messageContainer.addErrorMessage({
                            message: "An error occurred on the server. Please try to place the order again."
                        });
                    }
                },
                error: function (err) {
                    this.messageContainer.addErrorMessage({
                        message: "An error occurred on the server. Please try to place the order again."
                    });
                }
            });
            return tok;
        },
        getData: function () {
            return {
                'method': this.item.method,
                'additional_data': {
                    'paya_apiresponce': JSON.stringify(apiresponce),
                    'transactionId': transactionId,
                    'public_hash':this.publicHash
                }
            };
        },
        initObservable: function () {
            this.loadPayaJs(function () {
            });
            this._super()
                .observe('active');

            return this;
        },
        realPlaceOrder: function () {
            var self = this;
            this.getPlaceOrderDeferredObject()
                .fail(
                    function () {
                        self.isPlaceOrderActionAllowed(true);
                        fullScreenLoader.stopLoader(true);
                    }
                ).done(
                function () {
                    self.afterPlaceOrder();

                    if (self.redirectAfterPlaceOrder) {
                        redirectOnSuccessAction.execute();
                    }
                }
            );
        },
        loadPayaJs: function (callback) {
            $.getScript('https://www.sagepayments.net/pay/1.0.2/js/pay.min.js', function () {
                callback();
            });
        },
        getAddress: function (address) {
            var _address = {
                name: address.firstname + " " + address.lastname
            };
            if (address.street[0]) {
                _address['address'] = address.street[0];
            }
            if (address.city) {
                _address['city'] = address.city;
            }
            if (address.region) {
                _address['state'] = address.region;
            }
            if (address.postcode) {
                _address['postalCode'] = address.postcode;
            }
            if (address.postcode) {
                _address['country'] = address.countryId;
            }
            return _address;
        },
        placeOrder: function () {

            token = this.getToken();

            var mainPaya = this;
            this.isPlaceOrderActionAllowed(false);


            require(['jquery', 'PayJS/Core', 'PayJS/Request', 'PayJS/Response', 'PayJS/Formatting', 'PayJS/Validation'],
                function ($, $CORE, $REQUEST, $RESPONSE, $FORMATTING, $VALIDATION) {
                    $('body').trigger('processStart');
                    $.ajax({
                        type: 'GET',
                        data: {
                            form_key: $.cookie('form_key'),
                            token: token
                        },
                        url: url.build("paya/checkout/requestToken"),
                        dataType: "json",
                        showLoader: true,
                        success: function (response) {
                            if (response.success) {

                                clientId = response.clientId;
                                postbackUrl = response.postbackUrl;
                                environment = response.environment;
                                merchantId = response.merchantId;
                                authKey = response.authKey;
                                salt = response.salt;
                                requestType = response.requestType;
                                orderNumber = response.orderNumber;
                                amount = response.amount;
                                preAuth = response.preAuth;

                                mainPaya.isPlaceOrderActionAllowed(true);
                                $('body').trigger('processStart');

                                $CORE.Initialize({
                                    clientId: clientId,
                                    merchantId: merchantId,
                                    authKey: authKey,
                                    requestType: "payment",
                                    orderNumber: orderNumber,
                                    amount: amount,
                                    environment: environment,
                                    postbackUrl: postbackUrl,
                                    preAuth: preAuth,
                                    salt: salt
                                });

                                $CORE.setBilling(mainPaya.getAddress(quote.billingAddress()));
                                if (!quote.isVirtual()) {
                                    $CORE.setShipping(mainPaya.getAddress(quote.shippingAddress()));
                                }
                                if (customer.isLoggedIn()) {
                                    $CORE.setCustomer({
                                        email: customer.customerData.email
                                    });
                                }

                                $REQUEST.doTokenPayment(token, "", function (paymentResponse) {
                                    $RESPONSE.tryParse(paymentResponse);

                                    if ($RESPONSE.getTransactionSuccess()) {
                                        apiresponce = $RESPONSE.getResponse();
                                        transactionId = apiresponce.transactionId;
                                        mainPaya.realPlaceOrder();

                                    } else {

                                        mainPaya.messageContainer.addErrorMessage({
                                            message: $RESPONSE.getMessage()
                                        });

                                        mainPaya.isPlaceOrderActionAllowed(true);
                                        $('body').trigger('processStop');
                                    }

                                });

                            } else {

                                mainPaya.messageContainer.addErrorMessage({
                                    message: response.message
                                });
                                $('body').trigger('processStop');
                                mainPaya.isPlaceOrderActionAllowed(true);
                            }
                        },
                        error: function (err) {

                            mainPaya.messageContainer.addErrorMessage({
                                message: "An error occurred on the server. Please try to place the order again."
                            });

                            $('body').trigger('processStop');
                            mainPaya.isPlaceOrderActionAllowed(true);
                        }
                    });

                    $('body').trigger('processStop');

                }
            );
            $('body').trigger('processStop');
            return false;
        }
    });
});