<?php

namespace Meetanshi\Paya\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\StoreResolver;

class Data extends AbstractHelper
{
    const CONFIG_SANDBOX_MODE = 'payment/paya/mode';
    const CONFIG_PAYMENT_ACTION = 'payment/paya/payment_action';
    const CONFIG_PAYA_LOGO = 'payment/paya/show_logo';
    const CONFIG_PAYA_INSTRUCTION = 'payment/paya/instructions';
    const CONFIG_PAYA_DEBUG = 'payment/paya/debug';

    const CONFIG_PAYA_MERCHANT_ID = 'payment/paya/merchant_id';
    const CONFIG_PAYA_MERCHANT_KEY = 'payment/paya/merchant_key';

    const CLIENT_ID_CERT = "7SMmEF02WyC7H5TSdG1KssOQlwOOCagb";
    const CLIENT_KEY_CERT = "wtC5Ns0jbtiNA8sP";

    const CLIENT_ID_PROD = "0MsJ2l3W5KQWOh3G4DCx3l1TkpZSacg5";
    const CLIENT_KEY_PROD = "oLoURCj0ZbgkJZ8y";

    const PAYA_VAULT = 'payment/paya_vault/active';

    private $encryptor;
    private $curlFactory;
    private $storeResolver;
    private $storeManager;
    private $repository;
    private $request;

    public function __construct(Context $context, EncryptorInterface $encryptor, CurlFactory $curlFactory, StoreResolver $storeResolver, StoreManagerInterface $storeManager, Repository $repository, RequestInterface $request)
    {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->curlFactory = $curlFactory;
        $this->storeResolver = $storeResolver;
        $this->storeManager = $storeManager;
        $this->repository = $repository;
        $this->request = $request;
    }

    public function showLogo()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PAYA_LOGO, ScopeInterface::SCOPE_STORE);
    }

    public function payaVaultEnable()
    {
        return $this->scopeConfig->getValue(self::PAYA_VAULT, ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentLogo()
    {
        $params = ['_secure' => $this->request->isSecure()];
        return $this->repository->getUrlWithParams('Meetanshi_Paya::images/paya.png', $params);
    }

    public function getInstructions()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PAYA_INSTRUCTION, ScopeInterface::SCOPE_STORE);
    }

    public function getMode()
    {
        return $this->scopeConfig->getValue(self::CONFIG_SANDBOX_MODE, ScopeInterface::SCOPE_STORE);
    }

    public function getMerchantId()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_PAYA_MERCHANT_ID, ScopeInterface::SCOPE_STORE
        );
    }

    public function getMerchantKey()
    {
        return $this->encryptor->decrypt($this->scopeConfig->getValue(
            self::CONFIG_PAYA_MERCHANT_KEY, ScopeInterface::SCOPE_STORE
        ));
    }

    public function getDeveloperId()
    {
        if ($this->getMode()) {
            return self::CLIENT_ID_CERT;
        } else {
            return self::CLIENT_ID_PROD;
        }
    }

    public function getDeveloperKey()
    {
        if ($this->getMode()) {
            return self::CLIENT_KEY_CERT;
        } else {
            return self::CLIENT_KEY_PROD;
        }
    }

    public function getEnvironment()
    {
        if ($this->getMode()) {
            return 'cert';
        } else {
            return 'prod';
        }
    }

    public function isLoggerEnabled()
    {
        return $this->scopeConfig->getValue(self::CONFIG_PAYA_DEBUG, ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentType()
    {
        $action = $this->scopeConfig->getValue(self::CONFIG_PAYMENT_ACTION, ScopeInterface::SCOPE_STORE);
        if ($action == 'authorize_capture') {
            return 'Payment';
        } else {
            return 'Deferred';
        }
    }

    public function getPreAuth()
    {
        $action = $this->scopeConfig->getValue(self::CONFIG_PAYMENT_ACTION, ScopeInterface::SCOPE_STORE);
        if ($action == 'authorize_capture') {
            return "false";
        } else {
            return "true";
        }
    }

    public function getStoreName()
    {
        return $this->storeManager->getStore()->getName();
    }

    public function getAuthKey($toBeHashed, $password, $salt, $iv)
    {
        $encryptHash = hash_pbkdf2("sha1", $password, $salt, 1500, 32, true);
        $encrypted = openssl_encrypt($toBeHashed, "aes-256-cbc", $encryptHash, 0, $iv);
        return $encrypted;
    }

    public function getNonces()
    {
        $iv = openssl_random_pseudo_bytes(16);
        $salt = base64_encode(bin2hex($iv));
        return [
            "iv" => $iv,
            "salt" => $salt
        ];
    }

    public function getHmac($toBeHashed, $privateKey)
    {
        return base64_encode(hash_hmac('sha512', $toBeHashed, $privateKey, true));
    }

    public function isDecimal($currency)
    {
        return in_array(strtolower($currency), [
            'bif',
            'djf',
            'jpy',
            'krw',
            'pyg',
            'vnd',
            'xaf',
            'xpf',
            'clp',
            'gnf',
            'kmf',
            'mga',
            'rwf',
            'vuv',
            'xof'
        ]);
    }
}
