<?php

namespace Meetanshi\Paya\Controller\Adminhtml\Checkout;

use Magento\Payment\Helper\Data as paymentHelper;
use Magento\Backend\Model\Session\Quote as Session;
use Magento\Checkout\Helper\Data as checkoutHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Paya\Helper\Data;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Backend\App\Action;
use Meetanshi\Paya\Helper\Logger as PayaLogger;

class RequestToken extends Action
{
    protected $customerSession;
    protected $checkoutSession;
    protected $jsonFactory;
    protected $helper;
    protected $storeManager;
    protected $quoteRepository;
    protected $_formKeyValidator;
    protected $payaLogger;

    public function __construct(
        Action\Context $context,
        paymentHelper $paymentHelper,
        Session $checkoutSession,
        checkoutHelper $checkoutData,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        Data $helper,
        Validator $formKeyValidator,
        PayaLogger $payaLogger,
        CartRepositoryInterface $quoteRepository,
        $params = []
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
        $this->jsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->_formKeyValidator = $formKeyValidator;
        $this->payaLogger = $payaLogger;
        parent::__construct($context);
        $this->quoteRepository = $quoteRepository;
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Payment Error'
                    ]);
                }

                $token = $this->getRequest()->getParam('token');

                $quote = $this->checkoutSession->getQuote();
                if (!$quote->getReservedOrderId()) {
                    $quote->reserveOrderId();
                    $quote->setReservedOrderId($quote->getReservedOrderId())->save();
                }

                $merchant = [
                    "ID" => $this->helper->getMerchantId(),
                    "KEY" => $this->helper->getMerchantKey()
                ];

                $developer = [
                    "ID" => $this->helper->getDeveloperId(),
                    "KEY" => $this->helper->getDeveloperKey()
                ];
                $reqPer = [
                    "postbackUrl" => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                    "environment" => $this->helper->getEnvironment(),
                    "amount" => $quote->getBaseGrandTotal(),
                    "preAuth" => $this->helper->getPreAuth(),
                ];

                $nonces = $this->helper->getNonces();
                $environment = $reqPer['environment'];

                $req = [
                    "merchantId" => $merchant['ID'],
                    "merchantKey" => $merchant['KEY'],
                    "requestType" => "vault",
                    "orderNumber" => $quote->getReservedOrderId(),
                    "amount" => $reqPer['amount'],
                    "salt" => $nonces['salt'],
                    "postbackUrl" => $reqPer['postbackUrl'],
                    "token" => $token,
                    "preAuth" => $reqPer['preAuth']
                ];

                $authKey = $this->helper->getAuthKey(json_encode($req), $developer['KEY'], $nonces['salt'], $nonces['iv']);

                $finalRespo = [
                    'clientId' => $developer['ID'],
                    'postbackUrl' => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                    'environment' => $environment,
                    'merchantId' => $merchant['ID'],
                    'authKey' => $authKey,
                    'salt' => $nonces['salt'],
                    'requestType' => 'payment',
                    'orderNumber' => $quote->getReservedOrderId(),
                    'amount' => $quote->getBaseGrandTotal(),
                    'preAuth' => $reqPer['preAuth'],
                    'error' => false,
                    'success' => true,
                    'message' => '',
                ];

                $this->payaLogger->debug('Paya Request', $finalRespo);

                return $this->jsonFactory->create()->setData($finalRespo);

            } catch (\Exception $e) {
                return $this->jsonFactory->create()->setData([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $this->jsonFactory->create()->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }

}
