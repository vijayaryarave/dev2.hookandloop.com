<?php

namespace Meetanshi\Paya\Controller\Adminhtml\Checkout;

use Magento\Payment\Helper\Data as paymentHelper;
use Magento\Backend\Model\Session\Quote as Session;
use Magento\Checkout\Helper\Data as checkoutHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Paya\Helper\Data;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Backend\App\Action;
use Meetanshi\Paya\Helper\Logger as PayaLogger;
use Magento\Vault\Model\PaymentTokenManagement;

class GetToken extends Action
{
    protected $customerSession;
    protected $checkoutSession;
    protected $jsonFactory;
    protected $helper;
    protected $storeManager;
    protected $quoteRepository;
    protected $_formKeyValidator;
    protected $payaLogger;
    protected $paymentTokenManagement;

    public function __construct(
        Action\Context $context,
        paymentHelper $paymentHelper,
        Session $checkoutSession,
        checkoutHelper $checkoutData,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        Data $helper,
        Validator $formKeyValidator,
        PayaLogger $payaLogger,
        CartRepositoryInterface $quoteRepository,
        PaymentTokenManagement $paymentTokenManagement,
        $params = []
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
        $this->jsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->_formKeyValidator = $formKeyValidator;
        $this->payaLogger = $payaLogger;
        $this->paymentTokenManagement = $paymentTokenManagement;
        parent::__construct($context);
        $this->quoteRepository = $quoteRepository;
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Payment Error'
                    ]);
                }

                $pblicHash = $this->getRequest()->getParam('pblicHash');
                $token = $this->getPaymentToken($pblicHash);
                return $this->jsonFactory->create()->setData([
                    'success' => true,
                    'token' => $token
                ]);

            } catch (\Exception $e) {
                return $this->jsonFactory->create()->setData([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $this->jsonFactory->create()->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }

    private function getPaymentToken($publicHash)
    {
        if ($publicHash === null) {
            return null;
        }
        $quote = $this->checkoutSession->getQuote();


        $paymentToken = $this->paymentTokenManagement->getByPublicHash(
            $publicHash,
            $quote->getCustomerId()
        );

        if (!$paymentToken) {
            return '';
        }
        return $paymentToken->getGatewayToken();
    }

}
