<?php


namespace Meetanshi\Paya\Controller;

use Magento\Framework\App\Action\Context;
use Magento\Payment\Helper\Data as paymentHelper;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Helper\Data as checkoutHelper;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Paya\Helper\Data;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Meetanshi\Paya\Helper\Logger as PayaLogger;
use Magento\Customer\Model\Session as customerSession;
use Magento\Vault\Model\PaymentTokenManagement;

abstract class Checkout extends Action
{
    protected $customerSession;
    protected $checkoutSession;
    protected $orderFactory;
    protected $jsonFactory;
    protected $helper;
    protected $storeManager;
    protected $quoteRepository;
    protected $_formKeyValidator;
    protected $payaLogger;
    protected $paymentTokenManagement;

    public function __construct(
        Context $context,
        customerSession $customerSession,
        paymentHelper $paymentHelper,
        OrderFactory $orderFactory,
        Session $checkoutSession,
        checkoutHelper $checkoutData,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        Data $helper,
        Validator $formKeyValidator,
        PaymentTokenManagement $paymentTokenManagement,
        PayaLogger $payaLogger,
        CartRepositoryInterface $quoteRepository,
        $params = []
    ) {
        $this->checkoutSession   = $checkoutSession;
        $this->customerSession   = $customerSession;
        $this->orderFactory      = $orderFactory;
        $this->helper      = $helper;
        $this->jsonFactory       = $resultJsonFactory;
        $this->storeManager     = $storeManager;
        $this->_formKeyValidator = $formKeyValidator;
        $this->payaLogger = $payaLogger;
        $this->paymentTokenManagement = $paymentTokenManagement;
        parent::__construct($context);
        $this->quoteRepository = $quoteRepository;
    }
}
