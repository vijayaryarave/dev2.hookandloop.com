<?php

namespace Meetanshi\Paya\Controller\Checkout;

use Meetanshi\Paya\Controller\Checkout;

class Request extends Checkout
{

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');

        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
        }
        if ($this->getRequest()->isAjax()) {
            try {
             //Log the request
                if($isCheckoutLoggerEnabled) {
                    $logger->info('Checkout Meetanshi\Paya\Controller\Checkout\Request: START Sage Trasaction.');
                }

                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Payment Error'
                    ]);
                }


                    $quote = $this->checkoutSession->getQuote();
                    if (!$quote->getReservedOrderId()) {
                        $quote->reserveOrderId();
                        $quote->setReservedOrderId($quote->getReservedOrderId())->save();
                    }

                    if ($quote->getIsActive()) {

                        $merchant = [
                            "ID" => $this->helper->getMerchantId(),
                            "KEY" => $this->helper->getMerchantKey()
                        ];

                        $developer = [
                            "ID" => $this->helper->getDeveloperId(),
                            "KEY" => $this->helper->getDeveloperKey()
                        ];

                        $reqPer = [
                            "postbackUrl" => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                            "environment" => $this->helper->getEnvironment(),
                            "amount" => $quote->getBaseGrandTotal(),
                            "preAuth" => $this->helper->getPreAuth(),
                        ];

                        $nonces = $this->helper->getNonces();
                        $environment = $reqPer['environment'];

                        $req = [
                            "merchantId" => $merchant['ID'],
                            "merchantKey" => $merchant['KEY'],
                            "requestType" => "payment",
                            "orderNumber" => $quote->getReservedOrderId(),
                            "amount" => $reqPer['amount'],
                            "salt" => $nonces['salt'],
                            "postbackUrl" => $reqPer['postbackUrl'],
                            "preAuth" => $reqPer['preAuth']
                        ];

                        $authKey = $this->helper->getAuthKey(json_encode($req), $developer['KEY'], $nonces['salt'], $nonces['iv']);

                        $finalRespo = [
                            'clientId' => $developer['ID'],
                            'postbackUrl' => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                            'environment' => $environment,
                            'merchantId' => $merchant['ID'],
                            'authKey' => $authKey,
                            'salt' => $nonces['salt'],
                            'requestType' => 'payment',
                            'orderNumber' => $quote->getReservedOrderId(),
                            'amount' => $quote->getBaseGrandTotal(),
                            'preAuth' => $reqPer['preAuth'],
                            'error' => false,
                            'success' => true,
                            'message' => '',
                        ];

                        $this->payaLogger->debug('Paya Request', $finalRespo);

                        if($isCheckoutLoggerEnabled) {
                            $logger->info('Paya Request');
                            $logger->info($finalRespo);
                        }
                        return $this->jsonFactory->create()->setData($finalRespo);

                    } else {
                        return $this->jsonFactory->create()->setData([
                            'error' => true,
                            'message' => 'Operation is not allow'
                        ]);
                    }
            } catch (\Exception $e) {
                //Log the request
                if($isCheckoutLoggerEnabled) {
                    $logger->info('Checkout Error :'.$e->getMessage());
                }
                return $this->jsonFactory->create()->setData([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        
        return $this->jsonFactory->create()->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }
}
