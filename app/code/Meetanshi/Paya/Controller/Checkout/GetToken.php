<?php

namespace Meetanshi\Paya\Controller\Checkout;

use Meetanshi\Paya\Controller\Checkout;

class GetToken extends Checkout
{

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Payment Error'
                    ]);
                }

                $pblicHash = $this->getRequest()->getParam('pblicHash');
                $token = $this->getPaymentToken($pblicHash);
                return $this->jsonFactory->create()->setData([
                    'success' => true,
                    'token' => $token
                ]);

            } catch (\Exception $e) {
                return $this->jsonFactory->create()->setData([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $this->jsonFactory->create()->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }

    private function getPaymentToken($publicHash)
    {
        if ($publicHash === null) {
            return null;
        }

        $paymentToken = $this->paymentTokenManagement->getByPublicHash(
            $publicHash,
            $this->customerSession->getCustomerId()
        );

        if (!$paymentToken) {
            return '';
        }
        return $paymentToken->getGatewayToken();
    }
}
