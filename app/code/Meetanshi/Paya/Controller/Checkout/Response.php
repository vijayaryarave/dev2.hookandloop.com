<?php

namespace Meetanshi\Paya\Controller\Checkout;

use Meetanshi\Paya\Controller\Checkout;

class Response extends Checkout
{
    public function execute()
    {
    	 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Meetanshi\Paya\Controller\Checkout\Response ');
            $logger->info(var_export($this->getRequest()->getParams(), true));

        }
        $this->payaLogger->debug('Paya Request', var_export($this->getRequest()->getParams(), true));
    }
}
