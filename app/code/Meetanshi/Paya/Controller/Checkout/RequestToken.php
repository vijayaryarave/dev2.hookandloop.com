<?php

namespace Meetanshi\Paya\Controller\Checkout;

use Meetanshi\Paya\Controller\Checkout;

class RequestToken extends Checkout
{

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                if (!$this->_formKeyValidator->validate($this->getRequest())) {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Payment Error'
                    ]);
                }

                $token = $this->getRequest()->getParam('token');

                $quote = $this->checkoutSession->getQuote();
                if (!$quote->getReservedOrderId()) {
                    $quote->reserveOrderId();
                    $quote->setReservedOrderId($quote->getReservedOrderId())->save();
                }

                if ($quote->getIsActive()) {

                    $merchant = [
                        "ID" => $this->helper->getMerchantId(),
                        "KEY" => $this->helper->getMerchantKey()
                    ];

                    $developer = [
                        "ID" => $this->helper->getDeveloperId(),
                        "KEY" => $this->helper->getDeveloperKey()
                    ];
                    $reqPer = [
                        "postbackUrl" => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                        "environment" => $this->helper->getEnvironment(),
                        "amount" => $quote->getBaseGrandTotal(),
                        "preAuth" => $this->helper->getPreAuth(),
                    ];

                    $nonces = $this->helper->getNonces();
                    $environment = $reqPer['environment'];

                    $req = [
                        "merchantId" => $merchant['ID'],
                        "merchantKey" => $merchant['KEY'],
                        "requestType" => "vault",
                        "orderNumber" => $quote->getReservedOrderId(),
                        "amount" => $reqPer['amount'],
                        "salt" => $nonces['salt'],
                        "postbackUrl" => $reqPer['postbackUrl'],
                        "token" => $token,
                        "preAuth" => $reqPer['preAuth']
                    ];

                    $authKey = $this->helper->getAuthKey(json_encode($req), $developer['KEY'], $nonces['salt'], $nonces['iv']);

                    $finalRespo = [
                        'clientId' => $developer['ID'],
                        'postbackUrl' => $this->storeManager->getStore()->getBaseurl() . 'paya/checkout/response',
                        'environment' => $environment,
                        'merchantId' => $merchant['ID'],
                        'authKey' => $authKey,
                        'salt' => $nonces['salt'],
                        'requestType' => 'payment',
                        'orderNumber' => $quote->getReservedOrderId(),
                        'amount' => $quote->getBaseGrandTotal(),
                        'preAuth' => $reqPer['preAuth'],
                        'error' => false,
                        'success' => true,
                        'message' => '',
                    ];

                    $this->payaLogger->debug('Paya Request', $finalRespo);

                    return $this->jsonFactory->create()->setData($finalRespo);

                } else {
                    return $this->jsonFactory->create()->setData([
                        'error' => true,
                        'message' => 'Operation is not allow'
                    ]);
                }
            } catch (\Exception $e) {
                return $this->jsonFactory->create()->setData([
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        return $this->jsonFactory->create()->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }
}
