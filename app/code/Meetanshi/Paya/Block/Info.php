<?php

namespace Meetanshi\Paya\Block;

use Magento\Payment\Block\ConfigurableInfo;

class Info extends ConfigurableInfo
{
    protected function getLabel($field)
    {
        switch ($field) {
            case 'cc_type':
                return __('Card Type');
            case 'card_number':
                return __('Card number');
            case 'transactionId':
                return __('Transaction Id');
            default:
                return parent::getLabel($field);
        }
    }
}