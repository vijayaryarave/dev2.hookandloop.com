<?php

namespace Meetanshi\Paya\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Session\SessionManagerInterface;
use Meetanshi\Paya\Helper\Data;

class PayaConfigProvider implements ConfigProviderInterface
{
    protected $helper;
    protected $checkoutSession;
    protected $coreSession;

    const CODE = 'paya';
    const CC_VAULT_CODE = 'paya_vault';

    public function __construct(Data $helper, CheckoutSession $checkoutSession, SessionManagerInterface $coreSession)
    {
        $this->helper = $helper;
        $this->checkoutSession = $checkoutSession;
        $this->coreSession = $coreSession;
    }

    public function getConfig()
    {
        $config = [];
        $showLogo = $this->helper->showLogo();
        $imageUrl = $this->helper->getPaymentLogo();
        $instructions = $this->helper->getInstructions();
        $config['paya_imageurl'] = ($showLogo) ? $imageUrl : '';
        $config['paya_instructions'] = ($instructions) ? $instructions : '';
        $config['paya_ccVaultCode'] = self::CC_VAULT_CODE;

        return $config;
    }
}
