<?php

namespace Meetanshi\Paya\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;

class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'paya';
    const CC_VAULT_CODE = 'paya_vault';

    private $session;
    private $url;
    private $assetRepo;

    public function __construct(
        SessionManagerInterface $session,
        UrlInterface $url,
        Repository $assetRepo
    )
    {
        $this->session = $session;
        $this->url = $url;
        $this->assetRepo = $assetRepo;
    }

    public function getConfig()
    {
    }
}
