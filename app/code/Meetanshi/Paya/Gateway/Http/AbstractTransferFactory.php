<?php

namespace Meetanshi\Paya\Gateway\Http;

use Magento\Framework\Xml\Generator;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;

abstract class AbstractTransferFactory implements TransferFactoryInterface
{
    protected $config;
    protected $transferBuilder;
    protected $generator;
    protected $payaHelper;
    private $action;

    public function __construct(
        ConfigInterface $config,
        TransferBuilder $transferBuilder,
        Generator $generator,
        $action = null
    )
    {
        $this->config = $config;
        $this->transferBuilder = $transferBuilder;
        $this->generator = $generator;
        $this->action = $action;
    }
}
