<?php

namespace Meetanshi\Paya\Gateway\Http\Client;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Meetanshi\Paya\Helper\Logger as PayaLogger;
use Magento\Payment\Model\Method\Logger;

class Curl implements ClientInterface
{
    private $logger;
    private $payaLogger;

    public function __construct(
        Logger $logger,
        PayaLogger $payaLogger
    )
    {
        $this->logger = $logger;
        $this->payaLogger = $payaLogger;
    }

    public function placeRequest(TransferInterface $transferObject)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');
        $data = $transferObject->getBody();
           if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Meetanshi\Paya\Gateway\Http\Client\Curl Place Request - ');

        }
        try {
            $response = $data;
            if($isCheckoutLoggerEnabled){
                $logger->info($response);
            }

        } catch (\Exception $e) {
            $message = __($e->getMessage() ?: 'Sorry, but something went wrong.');
            $this->logger->critical($message);
            if($isCheckoutLoggerEnabled){
                $logger->info($e->getMessage());
            }
            throw new LocalizedException(__($message));
        }
        return $response;

    }

}
