<?php

namespace Meetanshi\Paya\Gateway\Http;

class TransferFactory extends AbstractTransferFactory
{

    public function create(array $request)
    {
        return $this->transferBuilder
            ->setBody($request)
            ->build();
    }
}
