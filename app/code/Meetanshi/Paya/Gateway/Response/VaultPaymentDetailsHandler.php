<?php

namespace Meetanshi\Paya\Gateway\Response;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Meetanshi\Paya\Gateway\Validator\AbstractResponseValidator;

class VaultPaymentDetailsHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = SubjectReader::readPayment($handlingSubject);

        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce, true);

        $payment->setTransactionId($cardReponce[AbstractResponseValidator::TRANSACTION_ID]);
        $payment->setLastTransId($cardReponce[AbstractResponseValidator::TRANSACTION_ID]);
        $payment->setIsTransactionClosed(false);


        $payment->setAdditionalInformation('transaction_id', $cardReponce['transactionId']);
        $payment->setAdditionalInformation('response_code', $cardReponce['status']);
        $payment->setAdditionalInformation('reference_num', $cardReponce['reference']);
        $payment->setAdditionalInformation('status_code', $cardReponce['code']);
    }
}
