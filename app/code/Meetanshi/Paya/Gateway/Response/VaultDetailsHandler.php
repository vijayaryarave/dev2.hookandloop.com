<?php

namespace Meetanshi\Paya\Gateway\Response;

use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterfaceFactory;
use Magento\Vault\Api\Data\PaymentTokenInterfaceFactory;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;
use Magento\Payment\Model\InfoInterface;
use Meetanshi\Paya\Helper\Data;

class VaultDetailsHandler implements HandlerInterface
{
    private $paymentTokenFactory;
    private $paymentExtensionFactory;
    private $subjectReader;
    private $serializer;
    private $helper;

    public function __construct(
        PaymentTokenFactoryInterface $paymentTokenFactory,
        OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory,
        SubjectReader $subjectReader,
        Data $helper,
        Json $serializer = null
    )
    {
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->paymentExtensionFactory = $paymentExtensionFactory;
        $this->subjectReader = $subjectReader;
        $this->helper = $helper;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    public function handle(array $handlingSubject, array $response)
    {

        if ($this->helper->payaVaultEnable() && array_key_exists('save_card', $response)) {

            if ($response['save_card']) {
                $paymentDO = SubjectReader::readPayment($handlingSubject);
                $payment = $paymentDO->getPayment();

                $cardDetail = $response['paya_cardinfo'];
                $cardDetail = json_decode($cardDetail, true);

                $paymentToken = $this->getVaultPaymentToken($response['token'], $cardDetail);
                if (null !== $paymentToken) {
                    $extensionAttributes = $this->getExtensionAttributes($payment);
                    $extensionAttributes->setVaultPaymentToken($paymentToken);
                }
            }
        }
    }

    protected function getVaultPaymentToken($token, $cardDetail)
    {
        if (empty($token)) {
            return null;
        }

        $paymentToken = $this->paymentTokenFactory->create(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);
        $paymentToken->setGatewayToken($token);
        $paymentToken->setExpiresAt($this->getExpirationDate($cardDetail));
        $paymentToken->setIsVisible(
            (bool) (int) (1)
        );


        $cartTypeArr = [3 => 'AE',
            4 => 'VI',
            5 => 'MC',
            6 => 'Di',
        ];
        $ccNum = $cardDetail['cardType'];
        $ccType = $cartTypeArr[$ccNum];
        $maskedCC = $cardDetail['lastFourDigits'];
        $expirationDate = $cardDetail['expirationDate'];

        $paymentToken->setTokenDetails($this->convertDetailsToJSON([
            'type' => $ccType,
            'maskedCC' => $maskedCC,
            'expirationDate' => $expirationDate
        ]));

        return $paymentToken;
    }

    private function getExpirationDate($cardDetail)
    {
        $dt = explode('/', $cardDetail['expirationDate']);
        $expDate = new \DateTime(
            $dt[1]
            . '-'
            . $dt[0]
            . '-'
            . '01'
            . ' '
            . '00:00:00',
            new \DateTimeZone('UTC')
        );
        $expDate->add(new \DateInterval('P1M'));
        return $expDate->format('Y-m-d 00:00:00');
    }

    private function convertDetailsToJSON($details)
    {
        $json = $this->serializer->serialize($details);
        return $json ? $json : '{}';
    }

    private function getExtensionAttributes(InfoInterface $payment)
    {
        $extensionAttributes = $payment->getExtensionAttributes();
        if (null === $extensionAttributes) {
            $extensionAttributes = $this->paymentExtensionFactory->create();
            $payment->setExtensionAttributes($extensionAttributes);
        }
        return $extensionAttributes;
    }

}
