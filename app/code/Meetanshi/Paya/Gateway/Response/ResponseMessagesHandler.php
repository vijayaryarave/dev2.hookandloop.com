<?php

namespace Meetanshi\Paya\Gateway\Response;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class ResponseMessagesHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable'); 
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        $paymentDO = SubjectReader::readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce,true);

        $responseCode = $cardReponce['status'];
        $messages = $cardReponce['message'];

        $state = $this->getState($responseCode);

        if ($state) {
            $payment->setAdditionalInformation(
                'approve_messages',
                $messages
            );
        } else {
            $payment->setIsTransactionPending(false);
            $payment->setIsFraudDetected(true);
            $payment->setAdditionalInformation('error_messages', $messages);
        }

        if($isCheckoutLoggerEnabled) {
            $logger->info('Paya Meetanshi\Paya\Gateway\Response\ResponseMessagesHandler');
            $logger->info('Code - '.$cardReponce['status']);
            $logger->info('message - '.$messages);
        }
    }

    protected function getState($responseCode)
    {
        if ($responseCode == 'null' || $responseCode != 'Approved') {
            return false;
        }
        return true;
    }
}
