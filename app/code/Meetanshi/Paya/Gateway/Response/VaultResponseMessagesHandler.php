<?php

namespace Meetanshi\Paya\Gateway\Response;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class VaultResponseMessagesHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {

        $paymentDO = SubjectReader::readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce,true);

        $responseCode = $cardReponce['status'];
        $messages = $cardReponce['message'];

        $state = $this->getState($responseCode);

        if ($state) {
            $payment->setAdditionalInformation(
                'approve_messages',
                $messages
            );
        } else {
            $payment->setIsTransactionPending(false);
            $payment->setIsFraudDetected(true);
            $payment->setAdditionalInformation('error_messages', $messages);
        }
    }

    protected function getState($responseCode)
    {
        if ($responseCode == 'null' || $responseCode != 'Approved') {
            return false;
        }
        return true;
    }
}
