<?php

namespace Meetanshi\Paya\Gateway\Response\Direct;

use Magento\Customer\Model\Session;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Model\Config;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order\Payment;
use Meetanshi\Paya\Helper\Data;

class CardDetailsHandler implements HandlerInterface
{
    private $config;
    private $customerSession;
    private $helper;
    private $encryptor;
    private $date;

    public function __construct(Config $config, Session $customerSession, Data $helper, EncryptorInterface $encryptor, TimezoneInterface $date)
    {
        $this->config = $config;
        $this->customerSession = $customerSession;
        $this->helper = $helper;
        $this->encryptor = $encryptor;
        $this->date = $date;
    }

    public function handle(array $handlingSubject, array $response)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Meetanshi\Paya\Gateway\Http\Client\Curl Place Request - ');

        }
        $paymentDO = SubjectReader::readPayment($handlingSubject);

        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

        $cardDetail = $response['paya_cardinfo'];
        $apiresponceDetail = $response['paya_apiresponce'];
        if($isCheckoutLoggerEnabled){
            $logger->info('Cart Details - ');
            $logger->info($cardDetail);
            $logger->info('Api response - ');
            $logger->info($apiresponceDetail);
        }
        $apiresponceDetail = json_decode($apiresponceDetail, true);
        $cardDetail = json_decode($cardDetail, true);

        $cardDetails = $cardDetail['lastFourDigits'];
        $cardType = $cardDetail['cardType'];
        $maskCcNumber = 'XXXX-' . $cardDetails;

        $cartTypeArr = [3 => 'AE',
            4 => 'VI',
            5 => 'MC',
            6 => 'Di',
        ];

        $payment->setAdditionalInformation(
            'cc_type',
            $cartTypeArr[$cardType]
        );

        $payment->setAdditionalInformation('card_number', $maskCcNumber);

        if (isset($response['token'])) {
            $payment->setAdditionalInformation(
                'token',
                $response['token']
            );
        }

        foreach ($apiresponceDetail as $key => $val) {
            $payment->setAdditionalInformation(
                $key,
                $val
            );
        }

        $payment->unsAdditionalInformation(OrderPaymentInterface::CC_NUMBER_ENC);
        $payment->unsAdditionalInformation('cc_sid_enc');
        $payment->unsAdditionalInformation('cc_number');
    }
}
