<?php

namespace Meetanshi\Paya\Gateway\Response;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Meetanshi\Paya\Gateway\Validator\AbstractResponseValidator;

class PaymentDetailsHandler implements HandlerInterface
{
    public function handle(array $handlingSubject, array $response)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('sales/logger/enable');     
        $paymentDO = SubjectReader::readPayment($handlingSubject);

        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce, true);

        $cardDetail = $response['paya_cardinfo'];
        $cardDetail = json_decode($cardDetail, true);

        $payment->setTransactionId($cardReponce[AbstractResponseValidator::TRANSACTION_ID]);
        $payment->setLastTransId($cardReponce[AbstractResponseValidator::TRANSACTION_ID]);
        $payment->setIsTransactionClosed(false);


        $payment->setAdditionalInformation('transaction_id', $cardReponce['transactionId']);
        $payment->setAdditionalInformation('response_code', $cardReponce['status']);
        $payment->setAdditionalInformation('reference_num', $cardReponce['reference']);
        $payment->setAdditionalInformation('status_code', $cardReponce['code']);

          //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/customer_record_data.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Paya Response app/code/Meetanshi/Paya/Gateway/Response/PaymentDetailsHandler');
            $logger->info('transactionId - '.$cardReponce['transactionId']);
            $logger->info('response code - '.$cardReponce['status']);
            $logger->info('reference_num - '.$cardReponce['reference']);
            $logger->info('status_code - '.$cardReponce['code']);
        }
    }
}
