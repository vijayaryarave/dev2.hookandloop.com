<?php

namespace Meetanshi\Paya\Gateway\Validator;

use Magento\Payment\Gateway\Helper\SubjectReader;

/**
 * Class RefundValidator
 *
 * @package Meetanshi\Paya\Gateway\Validator
 */
class RefundValidator extends AbstractResponseValidator
{
    /**
     * @inheritdoc
     */
    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject);
        $amount = SubjectReader::readAmount($validationSubject);

        $errorMessages = [];

        $validationResult = $this->validateResponseCode($response)
            && $this->validateAuthorisationCode($response)
            && $this->validateTotalAmount($response, $amount)
            && $this->validateTransactionId($response);

        if (!$validationResult) {
            $errorMessages = [__((string)$response['statusDetail'])];
        }

        return $this->createResult($validationResult, $errorMessages);
    }
}
