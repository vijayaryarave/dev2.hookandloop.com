<?php

namespace Meetanshi\Paya\Gateway\Validator\Direct;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Meetanshi\Paya\Gateway\Validator\AbstractResponseValidator;

class VaultResponseValidator extends AbstractResponseValidator
{
    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject);

        $errorMessages = [];

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce, true);

        $validationResult = $this->validateResponseCode($cardReponce) && $this->validateTransactionId($cardReponce);

        if (!$validationResult) {
            $errorMessages = [__('Something went wrong.')];
        }

        return $this->createResult($validationResult, $errorMessages);
    }
}
