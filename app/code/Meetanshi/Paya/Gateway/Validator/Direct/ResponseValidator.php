<?php

namespace Meetanshi\Paya\Gateway\Validator\Direct;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Meetanshi\Paya\Gateway\Validator\AbstractResponseValidator;

class ResponseValidator extends AbstractResponseValidator
{
    public function validate(array $validationSubject)
    {
        $response = SubjectReader::readResponse($validationSubject);

        $errorMessages = [];

        $cardReponce = $response['paya_apiresponce'];
        $cardReponce = json_decode($cardReponce,true);

        $cardDetail = $response['paya_cardinfo'];
        $cardDetail = json_decode($cardDetail,true);

        $validationResult = $this->validateResponseCode($cardReponce)
            && $this->validateTransactionId($cardReponce)
            && $this->validateCardDetails($cardDetail);

        if (!$validationResult) {
            $errorMessages = [__('Something went wrong.')];
        }

        return $this->createResult($validationResult, $errorMessages);
    }
}
