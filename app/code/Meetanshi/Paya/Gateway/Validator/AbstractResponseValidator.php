<?php

namespace Meetanshi\Paya\Gateway\Validator;

use Magento\Payment\Gateway\Validator\AbstractValidator;

abstract class AbstractResponseValidator extends AbstractValidator
{
    const TRANSACTION_ID = 'transactionId';
    const RESPONSE_CODE = 'status';
    const CARD_TYPE = 'cardType';

    protected function validateResponseCode(array $response)
    {
        return isset($response[self::RESPONSE_CODE])
            && $response[self::RESPONSE_CODE] != 'null' && $response[self::RESPONSE_CODE] === 'Approved';
    }

    protected function validateTransactionId(array $response)
    {
        return isset($response[self::TRANSACTION_ID])
            && $response[self::TRANSACTION_ID] != 'null';
    }

    protected function validateCardDetails(array $response)
    {
        return isset($response[self::CARD_TYPE]) && $response[self::CARD_TYPE] != 'null';
    }
}