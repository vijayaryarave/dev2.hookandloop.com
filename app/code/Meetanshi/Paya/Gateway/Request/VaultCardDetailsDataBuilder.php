<?php

namespace Meetanshi\Paya\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;

class VaultCardDetailsDataBuilder implements BuilderInterface
{

    const AMOUNT = 'amount';
    const CURRENCY = 'currency';
    const DESCRIPTION = 'description';
    const CAPTURE = 'capture';
    const RECEIPT_EMAIL = 'receipt_email';
    const PAYA_CARDINFO = 'paya_cardinfo';
    const PAYA_APIRESPONCE = 'paya_apiresponce';
    const SAVE_CARD = 'save_card';
    const TOKEN = 'token';

    private $subjectReader;

    public function __construct(
        SubjectReader $subjectReader
    )
    {
        $this->subjectReader = $subjectReader;
    }

    public function build(array $subject)
    {
        $paymentDO = $this->subjectReader->readPayment($subject);
        $order = $paymentDO->getOrder();

        $payment = $paymentDO->getPayment();

        $result = [
            self::AMOUNT => $this->subjectReader->readAmount($subject),
            self::DESCRIPTION => $order->getOrderIncrementId(),
            self::CURRENCY => $order->getCurrencyCode(),
            self::PAYA_CARDINFO => $payment->getAdditionalInformation(self::PAYA_CARDINFO),
            self::PAYA_APIRESPONCE => $payment->getAdditionalInformation(self::PAYA_APIRESPONCE),
            self::TOKEN => $payment->getAdditionalInformation(self::TOKEN),
            self::SAVE_CARD => $payment->getAdditionalInformation(self::SAVE_CARD)
        ];

        return $result;
    }
}

