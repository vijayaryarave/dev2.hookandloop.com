<?php

namespace Meetanshi\Paya\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Meetanshi\Paya\Helper\Data;

class RefundDataBuilder implements BuilderInterface
{
    const PAYMENT_METHOD = 'paymentMethod';

    private $helper;

    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    public function build(array $buildSubject)
    {
        $paymentDO = SubjectReader::readPayment($buildSubject);
        $payment = $paymentDO->getPayment();
        $transactionId = $payment->getAdditionalInformation("transaction_id");
        $order = $paymentDO->getOrder();

        $multiply = 100;
        $amount = SubjectReader::readAmount($buildSubject);
        if ($this->helper->isDecimal($order->getCurrencyCode())) {
            $multiply = 1;
        }
        $amount = round($amount * $multiply);

        $tranParams = '{' .
            '"transactionType": "Refund",' .
            '"referenceTransactionId": "' . $transactionId . '",' .
            '"vendorTxCode": "demotransaction' . time() . '",' .
            '"amount": ' . $amount . ',' .
            '"currency": "' . strtoupper($order->getCurrencyCode()) . '",' .
            '"description": "Demo Transaction"';
        $tranParams .= '}';
        return [
            self::PAYMENT_METHOD => $tranParams
        ];
    }
}
