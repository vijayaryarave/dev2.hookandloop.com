<?php

namespace Meetanshi\Paya\Plugin;
use Magento\Framework\View\Asset\Minification;

class ExcludeFilesFromMinification
{
    public function aroundGetExcludes(Minification $subject, callable $proceed, $contentType)
    {
        $result = $proceed($contentType);
        if ($contentType != 'js') {
            return $result;
        }
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Core.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Formatting.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/UI.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Validation.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Request.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Response.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/Extensions.js';
        $result[] = 'https://www.sagepayments.net/pay/1.0.2/js/build/UI.html.js';
        return $result;
    }
}