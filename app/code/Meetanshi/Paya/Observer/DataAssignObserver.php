<?php

namespace Meetanshi\Paya\Observer;

use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Framework\Event\Observer;
use Magento\Customer\Model\Session as customerSession;
use Magento\Vault\Model\PaymentTokenManagement;
use Magento\Framework\App\State;
use Magento\Backend\Model\Session\Quote as BackendSession;

class DataAssignObserver extends AbstractDataAssignObserver
{
    const CC_NUMBER = 'cc_number';
    const CC_CID = 'cc_cid';
    const CC_CID_ENC = 'cc_cid_enc';
    const PAYA_CARDINFO = 'paya_cardinfo';
    const PAYA_APIRESPONCE = 'paya_apiresponce';
    const SAVE_CARD = 'save_card';
    const TOKEN = 'token';

    protected $customerSession;
    protected $paymentTokenManagement;
    protected $state;
    protected $customerBackendSession;

    protected $additionalInformation = [
        self::CC_NUMBER,
        self::CC_CID,
        self::CC_CID_ENC,
        self::PAYA_CARDINFO,
        self::PAYA_APIRESPONCE,
        self::SAVE_CARD,
        self::TOKEN,
        OrderPaymentInterface::CC_TYPE,
        OrderPaymentInterface::CC_EXP_MONTH,
        OrderPaymentInterface::CC_EXP_YEAR,
    ];


    public function __construct(
        customerSession $customerSession,
        PaymentTokenManagement $paymentTokenManagement,
        State $state,
        BackendSession $customerBackendSession
    )
    {
        $this->customerSession = $customerSession;
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->state = $state;
        $this->customerBackendSession = $customerBackendSession;
    }

    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additional = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additional)) {
            return;
        }

        if (array_key_exists('public_hash', $additional)) {
            $paymentToken = $this->getPaymentToken($additional['public_hash']);
            if (!$paymentToken) {
                $cardInformAtion = '';
            } else {
                $cardInformAtion = $paymentToken->getTokenDetails();
            }
            $additional['paya_cardinfo'] = $cardInformAtion;
        }

        $payment = $this->readPaymentModelArgument($observer);

        foreach ($this->additionalInformation as $additionalInformation) {
            $value = isset($additional[$additionalInformation])
                ? $additional[$additionalInformation]
                : null;

            if ($value === null) {
                continue;
            }

            if ($additionalInformation == self::CC_NUMBER) {
                $payment->setAdditionalInformation(
                    OrderPaymentInterface::CC_NUMBER_ENC,
                    $payment->encrypt($value)
                );

                continue;
            } elseif ($additionalInformation == self::CC_CID) {
                $payment->setAdditionalInformation(
                    self::CC_CID_ENC,
                    $payment->encrypt($value)
                );
                continue;
            }

            $payment->setAdditionalInformation(
                $additionalInformation,
                $value
            );

            $payment->setData(
                $additionalInformation,
                $value
            );
        }
    }

    private function getArea()
    {
        try {
            return $this->state->getAreaCode();
        } catch (\Exception $e) {
            return 'frontend';
        }
    }

    private function getPaymentToken($publicHash)
    {
        if ($publicHash === null) {
            return null;
        }

        if ($this->getArea() == 'adminhtml') {
            $quote = $this->customerBackendSession->getQuote();
            $customerId = $quote->getCustomerId();
        } else {
            $customerId = $this->customerSession->getCustomerId();
        }

        $paymentToken = $this->paymentTokenManagement->getByPublicHash(
            $publicHash,
            $customerId
        );

        return $paymentToken;
    }
}
