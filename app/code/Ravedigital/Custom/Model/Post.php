<?php
namespace Ravedigital\Custom\Model;

class Post extends \FishPig\WordPress\Model\Post
{
	public function getPageTitle()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('wp_postmeta');
            $sql = "SELECT meta_value FROM " . $tableName ." WHERE post_id=".$this->getId()." AND meta_key= '_yoast_wpseo_title'";
            $result = $connection->fetchAll($sql);
            if(isset($result[0]['meta_value']) && $result[0]['meta_value'] !=''){
                return $result[0]['meta_value'];
            }
                else{ return sprintf('%s | %s', $this->getName(), $this->getBlogName());
            }
    }
}