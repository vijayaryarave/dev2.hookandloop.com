<?php
/**
 * Ravedigital_Ordercreate
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2016 Avalara, Inc.
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

namespace Ravedigital\Ordercreate\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Class OrderSuccessAfter
 */
class OrderSuccessAfter implements ObserverInterface
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        //Session set in Magenest\SagepayUS\Model\SagepayUSPayment
        $this->checkoutSession->unsCaptureCalledAt();

        //$lastOrderId = $observer->getEvent()->getOrderIds();
    }
}

