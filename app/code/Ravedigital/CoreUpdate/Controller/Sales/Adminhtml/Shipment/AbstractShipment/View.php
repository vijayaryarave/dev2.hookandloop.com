<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ravedigital\CoreUpdate\Controller\Sales\Adminhtml\Shipment\AbstractShipment;


abstract class View extends \Magento\Sales\Controller\Adminhtml\Shipment\AbstractShipment\View
{
    
    /**
     * Shipment information page
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        if ($this->getRequest()->getParam('shipment_id')) {
            $resultForward->setController('order_shipment')
                ->setModule('adminhtml')
                ->setParams(['come_from' => 'shipment'])
                ->forward('view');
            return $resultForward;
        } else {
            return $resultForward->forward('noroute');
        }
    }
}
