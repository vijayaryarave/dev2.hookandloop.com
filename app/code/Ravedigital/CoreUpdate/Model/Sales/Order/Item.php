<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ravedigital\CoreUpdate\Model\Sales\Order;

class Item extends \Magento\Sales\Model\Order\Item
{      

    /* Returns handling_charges
     *
     * @return float|null
     */
    public function getHandlingCharges()
    {
        //if($this->getData(OrderItemInterface::PRODUCT_TYPE) == 'simple'){
            return $this->getData(OrderItemInterface::HANDLING_CHARGES);
        //}
        
    }
     /**
     * Returns cut_to_length_charges
     *
     * @return float|null
     */
    public function getCutToLengthCharges()
    {
        return $this->getData(OrderItemInterface::CUT_TO_LENGTH_CHARGES);
    }

    /**
     * {@inheritdoc}
     */
    public function setHandlingCharges($handlingCharges)
    {
        //if($this->getData(OrderItemInterface::PRODUCT_TYPE) == 'simple'){
            return $this->setData(OrderItemInterface::HANDLING_CHARGES, $handlingCharges);
        //}
    }

    /**
     * {@inheritdoc}
     */
    public function setCutToLengthCharges($cutToLengthCharges)
    {
        return $this->setData(OrderItemInterface::CUT_TO_LENGTH_CHARGES, $cutToLengthCharges);
    }

  
}
