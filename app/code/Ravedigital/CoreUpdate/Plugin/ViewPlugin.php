<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ravedigital\CoreUpdate\Plugin;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;

class ViewPlugin
{    
    protected $resultForwardFactory;

    public function __construct(
        ForwardFactory $resultForwardFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_request = $request;
    }
    /**
     * Shipment information page
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function afterExecute($resultForwardFactory, $resultForward)
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        if ($this->_request->getParam('shipment_id')) {
            $resultForward->setController('order_shipment')
                ->setModule('adminhtml')
                ->setParams(['come_from' => 'shipment'])
                ->forward('view');
            return $resultForward;
        } else {
            return $resultForward->forward('noroute');
        }
    }
}
