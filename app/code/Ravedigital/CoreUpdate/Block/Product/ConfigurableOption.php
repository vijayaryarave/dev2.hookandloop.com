<?php

namespace Ravedigital\CoreUpdate\Block\Product;

class ConfigurableOption extends \Tigren\Ajaxcart\Block\Product\ConfigurableOption
{      
    protected $_registry;
    protected $_cart;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,       
        \Magento\Framework\Registry $registry,
        array $data = [],
        \Magento\Checkout\Model\Cart $cart
    )
    {       
        $this->_registry = $registry;
        $this->_cart = $cart;
        parent::__construct($context, $registry, $data);
    }

    public function getCustomOptionValue($productid)
    {
        $items = $this->_cart->getQuote()->getAllItems();
        $optionData ='';
        foreach ($items as $item) {
            if($item->getProduct()->getId() == $productid ){
                $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());         
                if (isset($options) && !empty($options['options'])) {
                    $customOptions = $options['options'];
                    if (!empty($customOptions)) {
                        $optionData ='';
                        foreach ($customOptions as $option) { 
                            if (!empty($option['value']) && strpos($option['value'], 'Yes') !== false) {
                                 $radioValue= ': Yes';
                            } else {
                                $radioValue = ' '.$option['value'];
                            }                         
                           $optionData .= '<span class="option-label">'.$option['label'].'<span class="option-value">'.$radioValue.'</span></span>';
                        }               
                    } else{
                        $optionData ='';
                    } 
                }
           }
        }
        return $optionData;
    }

    public function getCustomOptions(){
        $valid = true;
        if((null !== $this->_request->getParam('options')) && (count(array_filter($this->_request->getParam('options'))) == 0))
        {
            $valid = false;
        }
        return $valid;
    }
}