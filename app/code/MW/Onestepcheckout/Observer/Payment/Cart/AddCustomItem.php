<?php

/**
 * *
 *  Copyright © 2016 MW. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
 
namespace MW\Onestepcheckout\Observer\Payment\Cart;

use Magento\Framework\Event\ObserverInterface;

class AddCustomItem implements ObserverInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;
 
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
         $this->_checkoutSession = $checkoutSession;
    }
 
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $cart = $observer->getEvent()->getCart();
        $quote = $this->_checkoutSession->getQuote();
        $paymentMethod = $quote->getPayment()->getMethod();
        $paypalMehodList = [
            'payflowpro',
            'payflow_link',
            'payflow_advanced',
            'braintree_paypal',
            'paypal_express_bml',
            'payflow_express_bml',
            'payflow_express',
            'paypal_express'
        ];
        if ($quote->getOnestepcheckoutGiftwrapAmount() &&
            ($paymentMethod == null || in_array($paymentMethod, $paypalMehodList))) {
            if (method_exists($cart, 'addCustomItem')) {
                $name = __("Gift Wrap");
                $cart->addCustomItem($name, 1, $quote->getOnestepcheckoutGiftwrapAmount(), 'osc_giftwrap');
            }
        }

         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_onestepcheckout/general/logger');

          //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/checkout_stuck.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Checkout app\code\MW\Onestepcheckout\Observer\Payment\Cart\AddCustomItem: Payment Method - '.$paymentMethod);

            $writer1 = new \Zend\Log\Writer\Stream(BP . '/var/log/placeorder_payment.log');
            $logger1 = new \Zend\Log\Logger();
            $logger1->addWriter($writer1);
            //$logger1->info('app\code\MW\Onestepcheckout\Observer\Payment\Cart\AddCustomItem: Quote: ');
           // $logger1->info($quote);
            $logger1->info('app\code\MW\Onestepcheckout\Observer\Payment\Cart\AddCustomItem: Payment Method - '.$paymentMethod);
        }
        return $this;
    }
}
