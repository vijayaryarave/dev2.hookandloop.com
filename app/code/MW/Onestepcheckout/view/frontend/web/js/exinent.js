requirejs(['jquery'], function (jQuery) {
    jQuery(function ($) {
        $(document).ready(function () {
            
            var checkExist = setInterval(function () {
                if ($('#checkout .table-checkout-shipping-method tbody td.col-method input').length) {
                    console.log("Exists!");
                    $('#checkout .table-checkout-shipping-method tbody td.col-method input').on('change', function () {
                        var curElement = jQuery(this).parents('tr').find('td.col-carrier').attr('id');
                        console.log(curElement);
                        if (curElement == 'label_carrier_shippingoptions_shippingoptions') {
                            console.log('inside');
                            jQuery('select[name="shipping_option_field[shipping_options_method]"] option[value="fedex"]').prop('selected', true);
                            jQuery('select[name="shipping_option_field[shipping_options_service]"] option[value="ground"]').prop('selected', true);
                        }
                    });
                    clearInterval(checkExist);
                }
            }, 1000); // check every 100ms

        });
    });
});