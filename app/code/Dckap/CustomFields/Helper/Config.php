<?php

namespace Dckap\CustomFields\Helper;

/**
 * Class Config
 * @package MW\Onestepcheckout\Helper
 */

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

     /*Shipping option*/
    public function getShippingOption()
    {  
        $fedexData = $this->scopeConfig->getValue('carriers/shippingoptions/fedex_service_option');
        $upsData = $this->scopeConfig->getValue('carriers/shippingoptions/ups_service_option');
        $dhlData = $this->scopeConfig->getValue('carriers/shippingoptions/dhl_service_option');      

       $allShippingOption = array('fedex'=>$fedexData, 'ups'=>$upsData, 'dhl'=>$dhlData );
       return json_encode($allShippingOption);
    }
}
