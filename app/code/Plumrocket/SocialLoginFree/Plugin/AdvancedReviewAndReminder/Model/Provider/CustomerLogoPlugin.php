<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Plugin\AdvancedReviewAndReminder\Model\Provider;

class CustomerLogoPlugin
{
    /**
     * @var \Plumrocket\SocialLoginFree\Helper\Data
     */
    private $dataHelper;

    /**
     * CustomerLogoPlugin constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Helper\Data $dataHelper
     */
    public function __construct(
        \Plumrocket\SocialLoginFree\Helper\Data $dataHelper
    ) {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo $subject
     * @param string                                                            $result
     * @return string
     */
    public function afterGenerateLogoUrl(
        \Plumrocket\AdvancedReviewAndReminder\Model\Provider\CustomerLogo $subject,
        $result
    ) {
        if ($customerId = $subject->getProcessCustomerId()) {
             return (string)$this->dataHelper->getPhotoPath(false, $customerId);
        }

        return $result;
    }
}
