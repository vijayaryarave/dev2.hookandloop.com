<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Buttons;

class Preparer
{
    /**
     * @param array $buttons
     * @param array $sortableParams
     * @param bool  $splitByVisibility
     * @return array
     */
    public function prepareSortAndVisibility(array $buttons, array $sortableParams, $splitByVisibility = true)
    {
        $buttons = array_map(
            static function ($button) {
                $button['visible'] = true;
                return $button;
            },
            $buttons
        );

        if (! $splitByVisibility) {
            return $buttons;
        }

        return ['visible' => $buttons, 'hidden' => []];
    }
}
