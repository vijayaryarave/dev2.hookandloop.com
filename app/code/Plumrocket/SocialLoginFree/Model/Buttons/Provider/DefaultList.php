<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Buttons\Provider;

class DefaultList implements \Plumrocket\SocialLoginFree\Api\Buttons\ProviderInterface
{
    /**
     * @var \Plumrocket\SocialLoginFree\Model\Buttons\Factory
     */
    private $buttonsFactory;

    /**
     * @var \Plumrocket\SocialLoginFree\Api\TypesProviderInterface
     */
    private $typesProvider;

    /**
     * @var \Plumrocket\SocialLoginFree\Model\Buttons\Preparer
     */
    private $preparer;

    /**
     * @var array[]
     */
    private $buttonsCache = [];

    /**
     * DefaultList constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Model\Buttons\Factory      $buttonsFactory
     * @param \Plumrocket\SocialLoginFree\Api\TypesProviderInterface $typesProvider
     * @param \Plumrocket\SocialLoginFree\Model\Buttons\Preparer     $preparer
     */
    public function __construct(
        \Plumrocket\SocialLoginFree\Model\Buttons\Factory $buttonsFactory,
        \Plumrocket\SocialLoginFree\Api\TypesProviderInterface $typesProvider,
        \Plumrocket\SocialLoginFree\Model\Buttons\Preparer $preparer
    ) {
        $this->buttonsFactory = $buttonsFactory;
        $this->typesProvider = $typesProvider;
        $this->preparer = $preparer;
    }

    /**
     * {@inheritDoc}
     */
    public function getButtons($onlyEnabled = true, $storeId = null, $forceReload = false)
    {
        $cacheKey = $this->getCacheKey([$onlyEnabled, $storeId]);

        if ($forceReload || ! isset($this->buttonsCache[$cacheKey])) {
            $types = $onlyEnabled
                ? $this->typesProvider->getEnabledList($storeId)
                : $this->typesProvider->getAll($storeId);

            $this->buttonsCache[$cacheKey] = $this->buttonsFactory->create($types);
        }

        return $this->buttonsCache[$cacheKey];
    }

    /**
     * {@inheritDoc}
     */
    public function getPreparedButtons(
        $onlyEnabled = true,
        $splitByVisibility = true,
        $storeId = null,
        $forceReload = false
    ) {
        $buttons = $this->getButtons($onlyEnabled, $storeId, $forceReload);

        return $this->preparer->prepareSortAndVisibility($buttons, [], $splitByVisibility);
    }

    /**
     * Get key for cache
     *
     * @param array $data
     * @return string
     */
    private function getCacheKey($data)
    {
        $serializeData = [];
        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $serializeData[$key] = $value->getId();
            } else {
                $serializeData[$key] = $value;
            }
        }
        $serializeData = json_encode($serializeData);
        return sha1($serializeData);
    }
}
