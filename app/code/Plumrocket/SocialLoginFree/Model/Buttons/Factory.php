<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Buttons;

class Factory
{
    /**
     * @var \Plumrocket\SocialLoginFree\Model\AccountProviderInterface
     */
    private $networkProvider;

    /**
     * Factory constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Model\AccountProviderInterface $networkProvider
     */
    public function __construct(
        \Plumrocket\SocialLoginFree\Model\AccountProviderInterface $networkProvider
    ) {
        $this->networkProvider = $networkProvider;
    }

    /**
     * @param string[] $types
     * @param bool     $onlyEnabled
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create(array $types, $onlyEnabled = true)
    {
        $buttons = [];

        foreach ($types as $type) {
            $network = $this->networkProvider->createByType($type);

            if ($onlyEnabled && ! $network->enabled()) {
                continue;
            }

            $buttons[$network->getProvider()] = $network->getButton();
        }

        return $buttons;
    }
}
