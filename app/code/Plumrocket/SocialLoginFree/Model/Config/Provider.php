<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Config;

class Provider
{
    const XML_PATH_BUTTON_SORT = 'psloginpro/general/sortable';
    const XML_PATH_BUTTON_REMINDER_SORT = 'psloginpro/general/reminder_sortable';
    const XML_PATH_REGISTER_REQUIRE_LEVEL = 'psloginpro/general/validate_ignore';
    const XML_PATH_REMINDER_POPUP_ENABLED = 'psloginpro/general/reminder_popup';
    const XML_PATH_PREVENT_DUPLICATE_ENABLED = 'psloginpro/general/prevent_duplicate';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var array
     */
    private $networkEnabledInfo;

    /**
     * Provider constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Base Helper has got same method
     *
     * Receive magento config value
     *
     * @param string      $path
     * @param string|int  $store
     * @param string|null $scope
     * @return mixed
     */
    public function getConfig($path, $store = null, $scope = null)
    {
        if ($scope === null) {
            $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        }

        return $this->scopeConfig->getValue($path, $scope, $store);
    }

    /**
     * @param null $scope
     * @param null $scopeCode
     * @return array
     */
    public function getNetworkStatusInfo($scope = null, $scopeCode = null)
    {
        if (null === $this->networkEnabledInfo) {
            $groups = $this->getConfig(\Plumrocket\SocialLoginFree\Helper\Data::SECTION_ID, $scopeCode, $scope);
            $this->networkEnabledInfo = [];

            if (! $groups) {
                return $this->networkEnabledInfo;
            }

            if (is_array($groups)) {
                unset(
                    $groups['general'],
                    $groups['share'],
                    $groups['developer']
                );

                foreach ($groups as $name => $fields) {
                    $this->networkEnabledInfo[$name] = isset($fields['enable'])
                        ? (int)$fields['enable']
                        : 0;
                }
            }
        }

        return $this->networkEnabledInfo;
    }
}
