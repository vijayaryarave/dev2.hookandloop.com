<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_SocialLoginFree
 * @copyright   Copyright (c) 2019 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\SocialLoginFree\Model\Provider;

class Types implements \Plumrocket\SocialLoginFree\Api\TypesProviderInterface
{
    /**
     * @var \Plumrocket\SocialLoginFree\Model\Config\Provider
     */
    private $configProvider;

    /**
     * Types constructor.
     *
     * @param \Plumrocket\SocialLoginFree\Model\Config\Provider $configProvider
     */
    public function __construct(\Plumrocket\SocialLoginFree\Model\Config\Provider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    /**
     * @param null $scope
     * @param null $scopeCode
     * @return array
     */
    public function getEnabledList($scopeCode = null, $scope = null)
    {
        return array_keys(array_filter($this->configProvider->getNetworkStatusInfo($scope, $scopeCode)));
    }

    /**
     * @param null $scope
     * @param null $scopeCode
     * @return array
     */
    public function getDisabledList($scopeCode = null, $scope = null)
    {
        return array_keys(
            array_filter(
                $this->configProvider->getNetworkStatusInfo($scope, $scopeCode),
                static function ($status) {
                    return !$status;
                }
            )
        );
    }

    /**
     * @param null $scope
     * @param null $scopeCode
     * @return array
     */
    public function getAll($scopeCode = null, $scope = null)
    {
        return array_keys($this->configProvider->getNetworkStatusInfo($scope, $scopeCode));
    }
}
