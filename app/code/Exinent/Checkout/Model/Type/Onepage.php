<?php

/**
 * Exinent_Checkout Module 
 *
 * @category    checkout
 * @package     Exinent_Checkout
 * @author      pawan
 *
 */

namespace Exinent\Checkout\Model\Type;

class Onepage extends \Magento\Checkout\Model\Type\Onepage {

    /**
     * Create order based on checkout type. Create customer if necessary.
     *
     * @return $this
     */
//    public function saveOrder() {
//
//        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/templog.log');
//        $logger = new \Zend\Log\Logger();
//        $logger->addWriter($writer);
//
////        $logger->info($this->getQuote()->getId());
//        $logger->info("sdjkfjsdhgfhsdgh");
//
//        $this->validate();
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        $isNewCustomer = false;
//        switch ($this->getCheckoutMethod()) {
//            case self::METHOD_GUEST:
//                $this->_prepareGuestQuote();
//                break;
//            case self::METHOD_REGISTER:
//                $this->_prepareNewCustomerQuote();
//                $isNewCustomer = true;
//                break;
//            default:
//                $this->_prepareCustomerQuote();
//                break;
//        }
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        $order = $this->quoteManagement->submit($this->getQuote());
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        if ($isNewCustomer) {
//            try {
//                $this->_involveNewCustomer();
//            } catch (\Exception $e) {
//                $this->_logger->critical($e);
//            }
//        }
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        $this->_checkoutSession
//                ->setLastQuoteId($this->getQuote()->getId())
//                ->setLastSuccessQuoteId($this->getQuote()->getId())
//                ->clearHelperData();
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//        $logger->info($this->getQuote()->getId());
//        $logger->info($order->getId());
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        if ($order) {
//            $this->_eventManager->dispatch(
//                    'checkout_type_onepage_save_order_after', ['order' => $order, 'quote' => $this->getQuote()]
//            );
//
//            $logger->info(__FILE__ . '   ' . __LINE__);
//
//            /**
//             * a flag to set that there will be redirect to third party after confirmation
//             */
//            $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();
//
//            $logger->info(__FILE__ . '   ' . __LINE__);
//
//            /**
//             * we only want to send to customer about new order when there is no redirect to third party
//             */
//            if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
//                try {
//                    $logger->info(__FILE__ . '   ' . __LINE__);
//                    $this->orderSender->send($order);
//                } catch (\Exception $e) {
//                    $logger->info(__FILE__ . '   ' . __LINE__);
//                    $this->_logger->critical($e);
//                }
//            }
//
//            $logger->info(__FILE__ . '   ' . __LINE__);
//
//            // add order information to the session
//            $this->_checkoutSession
//                    ->setLastOrderId($order->getId())
//                    ->setRedirectUrl($redirectUrl)
//                    ->setLastRealOrderId($order->getIncrementId())
//                    ->setLastOrderStatus($order->getStatus());
//
//            $logger->info(__FILE__ . '   ' . __LINE__);
//        }
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        $this->_eventManager->dispatch(
//                'checkout_submit_all_after', [
//            'order' => $order,
//            'quote' => $this->getQuote()
//                ]
//        );
//
//        $logger->info(__FILE__ . '   ' . __LINE__);
//
//        return $this;
//    }
    
    public function duplicateOrder($order)
    {
        
        $isNewCustomer = false;
        switch ($this->getCheckoutMethod()) {
            case self::METHOD_GUEST:
                break;
            case self::METHOD_REGISTER:
                $isNewCustomer = true;
                break;
            default:
                break;
        }

        if ($isNewCustomer) {
            try {
                 
                $this->_involveNewCustomer();
            } catch (\Exception $e) {
                $this->_logger->critical($e);
            }
        }

        
        $this->_checkoutSession->setLastQuoteId($this->getQuote()->getId())
            ->setLastSuccessQuoteId($this->getQuote()->getId())
            ->clearHelperData();

        if ($order) {
            

            $redirectUrl = '';

            
            /**
             * we only want to send to customer about new order when there is no redirect to third party
             */
            if ($order->getCanSendNewEmailFlag()) {
                try {
                    $this->orderSender->send($order);
                } catch (\Exception $e) {
                    $this->_logger->critical($e);
                }
            }

            // add order information to the session
             //echo $order->getIncrementId();die("here");
            $this->_checkoutSession->setLastOrderId($order->getId())
                ->setRedirectUrl($redirectUrl)
                ->setLastRealOrderId($order->getIncrementId());
            
        }

        
        $quote = $this->getQuote();
        $quote->setIsActive(false);
        $quote->save();
	return $this;
    }

}
