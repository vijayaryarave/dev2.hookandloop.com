<?php

/**
 * Exinent_Catalog Module 
 *
 * @category    catalog
 * @package     Exinent_Catalog
 * @author      pawan
 *
 */

namespace Exinent\Catalog\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    public function __construct(
        Context $context, 
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
        \Magento\Framework\Serialize\Serializer\Json $serialize
    ) {

        $this->ruleCollectionFactory = $ruleCollectionFactory;
        $this->serialize = $serialize;
        parent::__construct($context);
    }

    public function getProductDiscounts($product) {
        $discounts = array();
        $productCategoryIds = $product->getCategoryIds();
        
        $collection = $this->ruleCollectionFactory->create();
        $collection->addFieldToFilter('coupon_type', 1);
        $collection->addFieldToFilter('is_active', 1);
        $collection->addFieldToFilter('conditions_serialized', array('like' => '%category_ids%'));
        foreach ($collection as $discount) {
            $cond = $this->serialize->unserialize($discount->getConditionsSerialized());
            $cond = isset($cond['conditions'][0]['conditions']) ? $cond['conditions'][0]['conditions'] : array();
            if (count($cond)) {
                $categoryIds = array();
                $qty = 0;

                foreach ($cond as $entity) {
                    if (isset($entity['attribute']) && ($entity['attribute'] == 'category_ids')) {
                        $categoryIds = array_map('intval', explode(',', $entity['value']));
                    } else if (isset($entity['conditions'])) {
                        $newCond = $entity['conditions'];

                        foreach ($newCond as $newEntity) {
                            if (isset($newEntity['attribute']) && ($newEntity['attribute'] == 'category_ids')) {
                                $categoryIds = array_map('intval', explode(',', $newEntity['value']));
                            }

                            if (isset($newEntity['attribute']) && ($newEntity['attribute'] == 'quote_item_qty')) {

                                if (!$qty || ($qty > $newEntity['value'])) {
                                    $qty = (int) $newEntity['value'];
                                }
                            }
                        }
                    }
                    
                    if (isset($entity['attribute']) && ($entity['attribute'] == 'quote_item_qty')) {

                        if (!$qty || ($qty > $entity['value'])) {
                            $qty = (int) $entity['value'];
                        }
                    }
                }
                
                if (count(array_intersect($productCategoryIds, $categoryIds))) {
                    $unit = 'ROLLS';
                    $name = $discount->getName();
                    if($qty != 0){
	                    $qtyDiscount=(int) ($qty / $product->getMeasurementSoldInSize());
	                    
	                    if (preg_match('/Wide Loop/', $name)) {
	                        $unit = 'YARDS';
	                        $qtyDiscount = (int) ($qty);
	                    }
	                    elseif (preg_match('/ONE-WRAP Cable Ties/', $name)) {
	                        $unit = 'STRAPS';
	                        $qtyDiscount=(int) ($qty / $product->getMeasurementSoldInSize());
	                    }
	                    elseif (preg_match('/Unit Volume/', $name)) {
	                        $unit = '';
	                        $qtyDiscount=(int) ($qty / $product->getMeasurementSoldInSize());
	                    }
	                    
	                    $prodDiscount['unit'] = $unit;
	                    $prodDiscount['qty'] = $qtyDiscount;
	                    $prodDiscount['discount'] = ((int) $discount->getDiscountAmount());
	                    $discounts[] = $prodDiscount;
	                }
                }
            }
        }
        
        return $discounts;
    }
}
