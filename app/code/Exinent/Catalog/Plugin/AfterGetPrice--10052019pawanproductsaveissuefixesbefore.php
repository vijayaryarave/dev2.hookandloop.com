<?php

/**
 * Exinent_Catalog Module 
 *
 * @category    checkout
 * @package     Exinent_Catalog
 * @author      pawan
 *
 */

namespace Exinent\Catalog\Plugin;

//use Magento\Framework\Event\ObserverInterface;


class AfterGetPrice {

    protected $scopeConfig;
    protected $request;

    public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\App\Request\Http $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
    }

//     protected $eventManager;

    public function afterGetPrice(\Magento\Catalog\Model\Product $product, $result) {
        
        
        $id = $product->getId();
        $sku = $product->getSku();
        $product->getCustomAttribute('id');
        $brand_id = substr($sku, 0, 2);
        $actionName = $this->request->getFullActionName();
        $result = $this->calculate($result, $product, $id, $brand_id);
        return $result;
    }

    public function calculate($price, $product, $id, $brand_id) {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/aftergetprice.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
//        $logger->info('Your text message');
        $actionName = $this->request->getFullActionName();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $qty = $product->getQty();
        if ($product->getData("special_price")) {
            $finalPrice = $product->getData("special_price");
        } else {
            $finalPrice = $product->getData("price");
        }
        if ($actionName == 'catalog_product_view'||$actionName == 'cms_page_view') {
        $productQuantity = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($id);
//        $logger->info("measure==".$product->getMeasurementSoldInSize()."==misale".$productQuantity->getMinSaleQty()."==final".$finalPrice."==id".$id);
//        $logger->info("measure==".$product->getMeasurementSoldInSize()."==misale".$productQuantity->getMinSaleQty()."==final".$finalPrice."==id".$id);
        if ($product->getMeasurementSoldInSize() > $productQuantity->getMinSaleQty()) {
            $finalPrice *= $product->getMeasurementSoldInSize();
            $product->setFinalPrice($finalPrice);
            
            return $finalPrice;
        }
        }
        return $finalPrice;  
    }

}
