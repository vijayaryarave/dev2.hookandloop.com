/**
 * Copyright 2019 aheadWorks. All rights reserved.\nSee LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            awHelpdeskContactForm: 'Aheadworks_Helpdesk/js/awhelpdesk-contact-form'
        }
    }
};
