/**
 * Copyright 2019 aheadWorks. All rights reserved.\nSee LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            awHelpdeskTicketManager: 'Aheadworks_Helpdesk/js/ticket',
            AWHelpdesk_Autocomplete: 'Aheadworks_Helpdesk/js/jquery.autocomplete'
        }
    }
};
