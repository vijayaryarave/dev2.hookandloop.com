# 1.0.0 (2018-12-13)

* Added Bing Ads Tracking support
* Added configuration settings
* Added ACL settings
* Added conversion tracking on order success page
* Added Uninstall script
* Added en_US translation