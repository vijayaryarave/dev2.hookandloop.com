var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/place-order': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            'Magento_Checkout/js/action/set-payment-information': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            'Magento_Checkout/js/action/set-payment-information-extended': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            // Paypal
            'Magento_Paypal/js/action/set-payment-method': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            // Amazon Payment
            'Amazon_Payment/js/action/place-order': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            // Mageplaza OSC
            'Mageplaza_Osc/js/action/set-payment-method': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            'Mageplaza_Osc/js/action/place-order': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            // CustomerParadigm Order Comments
            'CustomerParadigm_OrderComments/js/action/place-order': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            },
            // Mageants Front Order Comments
            'Mageants_FrontOrderComment/js/action/place-order': {
                'Swissup_Recaptcha/js/model/place-order-mixin': true
            }
        }
    }
};
