define([
    'jquery',
    'mage/template'
], function ($, mageTemplate) {
    'use strict';

    var defaults = {
        form: 'form',
        destination: '.field:last',
        method: 'after',
        hidden: true
    };

    /**
     * @param {jQuery} form
     */
    function initHiddenRecaptcha(form) {
        form.addClass('hidden-recaptcha');

        form.select('input, select, textarea').on('click', function () {
            $(this).addClass('visible-recaptcha');
            $(this).removeClass('hidden-recaptcha');
        });
    }

    return function (options) {
        var $form,
            renderedElement;

        options = $.extend(defaults, options);
        $form = $(options.form);

        if (options.hidden) {
            initHiddenRecaptcha($form);
        }

        renderedElement = mageTemplate(options.template);
        $(options.destination, $form)[options.method](renderedElement)
            .trigger('contentUpdated'); // force to render reCAPTCHA
    };
});
