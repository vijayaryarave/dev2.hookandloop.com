define([
    'Magento_Ui/js/lib/view/utils/async',
    'ko',
    'uiComponent',
    'Swissup_Recaptcha/js/recaptcha',
    'Magento_Checkout/js/model/quote'
], function ($, ko, Component, Recaptcha, quote) {
    'use strict';

    var config,
        recaptchaElement,
        lastTarget = null;

    /**
     * Get HTMLElement that represents payment method on checkout.
     *
     * @param  {String}      method
     * @return {HTMLElement}
     */
    function getPaymentMethodElement(method) {
        var element;

        $('.payment-method').each(function () {
            var uiComponent = ko.dataFor(this);

            if (uiComponent &&
                uiComponent.index === method
            ) {
                element = this;

                return false;
            }
        });

        return element;
    }

    return Component.extend({
        defaults: {
            template: 'Swissup_Recaptcha/checkout/recaptcha',
            formId: null
        },

        /**
         * {@inheritdoc}
         */
        initialize: function () {
            var me = this;

            me._super();

            if (window[me.configSource] && window[me.configSource].swissupRecaptcha) {
                config = window[me.configSource].swissupRecaptcha;
            }

            $.async(
                {
                    selector: '.payment-method._active',
                    ctx: $('.form.payments').get(0)
                },
                me.moveRecaptcha.bind(me)
            );

            quote.paymentMethod.subscribe(function (paymentMethod) {
                var paymentMethodElement = getPaymentMethodElement(paymentMethod.method);

                me.moveRecaptcha(paymentMethodElement);
            }, me);
        },

        /**
         * @param  {HTMLElement} element
         */
        initRecaptchaElement: function (element) {
            recaptchaElement = element;
            $(recaptchaElement).removeAttr('data-bind');
        },

        /**
         * Move recaptcha into payment method element.
         *
         * @param  {HTMLElement} target
         */
        moveRecaptcha: function (target) {
            var recaptchaWidget = $(recaptchaElement).data('swissupRecaptcha'),
                destination;

            if (!target || target === lastTarget) {
                return;
            }

            lastTarget = target;
            destination = $('.checkout-agreements-block, .afterpay-checkout-note', target);

            if (!destination.length) {
                // fallback for some customized checkouts
                destination = $('.payment-method-' + quote.paymentMethod().method + ' .checkout-agreements-block');
            }

            if (!destination.length) {
                return;
            }

            destination.first().append(recaptchaElement);

            // initialize recaptcha widget after move if still not inited
            if (!recaptchaWidget) {
                Recaptcha(config[this.formId], recaptchaElement);
            } else {
                recaptchaWidget.reset();
            }
        }
    });
});
