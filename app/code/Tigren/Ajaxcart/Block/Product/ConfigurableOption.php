<?php
/**
 * @copyright Copyright (c) 2017 www.tigren.com
 */

namespace Tigren\Ajaxcart\Block\Product;

/**
 * Class ConfigurableOption
 * @package Tigren\Ajaxcart\Block\Product
 */
class ConfigurableOption extends \Magento\Framework\View\Element\Template
{
    protected $_registry;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,       
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {       
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getColorLabel()
    {
        return $this->_request->getParam('colorLabel');
    }

    /**
     * @return mixed
     */
    public function getSizeLabel()
    {
        return $this->_request->getParam('sizeLabel');
    }

    public function getSuperAttributes()
    {
    //echo '<pre>';print_r($this->_request->getParams());
        return $this->_request->getParam('super_attribute');
    }
    public function getProductId()
    {
    return $this->_request->getParam('product');
    }


}