<?php

namespace Magenest\SagepayUS\Controller\Card;

use Magenest\SagepayUS\Controller\Card;
use Magento\Framework\Controller\ResultFactory;

class Save extends Card
{
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_onestepcheckout/general/logger');

        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $sageResp = $this->getRequest()->getParam('sage_resp');
        $sageHash = $this->getRequest()->getParam('sage_hash');
        $sageCardInfo = $this->getRequest()->getParam('sage_cardinfo');
       
        //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/checkout_stuck.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Checkout app/code/Magenest/SagepayUS/Controller/Card/Save: SageCardInfo start');
            $logger->info($sageCardInfo);
            $logger->info('Checkout app/code/Magenest/SagepayUS/Controller/Card/Save: SageCardInfo end');
        }

        if($this->_customerSession->isLoggedIn()){
            if($this->configHelper->validateResponse($sageResp, $sageHash)){
                $payRespObject = json_decode($sageResp, true);
                $responseObject = @$payRespObject['gatewayResponse'];
                if($this->configHelper->saveCard(@$responseObject['vaultResponse'], $sageCardInfo)) {
                    return $result->setData([
                        'error' => false,
                        'success' => true
                    ]);
                }else{
                    return $result->setData([
                        'error' => true,
                        'success' => false,
                        'message' => __("Operation is not allow")
                    ]);
                }
            }else{
                return $result->setData([
                    'error' => true,
                    'success'=> false,
                    'message' => __("Invalid response")
                ]);
            }
        }else{
            return $result->setData([
                'error' => true,
                'success'=> false,
                'message' => __("Customer is not logged in")
            ]);
        }
    }
}
