<?php
/**
 * Created by PhpStorm.
 * User: magenest
 * Date: 12/03/2017
 * Time: 15:13
 */

namespace Magenest\SagepayUS\Controller\Adminhtml\Checkout;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;

class Request extends \Magento\Backend\App\Action
{
    protected $configHelper;
    protected $_storeManager;

    public function __construct(
        Action\Context $context,
        \Magenest\SagepayUS\Helper\ConfigHelper $configHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context);
        $this->configHelper = $configHelper;
        $this->_storeManager = $storeManager;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $jsonFactory = $this->resultFactory->create('json');
        if ($this->getRequest()->isAjax()) {
            $quote = $this->_getOrderSession()->getQuote();
            $isToken = $this->getRequest()->getParam('is_token');
            $cardId = $this->getRequest()->getParam('card_id');
            /** @var \Magento\Customer\Model\Session $customer */
            $customer = $this->_objectManager->create('Magento\Customer\Model\Session');
            $customerId = $customer->getCustomerId();
            $cardToken = '';
            $payAction = $this->configHelper->getPaymentAction();
            $nonces = $this->configHelper->getNonces();
            $merchant = [
                "ID" => $this->configHelper->getMerchantId(),
                "KEY" => $this->configHelper->getMerchantKey()
            ];
            // sign up at https://developer.sagepayments.com/ to get your own dev creds
            $developer = [
                "ID" => $this->configHelper->getDeveloperId(),
                "KEY" => $this->configHelper->getDeveloperKey()
            ];
            $req = [
                "merchantId" => $merchant['ID'],
                "merchantKey" => $merchant['KEY'],
                "requestType" => "payment",
                "orderNumber" => (string)time(),
                "amount" => $quote->getBaseGrandTotal(),// use 5.00 to simulate a decline
                "salt" => $nonces['salt'],
                "postbackUrl" => $this->_storeManager->getStore()->getBaseurl() . 'sagepayus/checkout/response',
                "preAuth" => (bool)($payAction=='authorize'),
                "doVault" => false,
                "data" => $quote->getBaseGrandTotal(),
            ];
            if($cardToken){
                $req['token'] = $cardToken;
            }
            if($isToken){
                unset($req['amount']);
                unset($req['preAuth']);
                unset($req['token']);
                unset($req['data']);
                $req['doVault'] = true;
                $req['requestType'] = "vault";
            }

            $authKey = $this->configHelper->getAuthKey(
                json_encode($req),
                $developer['KEY'],
                $nonces['salt'],
                $nonces['iv']
            );
            unset($req['merchantKey']);
            $dataReturn = [
                'error' => false,
                'success' => true,
                'authKey' => $authKey,
                'clientId' => $developer['ID'],
                'environment' => $this->configHelper->getEnvironment(),
                'debug' => (bool)(int)$this->configHelper->getSageBrowserDebug()
            ];

            return $jsonFactory->setData(array_merge($dataReturn, $req));
        }

        return $jsonFactory->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }

    protected function _getOrderSession()
    {
        return $this->_objectManager->get(\Magento\Backend\Model\Session\Quote::class);
    }
}
