<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 17/10/2016
 * Time: 16:09
 */

namespace Magenest\SagepayUS\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Sales\Model\Order;

class SagepayUSPayment extends \Magento\Payment\Model\Method\Cc
{
    const CODE = 'magenest_sagepayus';

    protected $_code = self::CODE;
    protected $_isGateway = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canRefund = false;
    protected $_canVoid = false;
    protected $_canUseInternal = true;
    protected $customerSession;
    protected $backendAuthSession;
    protected $sessionQuote;
    protected $checkoutSession;
    protected $quoteRepository;
    protected $quoteManagement;
    protected $_messageManager;
    protected $checkoutData;
    protected $configHelper;
    protected $sageLogger;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Customer\Model\Session $session,
        \Magento\Backend\Model\Auth\Session $backendAuthSession,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        \Magenest\SagepayUS\Helper\ConfigHelper $configHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Helper\Data $checkoutData,
        \Magenest\SagepayUS\Helper\Logger $sageLogger,

        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        $this->_customerSession = $session;
        $this->_curlFactory = $curlFactory;
        $this->backendAuthSession = $backendAuthSession;
        $this->sessionQuote = $sessionQuote;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->quoteManagement = $quoteManagement;
        $this->_messageManager = $messageManager;
        $this->checkoutData = $checkoutData;
        $this->sageLogger = $sageLogger;
    }

    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $_tmpData = $data->_data;
        $additionalDataRef = $_tmpData['additional_data'];
        if($additionalDataRef){
            $sageResp = $additionalDataRef['sage_resp'];
            $sageHash = $additionalDataRef['sage_hash'];
            $sageCardInfo = $additionalDataRef['sage_cardinfo'];
            $saveCardCheckbox = $additionalDataRef['sage_savecard'];

            $infoInstance = $this->getInfoInstance();
            $infoInstance->setAdditionalInformation('sage_resp', $sageResp);
            $infoInstance->setAdditionalInformation('sage_hash', $sageHash);
            $infoInstance->setAdditionalInformation('sage_cardinfo', $sageCardInfo);
            $infoInstance->setAdditionalInformation('sage_savecard', $saveCardCheckbox);
        }    
        return $this;
    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->sageLogger->debug("Authorize order: ".$payment->getOrder()->getIncrementId());
        $this->performPayment($payment, $amount);
        return parent::authorize($payment, $amount);
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface|\Magento\Sales\Model\Order\Payment $payment
     * @param float $amount
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_onestepcheckout/general/logger');
        //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/checkout_stuck.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: capture- : ');
        }

        $this->sageLogger->debug("Capture order: ".$payment->getOrder()->getIncrementId());
        $this->sageLogger->debug("Capture Called At: ".time());
        $this->sageLogger->debug("Capture Called Saved: ".$this->checkoutSession->getCaptureCalledAt());
        //START: Preventing from duplicate charges.
        $isFiveMinutesAgo = false;
        if (!$this->checkoutSession->getCaptureCalledAt()) {
            $this->checkoutSession->setCaptureCalledAt(time());
        } else {
            $minutes = (time() - $this->checkoutSession->getCaptureCalledAt()) / 60;
            if ($minutes <= 5) {
                $isFiveMinutesAgo = true;
            }
        }

        if ($isFiveMinutesAgo) {

            //Log the request
            if($isCheckoutLoggerEnabled) {
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: Something went wrong, Please try again after sometime.');
            }

            $this->sageLogger->debug("Capture order failed as duplicate charge detected.");
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong, Please try again after sometime.')
            );
        }
        //END: Preventing from duplicate charges.
        $this->performPayment($payment, $amount);
        return parent::capture($payment, $amount); // TODO: Change the autogenerated stub
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface|\Magento\Sales\Model\Order\Payment $payment
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function performPayment($payment, $amount){
        /**
         * @var \Magento\Sales\Model\Order $order
         * @var \Magento\Sales\Model\Order\Address $billing
         */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_onestepcheckout/general/logger');
        //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/checkout_stuck.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: performPayment- : START Credit card log ');
        }

        $sageResp = $payment->getAdditionalInformation('sage_resp');
        $sageHash = $payment->getAdditionalInformation('sage_hash');
        $sageCardInfo = $payment->getAdditionalInformation('sage_cardinfo');
        $payRespObject = json_decode($sageResp, true);
        $responseObject = @$payRespObject['gatewayResponse'];
        $amountValidate = @$payRespObject['data'];
        if($this->_appState->getAreaCode() == 'adminhtml'){
            $responseObject = json_decode(@$payRespObject['Response'], true);
            $amountValidate = @$payRespObject['Data'];
        }
        $this->sageLogger->debug(var_export($payRespObject, true));
        if (@$responseObject['status'] == 'Approved') {
            $transactionId = @$responseObject['transactionId'];
            $payment->setCcAvsStatus(@$responseObject['avsResult']);
            $payment->setCcSecureVerify(@$responseObject['cvvResult']);
            $payment->setTransactionId($transactionId);
            $payment->setLastTransId($transactionId);
            $payment->setShouldCloseParentTransaction(1);
            $payment->setIsTransactionClosed(1);
            $this->savePayaResponse($payment, $responseObject);
            $payment->setAdditionalInformation("paya_amount_paid", $amountValidate);
            $hashCheck = "Pass";
            if (bccomp($amountValidate, $amount, 2) != 0) {
                //submit fraud order if amount is not equal
                $payment->setIsFraudDetected(true);
                //Log the request
                if($isCheckoutLoggerEnabled) {
                    $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: Fraud order detected');
                }
            }
            if(!$this->_appState->getAreaCode() == 'adminhtml') {
                if (!$this->configHelper->validateResponse($sageResp, $sageHash)) {
                    //submit fraud order if hash is not equal
                    $hashCheck = "Fail";
                    $payment->setIsFraudDetected(true);
                } else {
                    if (isset($responseObject['vaultResponse'])) {
                        $saveCardCheckbox = $payment->getAdditionalInformation("sage_savecard");
                        $this->configHelper->saveCard($responseObject['vaultResponse'], $sageCardInfo,
                            $saveCardCheckbox);
                    }
                }
            }
            $payment->setAdditionalInformation("paya_hash_check", $hashCheck);

            //Log the request
            if($isCheckoutLoggerEnabled) {
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: TransactionId- '.$transactionId);
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: amount- '.$amount);
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: amountValidate- '.$amountValidate);
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: sageCardInfo- '.$sageCardInfo);
            }

        } else {
            if(@$responseObject['message']) {
                $this->_messageManager->addErrorMessage(@$responseObject['message']);
            }
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Payment Declined')
            );
            //Log the request
            if($isCheckoutLoggerEnabled) {
                $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: Payment Declined- ');
            }
        }
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface|\Magento\Sales\Model\Order\Payment $payment
     */
    private function savePayaResponse($payment, $responseObject){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $isCheckoutLoggerEnabled = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_onestepcheckout/general/logger');
        //Log the request
        if($isCheckoutLoggerEnabled) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/checkout_stuck.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: savePayaResponse- : Start Paya response');
        }

        $listParams = ['status', 'reference', 'message', 'code', 'cvvResult', 'avsResult', 'riskCode', 'networkId', 'isPurchaseCard', 'orderNumber', 'transactionId'];
        $data = [];
        foreach ($responseObject as $k => $v){
            if(in_array($k, $listParams)){
                $data[] = [
                   'label' => $k,
                   'value' => $v
                ];;
                //Log the request
                if($isCheckoutLoggerEnabled) {
                    $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: Key- '.$k.' --Value- '. $v);
                }
            }
        }
        //Log the request
        if($isCheckoutLoggerEnabled) {
            $logger->info('Checkout app/code/Magenest/SagepayUS/Model/SagepayUSPayment: savePayaResponse End');
        }
        $payment->setAdditionalInformation("paya_response_data", json_encode($data));
    }

    public function validate()
    {
        return AbstractMethod::validate();
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        return AbstractMethod::isAvailable($quote);
    }
}
