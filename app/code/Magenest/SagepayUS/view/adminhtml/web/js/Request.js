define(['jquery', 'PayJS/Core', 'PayJS/Formatting'], function($, CORE, FORMATTING) {
    var _spsJQ = $.noConflict(true);

    var restfulHeaders = { "Content-Type": "application/json" };

    function addHeaders() {
        restfulHeaders["clientId"] = CORE.getApiKey();
        restfulHeaders["clientVersion"] = "@@VERSION".replace(/\./g, '');
    }

    function getUrl(type) {
        var BASE_URL = CORE.getApiUrl();
        const vaultPath = "/api/vault/card",
            transactionPath = "/api/payment/card",
            vaultTransactionPath = "/api/payment/token";

        if (CORE.getEnvironment() == "cert") {
            BASE_URL = BASE_URL.replace("api.sage", "api-cert.sage");
        }

        switch (type) {
            case "vaultPath":
                return BASE_URL + vaultPath;
                break;
            case "transactionPath":
                return BASE_URL + transactionPath;
                break;
            case "vaultTransactionPath":
                return BASE_URL + vaultTransactionPath;
                break;
            default:
                break;
        }
    }

    const lastCreditCard = {};

    function savePaymentDetails(cc, exp) {
        cc = FORMATTING.stripNonNumeric(cc);
        lastCreditCard.cardType = cc.charAt(0);
        lastCreditCard.BIN = cc.slice(0, 6);
        lastCreditCard.lastFourDigits = cc.slice(-4);
        const maskedNumber = FORMATTING.maskCreditCardNumber(cc);
        lastCreditCard.maskedNumber = maskedNumber;
        lastCreditCard.expirationDate = exp;
        cc = null;
    }

    return {
        doPayment: function(cc, exp, cvv, callback) {
            CORE.Log("Building transaction payload.", "Request", "log");
            savePaymentDetails(cc, exp);
            addHeaders();
            var url = getUrl("transactionPath");
            var transactionPayload = {
                "merchantId": CORE.getMerchantId(),
                "authKey": CORE.getAuthKey(),
                "requestId": CORE.getRequestId(),
                "cardExpirationDate": FORMATTING.stripNonNumeric(exp),
                "cardNumber": FORMATTING.stripNonNumeric(cc),
                "amount": CORE.getAmount(),
                "cvv": FORMATTING.stripNonNumeric(cvv),
                "nonce": CORE.getNonce(),
            };

            cc = null;
            exp = null;
            cvv = null;

            // use this convention for optional items
            if (customData = CORE.getCustomData()) { transactionPayload["data"] = customData }
            if (pbUrl = CORE.getPostbackUrl()) { transactionPayload["postbackUrl"] = pbUrl }
            if (billing = CORE.getBilling()) { transactionPayload["billing"] = billing }
            if (customer = CORE.getCustomer()) { transactionPayload["customer"] = customer }
            if (shipping = CORE.getShipping()) { transactionPayload["shipping"] = shipping }
            if (isPreAuth = CORE.getPreAuth()) { transactionPayload["preAuth"] = isPreAuth }
            if (taxAmount = CORE.getTaxAmount()) { transactionPayload["taxAmount"] = taxAmount }
            if (shippingAmount = CORE.getShippingAmount()) { transactionPayload["shippingAmount"] = shippingAmount }
            if (level2 = CORE.getLevel2()) { transactionPayload["level2"] = level2 }
            if (level3 = CORE.getLevel3()) { transactionPayload["level3"] = level3 }
            if (doVault = CORE.getDoVault()) { transactionPayload["doVault"] = doVault }
            if (isRecurring = CORE.getIsRecurring()) { transactionPayload["isRecurring"] = isRecurring }
            if (recurringSchedule = CORE.getRecurringSchedule()) { transactionPayload["recurringSchedule"] = recurringSchedule }

            CORE.Log("Sending request to gateway.", "Request", "log");
            _spsJQ.ajax({
                async: true,
                method: "POST",
                url: url,
                headers: restfulHeaders,
                data: JSON.stringify(transactionPayload),
                crossDomain: true,
                dataType: "text",
                beforeSend: function(xhr){
                    //Empty to remove magento's default handler
                }
            }).done(function(data, textStatus, jqXHR) {
                CORE.Log("$.ajax.done", "Request", "log");
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(data, textStatus, jqXHR);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                CORE.Log("$.ajax.fail", "Request", "error");
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(jqXHR, textStatus, errorThrown);
                }
            }).always(function() {
                transactionPayload = null;
            });;
        },
        doVault: function(cc, exp, callback) {
            CORE.Log("Building vault payload.", "Request", "log");
            savePaymentDetails(cc, exp);
            addHeaders();
            var url = getUrl("vaultPath");
            var vaultPayload = {
                "merchantId": CORE.getMerchantId(),
                "authKey": CORE.getAuthKey(),
                "requestId": CORE.getRequestId(),
                "cardExpirationDate": FORMATTING.stripNonNumeric(exp),
                "cardNumber": FORMATTING.stripNonNumeric(cc),
                "nonce": CORE.getNonce(),
            };

            cc = null;
            exp = null;

            // use this convention for optional items
            if (customData = CORE.getCustomData()) { vaultPayload["data"] = customData }
            if (pbUrl = CORE.getPostbackUrl()) { vaultPayload["postbackUrl"] = pbUrl }

            CORE.Log("Sending request to gateway.", "Request", "log");
            _spsJQ.ajax({
                async: true,
                method: "POST",
                url: url,
                headers: restfulHeaders,
                data: JSON.stringify(vaultPayload),
                crossDomain: true,
                dataType: "text"
            }).done(function(data, textStatus, jqXHR) {
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(data, textStatus, jqXHR);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                CORE.Log("$.ajax.fail", "Request", "error");
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(jqXHR, textStatus, errorThrown);
                }
            }).always(function() {
                vaultPayload = null;
            });;
        },
        doTokenPayment: function (token, cvv, callback) {
            CORE.Log("Building transaction payload.", "Request", "log");
            addHeaders();
            var url = getUrl("vaultTransactionPath");
            var transactionPayload = {
                "merchantId": CORE.getMerchantId(),
                "authKey": CORE.getAuthKey(),
                "requestId": CORE.getRequestId(),
                "token": FORMATTING.stripNonAlphanumeric(token),
                "amount": CORE.getAmount(),
                "cvv": FORMATTING.stripNonNumeric(cvv),
                "nonce": CORE.getNonce(),
            };

            cvv = null;

            // use this convention for optional items
            if (customData = CORE.getCustomData()) { transactionPayload["data"] = customData }
            if (pbUrl = CORE.getPostbackUrl()) { transactionPayload["postbackUrl"] = pbUrl }
            if (billing = CORE.getBilling()) { transactionPayload["billing"] = billing }
            if (customer = CORE.getCustomer()) { transactionPayload["customer"] = customer }
            if (shipping = CORE.getShipping()) { transactionPayload["shipping"] = shipping }
            if (isPreAuth = CORE.getPreAuth()) { transactionPayload["preAuth"] = isPreAuth }
            if (taxAmount = CORE.getTaxAmount()) { transactionPayload["taxAmount"] = taxAmount }
            if (shippingAmount = CORE.getShippingAmount()) { transactionPayload["shippingAmount"] = shippingAmount }
            if (level2 = CORE.getLevel2()) { transactionPayload["level2"] = level2 }
            if (level3 = CORE.getLevel3()) { transactionPayload["level3"] = level3 }
            if (isRecurring = CORE.getIsRecurring()) { transactionPayload["isRecurring"] = isRecurring }
            if (recurringSchedule = CORE.getRecurringSchedule()) { transactionPayload["recurringSchedule"] = recurringSchedule }

            CORE.Log("Sending request to gateway.", "Request", "log");
            _spsJQ.ajax({
                async: true,
                method: "POST",
                url: url,
                headers: restfulHeaders,
                data: JSON.stringify(transactionPayload),
                crossDomain: true,
                dataType: "text"
            }).done(function(data, textStatus, jqXHR) {
                CORE.Log("$.ajax.done", "Request", "log");
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(data, textStatus, jqXHR);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                CORE.Log("$.ajax.fail", "Request", "error");
                if (typeof callback === "function") {
                    CORE.Log("Executing callback.", "Request", "log");
                    callback(jqXHR, textStatus, errorThrown);
                }
            }).always(function() {
                transactionPayload = null;
            });
        },
        getLastCard: function() { return lastCreditCard; } // aka RESPONSE.getPaymentDetails();
    }

});