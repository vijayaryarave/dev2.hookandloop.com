var config = {
    paths: {
        "PayJS/UI": 'Magenest_SagepayUS/js/view/payment/method-renderer/UI',
        "PayJS/Core": 'Magenest_SagepayUS/js/view/payment/method-renderer/Core',
        "PayJS/Request": 'Magenest_SagepayUS/js/view/payment/method-renderer/Request',
        "PayJS/Response": 'Magenest_SagepayUS/js/view/payment/method-renderer/Response',
        "PayJS/Formatting": 'Magenest_SagepayUS/js/view/payment/method-renderer/Formatting',
        "PayJS/Validation": 'Magenest_SagepayUS/js/view/payment/method-renderer/Validation',
        "PayJS/Extensions": 'Magenest_SagepayUS/js/view/payment/method-renderer/Extensions',
        "PayJS/UI.html": 'Magenest_SagepayUS/js/view/payment/method-renderer/UI.html',
        "PayJS/UI.text": 'Magenest_SagepayUS/js/view/payment/method-renderer/UI.text'
    },
    shim: {
        'PayJS/UI': {
            deps: ['jquery']
        },
        'PayJS/Core': {
            deps: ['jquery']
        },
        'PayJS/Request': {
            deps: ['jquery']
        },
        'PayJS/Response': {
            deps: ['jquery']
        },
        'PayJS/Formatting': {
            deps: ['jquery']
        },
        'PayJS/Validation': {
            deps: ['jquery']
        },
        'PayJS/Extensions': {
            deps: ['jquery']
        },
        'PayJS/UI.html': {
            deps: ['jquery']
        },
        'PayJS/UI.text': {
            deps: ['jquery']
        }
    }
};