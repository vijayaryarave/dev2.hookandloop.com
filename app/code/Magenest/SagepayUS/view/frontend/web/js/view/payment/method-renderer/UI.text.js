/*!
   Sage Payment Solutions - PaymentsJS
   Build: 1.0.2.000009 / prod / 2017-04-04
*/

define(["PayJS/Core"],function(a){const b="{~{PAYJS}~}",c={en:{shared:{labelText:{bankcard:{pan:"Number",exp:{month:"Expiration Month",year:"Expiration Year"},cvv:"Security (or CVV) Code"},ach:{}}},payment:{modalTitle:"Payment Details",resultText:{success:"Thank you for your payment.",fail:"Please try again or call us for help.",failWithPhoneNumber:"Please try again or call us at {~{PAYJS}~}.",error:"Please call us to get the payment status.",errorWithPhoneNumber:"Please call us at {~{PAYJS}~} to get the payment status."}},vault:{modalTitle:"Credit Card Details",resultText:{success:"Thank you.",fail:"Please try again or call us for help.",failWithPhoneNumber:"Please try again or call us at {~{PAYJS}~}.",error:"Please call us to get the payment status.",errorWithPhoneNumber:"Please call us at {~{PAYJS}~} to get the payment status."}}},es:{},fr:{}};return{getStrings:function(b){return b?c[b.toLowerCase()]:c[a.getLanguage()]},replaceTemplate:function(a,c){return a.replace(b,c)}}});
//# sourceMappingURL=UI.text.js.map