<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Api;


interface PayaFormInterface
{
    /**
     * Returns url form paya
     *
     * @api
     * @return string url form paya.
     */
    public function request();
}
