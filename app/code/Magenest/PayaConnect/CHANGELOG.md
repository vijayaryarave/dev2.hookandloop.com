# Change Log
All notable changes to this extension will be documented in this file.
This extension adheres to [Magenest](http://magenest.com/).

## [1.0.0] - 2020-05-12
### Added
1. Allow customers to checkout using Pay Paya Form
2. Allow customers to save card with Account Paya Form
3. Allow customers to save cards with checkout
4. Allow merchants to view transaction detail
5. Allow merchants to capture, refund or void transactions
