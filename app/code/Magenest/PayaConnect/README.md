# README
Thank you for buying our product.
This extension adheres to [Magenest](https://magenest.com/).

## Installation
1. Upload the extension to your app/code/Magenest/PayaConnect
2. Run bin/magento setup:upgrade to install the module
3. Run bin/magento setup:di:compile to generate dependency code

## User guide
- For detailed user guide of this extension, please visit: https://confluence.izysync.com/pages/viewpage.action?pageId=25236584

- Support portal:  https://servicedesk.izysync.com/servicedesk/customer/portal/146

- All the updates of this module are included in CHANGELOG.md file.
