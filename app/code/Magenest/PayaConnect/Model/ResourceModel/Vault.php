<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Vault extends AbstractDb
{
    public function _construct()
    {
        $this->_init('magenest_paya_vault', 'id');
    }
}
