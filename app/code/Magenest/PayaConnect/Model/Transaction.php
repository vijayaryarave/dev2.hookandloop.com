<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model;


use Magento\Framework\Model\AbstractModel;

/**
 * Class Transaction
 * @package Magenest\PayaConnect\Model
 */
class Transaction extends AbstractModel
{
    /**
     * @var \Magenest\PayaConnect\Helper\Logger
     */
    protected $_payaLogger;

    /**
     * @var ResourceModel\Transaction
     */
    protected $_transactionResource;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * Transaction constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\PayaConnect\Helper\Logger $payaLogger
     * @param ResourceModel\Transaction $transactionResource
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\PayaConnect\Helper\Logger $payaLogger,
        \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_payaLogger = $payaLogger;
        $this->_transactionResource = $transactionResource;
        $this->_messageManager = $messageManager;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\PayaConnect\Model\ResourceModel\Transaction');
    }

    /**
     * @param $arrayData
     * @param $payment
     * @param $paymentGateway
     */
    public function saveTransactionOrder($arrayData, $payment, $paymentGateway)
    {
        $transactionModel = $this;
        $transactionData = [
            'transaction_id' => $arrayData['id'],
            'order_id' => $payment->getOrder()->getIncrementId(),
            'customer_id' => $payment->getOrder()->getData('customer_id'),
            'transaction_status' => $arrayData['status_id'],
            'payment_method' => $paymentGateway,
            'card_type' => $arrayData['account_type'],
            'transaction_amount' => $arrayData['transaction_amount'],
            'customer_email' =>  $payment->getOrder()->getCustomerEmail()
        ];
        $transactionModel->setData($transactionData);
        try {
            $this->_transactionResource->save($transactionModel);
        } catch (\Exception $e) {
            $this->_payaLogger->debug("PayaAccountForm Transaction ERROR:" . $paymentGateway . $payment->getOrder()->getIncrementId());
            $this->_payaLogger->debug(var_export($e->getMessage(), true));
        }
    }

    /**
     * @param $transactionId
     * @param $statusId
     */
    public function editStatusTransaction($transactionId, $statusId) {
        $transactionModel = $this;
        $transactionResource = $this->_transactionResource;
        $transactionResource->load($transactionModel, $transactionId, 'transaction_id');
        $transactionModel->setData('transaction_status', $statusId);
        try {
            $transactionResource->save($transactionModel);
        } catch (\Exception $e) {
            $this->_messageManager->addError(__('PayaAccountForm Transaction Status Not Success'));
        }
    }
}
