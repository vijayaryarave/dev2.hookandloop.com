<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * paya235 extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_paya235
 */

namespace Magenest\PayaConnect\Model\Source;


use Magento\Framework\Option\ArrayInterface;
use Magenest\PayaConnect\Model\Constant;

class SandboxMode implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => Constant::SANDBOX,
                'label' => __('Sandbox')
            ],            [
                'value' => Constant::PRODUCTION,
                'label' => __('Production'),
            ]
        ];
    }
}
