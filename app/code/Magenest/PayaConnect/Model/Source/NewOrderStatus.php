<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Sales\Model\Order;

class NewOrderStatus implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => Order::STATE_PROCESSING,
                'label' => __('Processing')
            ],            [
                'value' => Order::STATUS_FRAUD,
                'label' => __('Suspected Fraud'),
            ]
        ];
    }
}
