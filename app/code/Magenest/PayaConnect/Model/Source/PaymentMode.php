<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class PaymentMode implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => 'custom',
                'label' => __('Magento Custom UI'),
            ],
            [
                'value' => 'modal',
                'label' => __('Sagepay Modal UI')
            ],
            [
                'value' => 'inline',
                'label' => __('Sagepay Inline UI')
            ]
        ];
    }
}
