<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model\Ui;

use Magenest\PayaConnect\Model\PayaConnect;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magenest\PayaConnect\Helper\ConfigHelper;

/**
 * Class ConfigProvider
 * @package Magenest\PayaConnect\Model\Ui
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     *
     */
    const CODE = PayaConnect::CODE;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $_config;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    protected $_jsonHelper;

    protected $_vault;

    /**
     * ConfigProvider constructor.
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $_config
     */
    public function __construct(
        \Magenest\PayaConnect\Helper\ConfigHelper $_config,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magenest\PayaConnect\Model\Vault $vault
    )
    {
        $this->_config = $_config;
        $this->_customerSession = $customerSession;
        $this->_jsonHelper = $jsonHelper;
        $this->_vault = $vault;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $cardData = $this->_vault->getDataCard();
        return [
            'payment' => [
                self::CODE => [
                    'is_save_card' => $this->_config->getValueConfig(ConfigHelper::CAN_SAVE_CARD) ? true : false,
                    'paya_curl' => $this->_config->getCURL(),
                    'hasCard' => count($cardData)>0 ? true:false,
                    'isLogin' => $this->_customerSession->isLoggedIn(),
                    'saveCards' => $this->_jsonHelper->jsonEncode($cardData),
                    'checkCvv' => $this->_config->getValueConfig(ConfigHelper::CHECK_CVV) ? true : false
                ]
            ]
        ];
    }
}
