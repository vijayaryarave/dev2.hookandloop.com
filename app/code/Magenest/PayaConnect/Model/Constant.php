<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_payaconnect
 */

namespace Magenest\PayaConnect\Model;


class Constant
{
    const STATUS_ID_CAPTURE = '101';
    const STATUS_ID_AUTHORIZE = '102';
    const STATUS_ID_REFUND = '111';
    const STATUS_ID_VOID = '201';
    const ENDPOINT_PAYMENT_FORM = 'payform';
    const ENDPOINT_ACCOUNT_FORM = 'accountform';
    const ENDPOINT_ACCOUNT_VAULTS = 'accountvaults';
    const ENDPOINT_TRANSACTION = 'transactions';
    const ENDPOINT_CONTACT = 'contacts';
    const CURL_SANDBOX = 'https://api.sandbox.payaconnect.com';
    const CURL_LIVE = 'https://api.payaconnect.com';
    const RESPONSE_CVV_MATCH = 'M';
    const DEVELOPER_ID = '488585c6';
    const SANDBOX = 1;
    const PRODUCTION = 0;
}
