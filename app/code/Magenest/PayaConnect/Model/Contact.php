<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model;

use Magento\Framework\Model\AbstractModel;

class Contact extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Magenest\PayaConnect\Model\ResourceModel\Contact');
    }
}
