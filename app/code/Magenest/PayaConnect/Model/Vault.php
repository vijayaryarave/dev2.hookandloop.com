<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Vault
 * @package Magenest\PayaConnect\Model
 */
class Vault extends AbstractModel
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Vault constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\PayaConnect\Helper\Logger $payaLogger
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\PayaConnect\Helper\Logger $payaLogger,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\PayaConnect\Model\ResourceModel\Vault');
    }

    /**
     * @param $customerId
     * @return bool
     */
    public function isOwnCard($customerId){
        return $this->getCustomerId() == $customerId;
    }

    /**
     * @param null $customerId
     * @return array
     */
    public function getDataCard($customerId = null)
    {
        $customerSession = $this->_customerSession;
        try {
            if (!$customerId) {
                $customerId = $customerSession->getCustomerId();
            }
            $model = $this
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId);

            $dataOut = [];
            foreach ($model as $instace) {
                $dataOut[] = [
                    'card_id' => $instace->getId(),
                    'last4' => $instace->getData('last_four_digits'),
                    'brand' => $instace->getData('card_type'),
                ];
            }
            return $dataOut;
        } catch (\Exception $e) {
            return [];
        }
    }
}
