<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model;


use Magenest\PayaConnect\Helper\ConfigHelper;
use Magenest\PayaConnect\Helper\DataHelper;
use Magenest\PayaConnect\Model\Constant;
use Magento\Payment\Model\Method\Cc;

/**
 * Class PayaConnect
 * @package Magenest\PayaConnect\Model
 */
class PayaConnect extends Cc
{
    /**
     *
     */
    const CODE = 'magenest_payaconnect';

    /**
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var bool
     */
    protected $_canAuthorize = true;
    /**
     * @var bool
     */
    protected $_canCapture = true;
    /**
     * @var bool
     */
    protected $_canUseInternal = false;

    /**
     * @var bool
     */
    protected $_isGateway = true;

    /**
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * @var bool
     */
    protected $_canVoid = true;

    /**
     * @var \Magenest\PayaConnect\Helper\Logger
     */
    protected $_payaLogger;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curlClient;

    /**
     * @var ConfigHelper
     */
    protected $_configHelper;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;

    /**
     * @var ContactFactory
     */
    protected $_contactFactory;

    /**
     * @var ResourceModel\Contact
     */
    protected $_contactResource;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;

    /**
     * @var Transaction
     */
    protected $_transaction;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_managerInterface;

    /**
     * PayaConnect constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magenest\PayaConnect\Helper\Logger $payaLogger
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magenest\PayaConnect\Helper\DataHelper $jsonHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magenest\PayaConnect\Helper\Logger $payaLogger,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magenest\PayaConnect\Helper\DataHelper $dataHelper,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magenest\PayaConnect\Model\ContactFactory $contactFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Contact $contactResource,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magenest\PayaConnect\Model\Transaction $transaction,
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_payaLogger = $payaLogger;
        $this->_jsonHelper = $jsonHelper;
        $this->_dataHelper = $dataHelper;
        $this->curlClient = $curl;
        $this->_configHelper = $configHelper;
        $this->_authSession = $authSession;
        $this->_contactFactory = $contactFactory;
        $this->_contactResource = $contactResource;
        $this->_moduleManager = $moduleManager;
        $this->_transaction = $transaction;
        $this->_managerInterface = $managerInterface;
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $moduleList, $localeDate, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Magento\Payment\Model\Method\AbstractMethod|Cc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate()
    {
        return \Magento\Payment\Model\Method\AbstractMethod::validate();
    }

    /**
     * @param \Magento\Framework\DataObject $data
     * @return $this|Cc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);
        $this->_debug("Function: assignData");
        $infoInstance = $this->getInfoInstance();
        $additionalData = $data->getData('additional_data');
        $infoInstance->setAdditionalInformation('payment_action', $this->_configHelper->getConfigPaymentAction());
        $infoInstance->setAdditionalInformation('card_id', null);

        try {
            //payment with card save (all frontend and admin)
            if (isset($additionalData['cardId']) && $additionalData['cardId'] != 0) {
                $infoInstance->setAdditionalInformation('card_id', $additionalData['cardId']);
                $infoInstance->setAdditionalInformation('cvv', isset($additionalData['cvv']) ? $additionalData['cvv'] : $additionalData['cc_cid']);
                return $this;
            }

            //payment with new card in admin
            if ($this->_appState->getAreaCode() == 'adminhtml') {
                //unset cardId (because order by new card will not have cardId)
                unset($additionalData['cardId']);
                //check card information
                foreach ($additionalData as $item) {
                    if (!$item) {
                        throw new \Magento\Framework\Webapi\Exception(__('Unknown card information'));
                    }
                }
                $infoInstance->setAdditionalInformation('card_encode', base64_encode($this->_jsonHelper->jsonEncode($additionalData)));
                $infoInstance->setAdditionalInformation('create_by_admin', true);
            } else {
                $transactionData = $this->_jsonHelper->jsonDecode($additionalData['transaction_infor']);

                $infoInstance->setAdditionalInformation('transaction_response', $additionalData['transaction_infor']);

                if ($transactionData['account_type'] == 'visa') {
                    $transactionData['account_type'] = 'vi';
                }
                $infoInstance->addData(
                    [
                        'cc_type' => isset($transactionData['account_type'])?strtoupper($transactionData['account_type']):"",
                        'cc_last_4' => isset($transactionData['last_four'])?$transactionData['last_four']:"",
                    ]
                );

                //check save and save card
                if (isset($additionalData['saved']) && $additionalData['saved'] != null) {
                    $this->_dataHelper->saveCard($transactionData['id']);
                }
            }

            return $this;
        } catch (\Exception $e) {
            throw new \Magento\Framework\Webapi\Exception(__('Unknown card information'));
        }
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return Cc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->_payaLogger->debug("Capture order: ".$payment->getOrder()->getIncrementId());
        //capture online when admin submit invoice
        if ($payment->getAdditionalInformation('transaction_response')) {
            $additionalData = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
            $transactionStatus = $additionalData['status_id'];
            if ($transactionStatus == Constant::STATUS_ID_AUTHORIZE) {
                $linkTransaction = $additionalData['_links']['self']['href'];

                $params = [
                    "transaction" => [
                        "action" => "authcomplete",
                        "transaction_amount" => $amount,
                    ]
                ];
                //send request to capture payment Paya
                $response = $this->_dataHelper->sendRequest($linkTransaction, $this->_jsonHelper->jsonEncode($params), 'PUT');
                if (!isset($response['errors'])) {
                    $this->performPayment($payment, $amount);
                    $this->_transaction->editStatusTransaction($response['transaction']['id'], $response['transaction']['status_id']);
                    $this->_managerInterface->addSuccess(__('Capture Online ' .$response['transaction']['transaction_amount']. ' by Paya success'));
                    return parent::capture($payment, $amount);
                }
                $this->_payaLogger->debug("not Capture when Submit invoice". $payment->getOrder()->getIncrementId() . 'response' .$response['errors']);
            }
        }

        //check contact
        $this->checkContact($payment);

        //payment with card save
        $cardId = $payment->getAdditionalInformation('card_id')? $payment->getAdditionalInformation('card_id') : null;
        if ($cardId) {
            $response = $this->_dataHelper->orderWithCardSave($cardId, $amount, $payment);
            if (!isset($response['errors'])) {
                $responseTransaction = $response['transaction'];
                $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($responseTransaction));
                if ($responseTransaction['cvv_response'] != Constant::RESPONSE_CVV_MATCH && $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV)) {
                    $this->_dataHelper->updateTransaction($payment, 'Invalid CVV');
                    throw new \Magento\Framework\Webapi\Exception(__('Invalid CVV, Please try again'));
                }
                $this->_transaction->saveTransactionOrder($responseTransaction, $payment, self::CODE);
                $this->performPayment($payment, $amount);
                $this->checkFraud($responseTransaction['auth_amount'], $amount, $payment);
            } else {
                foreach ($response['errors'] as $error) {
                    throw new \Magento\Framework\Webapi\Exception(__($error[0]));
                }
            }
            return parent::capture($payment, $amount);
        }
        $createByAdmin = $payment->getAdditionalInformation('create_by_admin') ? true : false;
        $paymentAction = $payment->getAdditionalInformation('payment_action');

        //payment in admin
        if ($paymentAction == ConfigHelper::CAPTURE && $createByAdmin) {
            $response = $this->_dataHelper->orderInAdmin($payment, $amount, $payment->getAdditionalInformation('card_encode'), ConfigHelper::PAYA_CAPTURE);

            if (!isset($response['errors'])) {
                $responseTransaction = $response['transaction'];
                $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($responseTransaction));
                if ($responseTransaction['cvv_response'] != Constant::RESPONSE_CVV_MATCH && $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV)) {
                    $this->_dataHelper->updateTransaction($payment, 'Invalid CVV');
                    throw new \Magento\Framework\Webapi\Exception(__('Invalid CVV, Please try again'));
                }
                //save transaction order
                $this->_transaction->saveTransactionOrder($responseTransaction, $payment, self::CODE);
                $this->performPayment($payment, $amount);
                $this->checkFraud($responseTransaction['auth_amount'], $amount, $payment);
            }else {
                foreach ($response['errors'] as $error) {
                    throw new \Magento\Framework\Webapi\Exception(__($error[0]));
                }
            }
        } else {
            //payment in frontend with new card
            $additionalData = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
            $this->performPayment($payment, $amount);
            $this->_transaction->saveTransactionOrder($additionalData, $payment, self::CODE);
            $this->_dataHelper->updateTransaction($payment);
        }

        return parent::capture($payment, $amount);
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->_moduleManager->isEnabled('Magenest_PayaConnect')) {
            return false;
        }
        return \Magento\Payment\Model\Method\AbstractMethod::isAvailable($quote);
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return Cc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $this->_payaLogger->debug("Authorize order: ".$payment->getOrder()->getIncrementId());
        //check contact
        $this->checkContact($payment);

        //payment with card save
        $cardId = $payment->getAdditionalInformation('card_id')? $payment->getAdditionalInformation('card_id') : null;
        if ($cardId) {
            $response = $this->_dataHelper->orderWithCardSave($cardId, $amount, $payment);
            if (!isset($response['errors'])) {
                $responseTransaction = $response['transaction'];
                $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($responseTransaction));
                if ($responseTransaction['cvv_response'] != Constant::RESPONSE_CVV_MATCH && $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV)) {
                    $this->_dataHelper->updateTransaction($payment, 'Invalid CVV');
                    throw new \Magento\Framework\Webapi\Exception(__('Invalid CVV, Please try again'));
                }
                $this->checkFraud($responseTransaction['auth_amount'], $amount, $payment);

                //save transaction order
                $this->_transaction->saveTransactionOrder($responseTransaction, $payment, self::CODE);
                $payment->setTransactionId($responseTransaction['id'])
                    ->setIsTransactionClosed(false)
                    ->setShouldCloseParentTransaction(false)
                    ->setCcTransId($responseTransaction['id']);
                return parent::authorize($payment, $amount);
            }else {
                foreach ($response['errors'] as $error) {
                    throw new \Magento\Framework\Webapi\Exception(__($error[0]));
                }
            }
        }

        //payment in admin
        if ($this->_appState->getAreaCode() == 'adminhtml') {
            //send request payment to paya
            $response = $this->_dataHelper->orderInAdmin($payment, $amount, $payment->getAdditionalInformation('card_encode'), ConfigHelper::PAYA_AUTHORIZE);$responseTransaction = $response['transaction'];
            if (!isset($response['errors'])) {
                $responseTransaction = $response['transaction'];
                $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($responseTransaction));
                if ($responseTransaction['cvv_response'] != Constant::RESPONSE_CVV_MATCH && $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV)) {
                    $this->_dataHelper->updateTransaction($payment, 'Invalid CVV');
                    throw new \Magento\Framework\Webapi\Exception(__('Invalid CVV, Please try again'));
                }
                //save transaction order
                $this->_transaction->saveTransactionOrder($responseTransaction, $payment, self::CODE);
            }
        } else {
            //payment in frontend with new card
            //save transaction order for payment new card
            if (!$cardId) {
                $responseTransaction = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
                $this->_transaction->saveTransactionOrder($responseTransaction, $payment, self::CODE);
                $payment->setTransactionId($responseTransaction['id'])
                    ->setIsTransactionClosed(false)
                    ->setShouldCloseParentTransaction(false)
                    ->setCcTransId($responseTransaction['id']);
            }
        }
        $this->_dataHelper->updateTransaction($payment);
        return parent::authorize($payment, $amount);
    }

    /**
     * @param $payment
     * @param $amount
     */
    private function performPayment($payment, $amount) {
        $response = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
        $this->_payaLogger->debug(var_export($response, true));
        $transactionId = $response['id'];
        $payment->setTransactionId($transactionId);
        $payment->setShouldCloseParentTransaction(true);
        $payment->setIsTransactionClosed(true);
        $payment->setAmount($amount);
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return $this|PayaConnect
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function cancel(\Magento\Payment\Model\InfoInterface $payment)
    {
        try {
            $transaction = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
            $transactionId = $transaction['id'];
            $params = [
                "transaction" => [
                    "action" => "void"
                ]
            ];
            $urlToVoidTransaction = $this->_configHelper->getCURL() . $this->_dataHelper->getEndPoint(Constant::ENDPOINT_TRANSACTION) . '/' . $transactionId;
            $response = $this->_dataHelper->sendRequest($urlToVoidTransaction, $this->_jsonHelper->jsonEncode($params), 'PUT');
            //Changed Status Transaction
            $this->_transaction->editStatusTransaction($transactionId, $response['transaction']['status_id']);
            return $this;
        } catch (\Exception $e) {
            throw new \Magento\Framework\Webapi\Exception(__('Cannot Cancel Order'));
        }
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return Cc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $messNotification = null;
        $additionalData = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
        $transactionId = $additionalData['id'];

        $order = $payment->getOrder();
        $params = [
            "transaction" => [
                "action" => "refund",
                "payment_method" => "cc",
                "previous_transaction_id" => $transactionId,
                "transaction_amount" => $amount,
                "location_id" => $this->_configHelper->getValueRequiredConfig(ConfigHelper::LOCATION_ID),
                "description" => "Refund: " . $amount . " For Order: ". $order->getIncrementId()
            ]
        ];
        $urlToRefund = $this->_configHelper->getCURL() . $this->_dataHelper->getEndPoint(Constant::ENDPOINT_TRANSACTION);
        //send request to refund payment Paya
        $response = $this->_dataHelper->sendRequest($urlToRefund, $this->_jsonHelper->jsonEncode($params), 'POST');
        if (!isset($response['errors'])) {
            $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($response['transaction']));

            //Changed Status Transaction
            $this->_transaction->editStatusTransaction($transactionId, $response['transaction']['status_id']);
            $this->_managerInterface->addSuccess(__('Refund ' .$response['transaction']['transaction_amount']. ' by Paya success'));
            return parent::refund($payment, $amount);
        }else {
            throw new \Magento\Framework\Webapi\Exception(__('Cannot refund order: ' . $this->_jsonHelper->jsonEncode($response['errors'])));
        }
    }

    /**
     * @param $amountResponse
     * @param $amountOrder
     * @param $payment
     */
    private function checkFraud($amountResponse, $amountOrder, $payment) {
        if(bccomp($amountResponse, $amountOrder, 2) != 0) {
            //submit fraud order if new order status is status
            $this->_payaLogger->debug("Paya Amount Not Same Order Amount: ".$payment->getOrder()->getIncrementId());
            $payment->setIsFraudDetected(true);
        }
    }

    /**
     * @param array $debugData
     */
    protected function _debug($debugData)
    {
        $this->_payaLogger->debug(var_export($debugData, true));
    }

    /**
     * @param $payment
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function checkContact($payment) {
        $order = $payment->getOrder();
        $customerId = $order->getCustomerId();
        if ($customerId) {
            $contactModel = $this->_contactFactory->create();
            $this->_contactResource->load($contactModel, $customerId, 'customer_id');
            if (!count($contactModel->getData())) {
                $contactData = [
                    "first_name" => $order->getCustomerFirstname(),
                    "last_name" => $order->getCustomerLastname(),
                    "address" => $order->getBillingAddress()->getData('street'),
                    "city" => $order->getBillingAddress()->getData('city'),
                    "email" => $order->getCustomerEmail(),
                    "customer_id" => $order->getCustomerId(),
                ];
                $this->_dataHelper->createContact($contactData);
            }
        }
    }
}
