<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Model\Api;


use Magenest\PayaConnect\Api\PayaFormInterface;
use Magenest\PayaConnect\Helper\ConfigHelper;
use Magenest\PayaConnect\Model\Constant;

/**
 * Class PayaForm
 * @package Magenest\PayaConnect\Model\Api
 */
class PayaForm implements PayaFormInterface
{
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $configHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magenest\PayaConnect\Model\ContactFactory
     */
    protected $contactFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Contact
     */
    protected $contactResource;

    /**
     * @var \Magenest\PayaConnect\Helper\DataHelper
     */
    protected $dataHelper;

    /**
     * PayaForm constructor.
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $configHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\PayaConnect\Model\ContactFactory $contactFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Contact $contactResource,
        \Magenest\PayaConnect\Helper\DataHelper $dataHelper
    )
    {
        $this->formKeyValidator = $formKeyValidator;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
        $this->jsonHelper = $jsonHelper;
        $this->customerSession = $customerSession;
        $this->contactFactory = $contactFactory;
        $this->contactResource = $contactResource;
        $this->dataHelper = $dataHelper;
    }


    /**
     *
     * @return string Greeting message with users name.
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @api
     */
    public function request() {
        $quote = $this->checkoutSession->getQuote();
        if ($quote->getIsActive()) {
            $user_hash_key = $this->configHelper->getUserHashKey();
            $user_id = $this->configHelper->getValueRequiredConfig(ConfigHelper::USER_ID);
            $timestamp = time();
            $salt = $user_id . $timestamp;
            $hash_key = hash_hmac('sha256', $salt, $user_hash_key);
            $developer_id = $this->configHelper->getDeveloperId();
            $domain = $this->configHelper->getCURL();
            $location_id = $this->configHelper->getValueRequiredConfig(ConfigHelper::LOCATION_ID);
            // Used for pay form
            $endpoint = Constant::ENDPOINT_PAYMENT_FORM;
            //Get random transaction id
            $transaction_api_id = $this->configHelper->getNonces();
            $transaction_amount = substr($quote->getBaseGrandTotal(), 0, -2);
            $action = $this->configHelper->getPaymentAction();
            $checkCvv = $this->configHelper->getValueConfig(ConfigHelper::CHECK_CVV);

            //Check and get contact id
            $contact_id = null;
            $contactModel = $this->contactFactory->create();
            $customer = $this->customerSession->getCustomer();
            $customerId = $customer->getId();
            if($customerId) {
                $this->contactResource->load($contactModel, $customerId, 'customer_id');
                if (!count($contactModel->getData())) {
                    $contactData = [
                        "first_name" => $customer->getData('firstname'),
                        "last_name" => $customer->getData('lastname'),
                        "address" => $quote->getBillingAddress()->getData('street'),
                        "city" => $quote->getBillingAddress()->getData('city'),
                        "email" => $customer->getEmail(),
                        "customer_id" => $customerId,
                    ];
                    $response = $this->dataHelper->createContact($contactData);
                    if (isset($response['contact'])) {
                        $response_contact = $response['contact'];
                        $contact_id = $response_contact['id'];
                    } else {
                        return $this->jsonHelper->jsonEncode([
                            'error' => true,
                            'message' => 'Cannot send request to Paya servers.'
                        ]);
                    }
                } else {
                    $contact_id = $contactModel->getData('contact_id');
                }
            }

            $params = [
                "transaction" => [
                    "payment_method" => "cc",
                    "action" => $action,
                    "transaction_amount" => $transaction_amount,
                    "location_id" => $location_id,
                    "transaction_api_id" => $transaction_api_id,
                    "show_cvv" => "1",
                    "contact_id" => $contact_id,
                    "parent_send_message" => true,
                    "display_close_button" => false,
                    "auto_decline_cvv_override" => $checkCvv,
                    "description" => $checkCvv ? 'CVV number has not been processed yet' : 'Payment By New Card'
                ]
            ];

            $data = implode(unpack("H*", $this->jsonHelper->jsonEncode($params)));

            //Build the URL to retreive the form
            $url = sprintf("%s/v2/%s?developer-id=%s&hash-key=%s&user-id=%s&timestamp=%s&data=%s",
                $domain,
                $endpoint,
                $developer_id,
                $hash_key,
                $user_id,
                $timestamp,
                $data
            );

            $result = [
                'success' => true,
                'url' => $url
            ];
            return $this->jsonHelper->jsonEncode($result);
        } else {
            return $this->jsonHelper->jsonEncode([
                'error' => true,
                'message' => 'Quote does not work. Please add the cart again'
            ]);
        }
    }
}
