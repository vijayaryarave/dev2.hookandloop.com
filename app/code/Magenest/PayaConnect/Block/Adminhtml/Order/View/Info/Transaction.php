<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Block\Adminhtml\Order\View\Info;


use Magento\Backend\Block\Template;
use Magenest\PayaConnect\Model\Constant;

/**
 * Class Transaction
 * @package Magenest\PayaConnect\Block\Adminhtml\Order\View\Info
 */
class Transaction extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Transaction
     */
    protected $_transactionResource;

    /**
     * @var \Magenest\PayaConnect\Model\TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * Transaction constructor.
     * @param \Magenest\PayaConnect\Model\TransactionFactory $transactionFactory
     * @param \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource
     * @param \Magento\Framework\Registry $registry
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magenest\PayaConnect\Model\TransactionFactory $transactionFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource,
        \Magento\Framework\Registry $registry,
        Template\Context $context,
        array $data = []
    )
    {
        $this->_transactionFactory = $transactionFactory;
        $this->_transactionResource = $transactionResource;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getDataView()
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_registry->registry('current_order');
        $transactionModel = $this->_transactionFactory->create();
        $this->_transactionResource->load($transactionModel, $order->getIncrementId(), 'order_id');
        $transactionStatus = $transactionModel->getData('transaction_status');
        switch ($transactionStatus) {
            case Constant::STATUS_ID_REFUND:
                $transactionStatus = 'REFUND';
                break;
            case Constant::STATUS_ID_AUTHORIZE:
                $transactionStatus = 'AUTHORIZE';
                break;
            case Constant::STATUS_ID_CAPTURE:
                $transactionStatus = 'CAPTURE';
                break;
        }
        return [
                    'Transaction Id' => $transactionModel->getData('transaction_id'),
                    'Payment Status' => $transactionStatus,
                    'Transaction Amount' => $transactionModel->getData('transaction_amount')
//                    'Payment Method' => $transactionModel->getData('payment_method')
              ];
    }
}
