<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Block\Adminhtml\System\Config\Fieldset;

use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Backend\Block\Template;
use Magento\Framework\Module\Dir\Reader as DirReader;

/**
 * Class Version
 * @package Magenest\PayaConnect\Block\Adminhtml\System\Config\Fieldset
 */
class Version extends Template implements RendererInterface
{
    /**
     * @var DirReader
     */
    protected $dirReader;

    protected $moduleList;

    protected $fileDriver;

    protected $jsonHelper;

    /**
     * Version constructor.
     * @param DirReader $dirReader
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        DirReader $dirReader,
        Template\Context $context,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        array $data = []
    )
    {
        $this->dirReader = $dirReader;
        $this->moduleList = $moduleList;
        $this->fileDriver = $fileDriver;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = '';
        if ($element->getData('group')['id'] == 'version') {
            $html = $this->toHtml();
        }
        return $html;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return 'Magenest_PayaConnect::system/config/fieldset/version.phtml';
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->moduleList
            ->getOne('Magenest_PayaConnect')['setup_version'];
    }

    /**
     * @param $moduleName
     * @return bool|mixed
     */
    public function getComposerInformation($moduleName)
    {
        $dir = $this->dirReader->getModuleDir("", $moduleName);

        if ($this->fileDriver->isExists($dir . '/composer.json')) {
            return $this->jsonHelper->jsonDecode(file_get_contents($dir . '/composer.json'), true);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getDownloadDebugUrl()
    {
        return $this->getUrl('payaconnect/config/downloadDebug', ['version' => $this->getVersion()]);
    }
}
