<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Block;

use Magento\Catalog\Block\Product\Context;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\View\Element\Template;
use Magenest\PayaConnect\Helper\ConfigHelper;

/**
 * Class Card
 * @package Magenest\PayaConnect\Block
 */
class Card extends Template
{
    /**
     * @var CurrentCustomer
     */
    protected $_currentCustomer;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $_config;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory
     */
    protected $_vaultCollectionFactory;

    /**
     * Card constructor.
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $config
     * @param \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory $vaultCollectionFactoryFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        \Magenest\PayaConnect\Helper\ConfigHelper $config,
        \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory $vaultCollectionFactoryFactory,
        array $data
    ) {
        $this->_currentCustomer = $currentCustomer;
        $this->_config = $config;
        $this->_vaultCollectionFactory = $vaultCollectionFactoryFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param $id
     * @return string
     */
    public function getDeleteUrl($id)
    {
        return $this->getUrl('payaconnect/card/delete', ['id' => $id]);
    }

    /**
     * @return \Magenest\PayaConnect\Model\ResourceModel\Vault\Collection
     */
    public function getDataCard()
    {
        $customerId = $this->_currentCustomer->getCustomerId();
        $vaultCollection = $this->_vaultCollectionFactory->create()->addFieldToFilter("customer_id", $customerId);
        return $vaultCollection;
    }

    public function checkCanSaveCard() {
        return $this->_config->getValueConfig(ConfigHelper::CAN_SAVE_CARD);
    }
}
