<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Helper;


use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magenest\PayaConnect\Model\Constant;
use Magenest\PayaConnect\Helper\ConfigHelper;

/**
 * Class DataHelper
 * @package Magenest\PayaConnect\Helper
 */
class DataHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     */
    const REQUEST_UNSUCCESS = 'Request unsuccessful';

    /**
     *
     */
    const STATUS_CODE_ERROR = 401;
    /**
     * @var \Magenest\PayaConnect\Model\TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Transaction
     */
    protected $_transactionResource;

    /**
     * @var Logger
     */
    protected $_payaLogger;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * @var ConfigHelper
     */
    protected $_configHelper;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;

    /**
     * @var \Magenest\PayaConnect\Model\VaultFactory
     */
    protected $_vaultFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Vault
     */
    protected $_vaultResource;

    /**
     * @var \Magenest\PayaConnect\Model\ContactFactory
     */
    protected $_contactFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Contact
     */
    protected $_contactResource;

    /**
     * DataHelper constructor.
     * @param \Magenest\PayaConnect\Model\TransactionFactory $transactionFactory
     * @param \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource
     * @param Logger $payaLogger
     * @param \Magento\Customer\Model\Session $customerSession
     * @param Context $context
     */
    public function __construct(
        \Magenest\PayaConnect\Model\TransactionFactory $transactionFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource,
        \Magenest\PayaConnect\Helper\Logger $payaLogger,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magenest\PayaConnect\Model\VaultFactory $vaultFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Vault $vaultResource,
        \Magenest\PayaConnect\Model\ContactFactory $contactFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Contact $contactRource,
        Context $context
    )
    {
        $this->_transactionFactory = $transactionFactory;
        $this->_transactionResource = $transactionResource;
        $this->_payaLogger = $payaLogger;
        $this->_customerSession = $customerSession;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_configHelper = $configHelper;
        $this->_jsonHelper = $jsonHelper;
        $this->messageManager = $messageManager;
        $this->_authSession = $authSession;
        $this->_vaultFactory = $vaultFactory;
        $this->_vaultResource = $vaultResource;
        $this->_contactFactory = $contactFactory;
        $this->_contactResource = $contactRource;
        parent::__construct($context);
    }

    /**
     * @param $payment
     * @param $amount
     * @param $cardEncode
     * @param $action
     * @return mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function orderInAdmin($payment, $amount, $cardEncode, $action){
        $cardInformation = $this->_jsonHelper->jsonDecode(base64_decode($cardEncode));
        $exp_month = $cardInformation['cc_exp_month'];
        //Add 0 to the months
        if (strlen($exp_month) == '1') {
            $exp_month = '0' . $exp_month;
        }
        $exp_year = substr($cardInformation['cc_exp_year'], -2);
        $exp_date = $exp_month . $exp_year;
        $curl = $this->_configHelper->getCURL() . $this->getEndPoint(Constant::ENDPOINT_TRANSACTION);

        //get contact id
        $order = $payment->getOrder();
        //get contact id
        $customerId = $order->getCustomerId();
        $contactModel = $this->_contactFactory->create();
        $this->_contactResource->load($contactModel, $customerId, 'customer_id');

        //add description
        $params = [
            "transaction" => [
                "payment_method" => "cc",
                "action" => $action,
                "account_number" => $cardInformation['cc_number'],
                "exp_date" => $exp_date,
                "transaction_amount" => $amount,
                "contact_id" => $contactModel->getData('contact_id'),
                "cvv" => $cardInformation['cc_cid'],
                "auto_decline_cvv_override" => $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV),
                "description" => "Create In Admin - " . $order->getIncrementId()
            ]
        ];
        $response = $this->sendRequest($curl, $this->_jsonHelper->jsonEncode($params));
        if (!isset($response['errors'])) {
            $payment->setAdditionalInformation('transaction_response', $this->_jsonHelper->jsonEncode($response['transaction']));
        }
        return $response;
    }

    /**
     * @param $cardId
     * @param $amount
     * @param $payment
     * @return mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function orderWithCardSave($cardId, $amount, $payment)
    {
        $vaultModel = $this->_vaultFactory->create();
        $order = $payment->getOrder();
        //get contact id
        $customerId = $order->getCustomerId();
        $contactModel = $this->_contactFactory->create();
        $this->_contactResource->load($contactModel, $customerId, 'customer_id');

        $this->_vaultResource->load($vaultModel, $cardId);
        $params = [
            "transaction" => [
                "payment_method" => "cc",
                "action" => $this->_configHelper->getPaymentAction(),
                "transaction_amount" => $amount,
                "auto_decline_cvv_override" => $this->_configHelper->getValueConfig(ConfigHelper::CHECK_CVV),
                "cvv" => $payment->getAdditionalInformation('cvv'),
                "account_vault_id" => $vaultModel->getData('card_id'),
                "contact_id" => $contactModel->getData('contact_id'),
                "description" => $order->getIncrementId()
            ]
        ];
        $curl = $this->_configHelper->getCURL() . $this->getEndPoint(Constant::ENDPOINT_TRANSACTION);
        $response = $this->sendRequest($curl, $this->_jsonHelper->jsonEncode($params));
        return $response;
    }

    /**
     * @param $url
     * @param $content
     * @param null $requestMethod
     * @return mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendRequest($url, $content = null, $requestMethod = null)
    {
        if (!$requestMethod) {
            $requestMethod = "POST";
        }

        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders([
            "Content-Type" => "application/json",
            "developer-id" => $this->_configHelper->getDeveloperId(),
            "user-id" => $this->_configHelper->getValueRequiredConfig(ConfigHelper::USER_ID),
            "user-api-key" => $this->_configHelper->getValueRequiredConfig(ConfigHelper::API_KEY)
        ]);

        $request = new \Zend\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setContent($content);
        $request->setUri($url);
        $request->setMethod(strtoupper($requestMethod));


        $client = new \Zend\Http\Client();
        $options = [
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);
        try {
            $response = $client->send($request);
            $responseBody = $response->getBody();

            //When delete the card, the paya has no response
            if ($requestMethod = 'DELETE' && !$responseBody) {
                return true;
            }

            $responseBody = $this->_jsonHelper->jsonDecode($responseBody);

            if (isset($responseBody['status']) && $responseBody['status'] == self::STATUS_CODE_ERROR ) {
                return [
                    'errors' => self::REQUEST_UNSUCCESS
                ];
            }
            return $responseBody;
        } catch (\Exception $e) {
            return [
                'errors' => self::REQUEST_UNSUCCESS
            ];
        }
    }

    /**
     * @param $transactionId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveCard($transactionId){
        //get curl request paya
        $curl = $this->_configHelper->getCURL() . $this->getEndPoint(Constant::ENDPOINT_ACCOUNT_VAULTS);

        //get contact id
        $customer = $this->_customerSession;
        $customerId = $customer->getId();
        $contactModel = $this->_contactFactory->create();
        $this->_contactResource->load($contactModel, $customerId, 'customer_id');
        $contact_id = $contactModel->getData('contact_id') ? $contactModel->getData('contact_id') : null;

        $params = [
            "accountvault" => [
                "payment_method" => "cc",
                "previous_transaction_id" => $transactionId,
                "contact_id" => $contact_id,
                "location_id" =>  $this->_configHelper->getValueRequiredConfig(ConfigHelper::LOCATION_ID),
                "title" => "Checkout"
            ]
        ];
        $response = $this->sendRequest($curl, $this->_jsonHelper->jsonEncode($params));
        if (!isset($response['errors'])) {
            //save in table
            $response_accountvault = $response['accountvault'];
            $vaultModel = $this->_vaultFactory->create();
            $vaultData = [
                'customer_id' => $customerId,
                'card_id' => $response_accountvault['id'],
                'card_type' => $response_accountvault['account_type'],
                'last_four_digits' => $response_accountvault['last_four'],
                'masked_number' => 'xxxxxxxxxxxx' . $response_accountvault['last_four'],
                'expiration_date' => $response_accountvault['exp_date'],
                'created_at' => time(),
            ];
            $vaultModel->setData($vaultData);
            try {
                $this->_vaultResource->save($vaultModel);
            } catch (\Exception $e) {
                $this->_payaLogger->debug("Save Card ERROR");
                $this->_payaLogger->debug(var_export($e->getMessage(), true));
            }
        }
    }

    /**
     * @param $payment
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateTransaction($payment, $description = null) {
        $response = $this->_jsonHelper->jsonDecode($payment->getAdditionalInformation('transaction_response'));
        $curl = $this->_configHelper->getCURL() . $this->getEndPoint(Constant::ENDPOINT_TRANSACTION) . '/'. $response['id'];
        $order = $payment->getOrder();
        $params = [
            "transaction" => [
                "action" => "edit",
                "description" => $description ? $description : $order->getIncrementId()
            ]
        ];
        $this->sendRequest($curl, $this->_jsonHelper->jsonEncode($params), 'PUT');
    }

    /**
     * @param $contactData
     * @return mixed|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createContact($contactData) {
        $curl = $this->_configHelper->getCURL() . $this->getEndPoint(Constant::ENDPOINT_CONTACT);
        $params = [
            "contact" => [
                "first_name" => $this->removeAccents($contactData['first_name']),
                "last_name" => $this->removeAccents($contactData['last_name']),
                "address" => $this->removeAccents($contactData['address']),
                "city" => $this->removeAccents($contactData['city']),
                "email" => $contactData['email'],
                "location_id" =>  $this->_configHelper->getValueRequiredConfig(ConfigHelper::LOCATION_ID)
            ]
        ];
        $response = $this->sendRequest($curl, $this->_jsonHelper->jsonEncode($params));
        if (isset($response['contact'])) {
            try {
                //save contact in table
                $response_contact = $response['contact'];
                $contactModel = $this->_contactFactory->create();
                $contactData = [
                    'contact_id' => $response_contact['id'],
                    'customer_id' => $contactData['customer_id'],
                    'first_name' => $response_contact['first_name'],
                    'last_name' => $response_contact['last_name'],
                ];
                $contactModel->setData($contactData);
                $this->_contactResource->save($contactModel);
                return $response;
            } catch (\Exception $e) {
                $this->_payaLogger->debug("Save Contact ERROR");
                $this->_payaLogger->debug(var_export($e->getMessage(), true));
            }
        }

        return $response;

    }

    /**
     * @param $payment
     */
    public function voidTransaction($payment) {

    }

    /**
     * @param $string
     * @return string|string[]
     */
    public function removeAccents($string) {
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach($unicode as $nonUnicode=>$uni){
            $string = preg_replace("/($uni)/i", $nonUnicode, $string);
        }
        $string = str_replace(' ','_',$string);
        return $string;
    }

    /**
     * @param $value
     * @return string
     */
    public function getEndPoint($value) {
        return '/v2/' . $value;
    }
}
