<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magenest\PayaConnect\Model\Constant;

/**
 * Class ConfigHelper
 * @package Magenest\PayaConnect\Helper
 */
class ConfigHelper extends AbstractHelper
{
    /**
     *
     */
    const CAPTURE = 'authorize_capture';

    /**
     *
     */
    const AUTHORIZE = 'authorize';

    /**
     *
     */
    const PAYA_CAPTURE = 'sale';

    const PAYA_AUTHORIZE = 'authonly';
    const PAYMENT_ACTION = 'payment/magenest_payaconnect/payment_action';
    const SANDBOX_MODE = 'payment/magenest_required_config/sandbox';
    const USER_HASH_KEY_TEST = 'payment/magenest_required_config/user_hash_key_test';
    const USER_HASH_KEY_LIVE = 'payment/magenest_required_config/user_hash_key';
    const DEVELOPER_ID_TEST = 'payment/magenest_required_config/developer_id';
    const CAN_SAVE_CARD = 'payment/magenest_payaconnect/can_save_card';
    const DELETE_ACCOUNT_VAULT = 'payment/magenest_payaconnect/can_delete_account_vault';
    const CHECK_CVV = 'payment/magenest_payaconnect/check_cvv';
    const API_KEY = 'payment/magenest_required_config/api_key';
    const USER_ID = 'payment/magenest_required_config/user_id';
    const LOCATION_ID = 'payment/magenest_required_config/location_id';

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * ConfigHelper constructor.
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param Context $context
     */
    public function __construct(
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        Context $context
    )
    {
        $this->_encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function checkSandbox() {
        return $this->scopeConfig->getValue(
            self::SANDBOX_MODE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getValueConfig($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getUserHashKey()
    {
        if ($this->checkSandbox()) {
            return $this->_encryptor->decrypt($this->scopeConfig->getValue(
                self::USER_HASH_KEY_TEST,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ));
        }
        return $this->_encryptor->decrypt($this->scopeConfig->getValue(
            self::USER_HASH_KEY_LIVE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }

    /**
     * @return string
     */
    public function getCURL()
    {
        if ($this->checkSandbox()) {
            return Constant::CURL_SANDBOX;
        }
        return Constant::CURL_LIVE;
    }

    /**
     * @return string
     */
    public function getPaymentAction()
    {
        $action = $this->getConfigPaymentAction();
        //return param to request payment action PAYA
        if ($action == self::AUTHORIZE) {
            return self::PAYA_AUTHORIZE;
        }
        return self::PAYA_CAPTURE;
    }

    /**
     * @return mixed
     */
    public function getConfigPaymentAction() {
        return $this->scopeConfig->getValue(
            self::PAYMENT_ACTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNonces()
    {
        $id = openssl_random_pseudo_bytes(16);
        return base64_encode(bin2hex($id));
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getValueRequiredConfig($path)
    {
        if ($this->checkSandbox()) {
            $path .= '_test';
        }
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed|string
     */
    public function getDeveloperId() {
        if ($this->checkSandbox()) {
            return $this->scopeConfig->getValue(
                self::DEVELOPER_ID_TEST,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }
        return Constant::DEVELOPER_ID;
    }
}
