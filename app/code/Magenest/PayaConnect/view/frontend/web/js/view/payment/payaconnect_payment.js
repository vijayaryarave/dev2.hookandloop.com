define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';

        rendererList.push(
            {
                type: 'magenest_payaconnect',
                component: 'Magenest_PayaConnect/js/view/payment/method-renderer/payaconnect-payments-method'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    }
);
