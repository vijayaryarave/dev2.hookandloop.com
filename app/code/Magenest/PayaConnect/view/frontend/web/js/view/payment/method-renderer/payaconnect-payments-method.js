define([
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'ko',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/url',
        'mage/storage',
        'Magento_Checkout/js/action/redirect-on-success',
    ],
    function (
        $,
        Component,
        ko,
        additionalValidators,
        fullScreenLoader,
        url,
        storage,
        redirectOnSuccessAction,
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_PayaConnect/payment/payaconnect-payments-method',
                cardId: ko.observable(0),
                payaCurl : window.checkoutConfig.payment.magenest_payaconnect.paya_curl,
                saveCardConfig: window.checkoutConfig.payment.magenest_payaconnect.is_save_card,
                hasCard: window.checkoutConfig.payment.magenest_payaconnect.hasCard,
                isLogged: window.checkoutConfig.payment.magenest_payaconnect.isLogin,
                checkCvv: window.checkoutConfig.payment.magenest_payaconnect.checkCvv,
                showPaymentField: ko.observable(false),
                isSelectCard: ko.observable(false),
                isNewCard: ko.observable(false),
                customerCard: ko.observableArray(JSON.parse(window.checkoutConfig.payment.magenest_payaconnect.saveCards)),
                saveCardOption: "",
                cvvResponseMatch: 'M',
                loadFormSuccess: 'formSuccess',
                checkRunRealPlaceOrder: ko.observable(false)
            },

            initObservable: function(){
                var self = this;
                this._super();

                this.isSelectCard = ko.computed(function () {
                    if (self.cardId() && self.hasCard){
                        self.isNewCard(false);
                        return true;
                    }else{
                        self.isNewCard(true);
                        return false;
                    }
                }, this);
                this.showPaymentField = ko.computed(function () {
                    if((!this.saveCardConfig) || !this.isSelectCard()){
                        return true;
                    }
                }, this);
                return this;
            },

            showPlaceOrder: function(){
                //show Place Order button when select card save
                var self = this;
                var divForm = '#' + self.getCode() + '-card-element';
                var idListCard = '#' + self.getCode() + '-card-id';
                if ($(divForm).hasClass(self.loadFormSuccess) && !$(idListCard).val()) {
                    $('.actions-toolbar').hide();
                } else {
                    $('.actions-toolbar').show();
                }
            },

            placeOrder: function (data, event) {
                var self = this;

                if (this.cardId()) {
                    self.realPlaceOrder();
                } else {
                    if (event) {
                        event.preventDefault();
                    }

                    var divForm = '#' + self.getCode() + '-card-element';
                    $(divForm).show();

                    if (this.validate() && additionalValidators.validate()) {
                        $.when(self.renderPayaForm()).then(function () {
                            $('#paymentButton').trigger("click");
                        });
                    }
                }
                return false;
            },

            renderPayaForm: function () {
                var df = $.Deferred();
                var self = this;
                fullScreenLoader.startLoader();

                storage.post(
                    url.build('rest/V1/magenest/paya/form'),
                ).done(function (response) {
                    var data = JSON.parse(response);
                    if (data.success) {
                        var src = "src=" + data.url;
                        var divForm = '#' + self.getCode() + '-card-element';
                        $(divForm).html('<iframe style="margin-top: 30px" height="430px" width="305px"' + src  + '></iframe>');
                        $(divForm).addClass(self.loadFormSuccess);

                        //Disable button Place Order
                        $('.actions-toolbar').hide();

                        window.addEventListener("message", receiveMessage, false);

                        function receiveMessage(event) {
                            // Verify sender's identity
                            if (event.origin !== self.payaCurl){
                                df.reject(true);
                                self.messageContainer.addErrorMessage({
                                    message: $.mage.__('CURL API does not work')
                                });
                                console.log(data.message);
                                fullScreenLoader.stopLoader(true);
                            }

                            if (event.isTrusted && event.data) {
                                if (JSON.parse(event.data).cvv_response === self.cvvResponseMatch && self.checkCvv || !self.checkCvv) {
                                    self.source = event.data;
                                    $(divForm).hide(2000);
                                    if(!self.checkRunRealPlaceOrder()) {
                                        self.realPlaceOrder();
                                    }
                                } else {
                                    $(divForm).hide(888);
                                    self.messageContainer.addErrorMessage({
                                        message: $.mage.__('Invalid CVV, Please try again')
                                    });
                                    //show place order button
                                    $('.actions-toolbar').show();
                                    $(divForm).removeClass(self.loadFormSuccess);
                                    //remove iframe
                                    $(divForm).empty();
                                }
                            } else {
                                $(divForm).hide(888);
                                self.messageContainer.addErrorMessage({
                                    message: $.mage.__('Paya does not accept payments. Please try again')
                                });
                                fullScreenLoader.stopLoader(true);
                            }
                        }
                        fullScreenLoader.stopLoader();
                    }

                    if (data.error) {
                        df.reject(true);
                        self.messageContainer.addErrorMessage({
                            message: data.message
                        });
                        fullScreenLoader.stopLoader(true);
                    }
                });
                return df.promise();
            },

            realPlaceOrder: function () {
                var self = this;
                self.checkRunRealPlaceOrder(true);
                this.getPlaceOrderDeferredObject()
                    .fail(
                        function () {
                            self.isPlaceOrderActionAllowed(true);
                            self.checkRunRealPlaceOrder(false);
                            fullScreenLoader.stopLoader(true);
                        }
                    ).done(
                    function () {
                        // self.afterPlaceOrder();
                        if (self.redirectAfterPlaceOrder) {
                            redirectOnSuccessAction.execute();
                        }
                    }
                );
            },

            getData: function () {
                var self = this;
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'transaction_infor': self.source,
                        'saved': this.saveCardOption,
                        'cardId': this.cardId(),
                        'cvv': self.creditCardVerificationNumber()
                    }
                }
            },

            getCode: function() {
                return 'magenest_payaconnect';
            },

            isActive: function() {
                return true;
            }
        });
    }
);
