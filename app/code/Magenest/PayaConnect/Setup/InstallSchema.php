<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * paya2210 extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_paya2210
 */

namespace Magenest\PayaConnect\Setup;


use Magento\Framework\DB\Ddl\Table as Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    protected $productMetadata;

    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->productMetadata = $productMetadata;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $magentoVersion = floatval($this->productMetadata->getVersion());
        //check magento version
        if ($magentoVersion < 2.3) {
            $this->installWhenVersionLower23($setup, $context);
        }
    }

    protected function installWhenVersionLower23($setup, $context) {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_paya_vault'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer Id'
            )
            ->addColumn(
                'card_id',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Card Id'
            )
            ->addColumn(
                'card_type',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Card Type'
            )
            ->addColumn(
                'last_four_digits',
                Table::TYPE_TEXT,
                null,
                [],
                'Last Four Digits'
            )
            ->addColumn(
                'masked_number',
                Table::TYPE_TEXT,
                null,
                [],
                'Masked Number'
            )
            ->addColumn(
                'expiration_date',
                Table::TYPE_TEXT,
                null,
                [],
                'Masked Number'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                'null',
                ['default' => Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->setComment('Magenest Paya Vault');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_paya_transaction'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'transaction_id',
                Table::TYPE_TEXT,
                null,
                [],
                'Transaction Id'
            )
            ->addColumn(
                'transaction_status',
                Table::TYPE_TEXT,
                null,
                [],
                'Transaction Status'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Order Id'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer Id'
            )
            ->addColumn(
                'customer_email',
                Table::TYPE_TEXT,
                null,
                [],
                'Customer Email'
            )
            ->addColumn(
                'payment_method',
                Table::TYPE_TEXT,
                null,
                [],
                'Payment Method'
            )
            ->addColumn(
                'card_type',
                Table::TYPE_TEXT,
                null,
                [],
                'Card Type'
            )
            ->addColumn(
                'transaction_amount',
                Table::TYPE_TEXT,
                null,
                [],
                'Transaction Amount'
            )
            ->setComment('Charge table');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_paya_contacts'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'contact_id',
                Table::TYPE_TEXT,
                null,
                [],
                'Contact Id'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer Id'
            )
            ->addColumn(
                'first_name',
                Table::TYPE_TEXT,
                null,
                [],
                'First Name'
            )
            ->addColumn(
                'last_name',
                Table::TYPE_TEXT,
                null,
                [],
                'Last Name'
            );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
