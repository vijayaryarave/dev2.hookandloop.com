<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_payaconnect
 */

namespace Magenest\PayaConnect\Ui\Component\Listing\Columns;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class ViewOrder
 * @package Magenest\PayaConnect\Ui\Component\Listing\Columns
 */
class ViewOrder extends Column
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Sales\Api\Data\OrderInterface
     */
    protected $order;

    /**
     * ViewOrder constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Api\Data\OrderInterface $order,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->order = $order;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $href = $this->getUrlViewOrder($item['order_id']);
                $item[$this->getData('name')] = '<a class="rating" href="' . $href . '">' .$item['order_id'] . '</a>';
            }
        }
        return $dataSource;
    }

    /**
     * @param $orderId
     * @return string
     */
    public function getUrlViewOrder($orderId)
    {
        $order = $this->order->loadByIncrementId($orderId);
        return $this->urlBuilder->getUrl('sales/order/view', ['order_id' => $order->getId()]);
    }
}
