<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_payaconnect
 */

namespace Magenest\PayaConnect\Ui\Component\Listing\Columns;


use Magento\Ui\Component\Listing\Columns\Column;
use Magenest\PayaConnect\Model\Constant;

/**
 * Class StatusId
 * @package Magenest\PayaConnect\Ui\Component\Listing\Columns
 */
class StatusId extends Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = $this->showTransactionStatus($item['transaction_status']);
            }
        }
        return $dataSource;
    }

    /**
     * @param $statusId
     * @return string
     */
    public function showTransactionStatus($statusId)
    {
        $status = null;
        switch ($statusId) {
            case $statusId == Constant::STATUS_ID_CAPTURE:
                $status = "CAPTURE";
                break;
            case $statusId == Constant::STATUS_ID_AUTHORIZE;
                $status = "AUTHORIZE ONLY";
                break;
            case $statusId == Constant::STATUS_ID_REFUND;
                $status = "REFUND";
                break;
            case $statusId == Constant::STATUS_ID_VOID;
                $status = "CANCEL";
                break;
        }
        return $status;
    }
}
