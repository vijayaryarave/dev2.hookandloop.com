<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Ui\Component\Listing\Columns;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class TransactionActions
 * @package Magenest\PayaConnect\Ui\Component\Listing\Columns
 */
class TransactionActions extends Column
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    protected $transactionFactory;

    protected $transactionResource;

    /**
     * TransactionActions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magenest\PayaConnect\Model\TransactionFactory $transactionFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Transaction $transactionResource,
        array $components = [], array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->orderFactory = $orderFactory;
        $this->transactionFactory = $transactionFactory;
        $this->transactionResource = $transactionResource;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $transactionModel = $this->transactionFactory->create();
                $this->transactionResource->load($transactionModel, $item['id']);
                $name = $this->getData('name');
                if (isset($item['id'])) {

                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl('payaconnect/transaction/delete', ['id' => $item['id']]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete transaction'),
                            'message' => __('Are you sure you wan\'t to delete this transaction "${ $.$data.transaction_id }"?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
