<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Controller\Adminhtml\Config;


use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class DownloadDebug
 * @package Magenest\PayaConnect\Controller\Adminhtml\Config
 */
class DownloadDebug extends Action
{
    /**
     * @var DirectoryList
     */
    protected $directory_list;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    protected $fileDriver;

    /**
     * DownloadDebug constructor.
     * @param Action\Context $context
     * @param DirectoryList $directory_list
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        Action\Context $context,
        DirectoryList $directory_list,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem\Driver\File $fileDriver
    )
    {
        $this->directory_list = $directory_list;
        $this->fileFactory = $fileFactory;
        $this->fileDriver = $fileDriver;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute()
    {
        $version = $this->getRequest()->getParam('version');
        $filename = "paya_connect_debugfile_" . $version . "_" . date("Ymd") . ".log";
        $file = $this->directory_list->getPath("var") . "/log/paya/debug.log";
        if ($this->fileDriver->isExists($file)) {
            return $this->fileFactory->create($filename, file_get_contents($file), "tmp");
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->messageManager->addError(__('Debug File Does Not Exist'));
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_PayaConnec::settings');
    }
}
