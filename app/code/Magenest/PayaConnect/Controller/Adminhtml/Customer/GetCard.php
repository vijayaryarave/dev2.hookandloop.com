<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_payaconnect
 */

namespace Magenest\PayaConnect\Controller\Adminhtml\Customer;


use Magento\Backend\App\Action;

/**
 * Class GetCard
 * @package Magenest\PayaConnect\Controller\Adminhtml\Customer
 */
class GetCard extends Action
{
    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory
     */
    protected $vaultCollectionFactory;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $configHelper;

    /**
     * GetCard constructor.
     * @param \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory $vaultCollectionFactory
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $configHelper
     * @param Action\Context $context
     */
    public function __construct(
        \Magenest\PayaConnect\Model\ResourceModel\Vault\CollectionFactory $vaultCollectionFactory,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        Action\Context $context
    ) {
        $this->vaultCollectionFactory = $vaultCollectionFactory;
        $this->configHelper = $configHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('customer_id');
        $listCard = $this->vaultCollectionFactory->create()->addFieldToFilter('customer_id', $customerId);
        $canUseCardMoto = $this->configHelper->getValueConfig('admin_use_card_save') ? true : false;
        if (count($listCard->getData())) {
            $html = "";
            $html .= "<option value=''>".__("Select card")."</option>";
            foreach ($listCard as $card) {
                $label = $card->getData('masked_number') . ' (' . $card->getData('card_type'). ')';
                $html .= "
            <option value=".$card->getId().">".$label."</option>
            ";
            }
            return $this->resultFactory->create("json")->setData([
                'success' => true,
                'canUseCardMoto' => $canUseCardMoto,
                'html' => $html
            ]);
        }
        return $this->resultFactory->create("json")->setData([
            'success' => false,
            'canUseCardMoto' => $canUseCardMoto,
        ]);
    }
}
