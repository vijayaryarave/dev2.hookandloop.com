<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Controller\Adminhtml\Transaction;


use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Magenest\PayaConnect\Controller\Adminhtml\Transaction
 */
class Index extends Action
{
    protected $resultPageFactory;
    /**
     * Index constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_PayaConnect::transaction');
        $resultPage->addBreadcrumb(__('Paya Transaction'), __('Paya Transaction'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Paya Transaction'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_PayaConnect::transaction');
    }
}
