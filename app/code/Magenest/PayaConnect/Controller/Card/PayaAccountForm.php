<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Controller\Card;


use Magenest\PayaConnect\Helper\ConfigHelper;
use Magenest\PayaConnect\Model\Constant;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\PayaConnect\Model\Contact;

/**
 * Class PayaAccountForm
 * @package Magenest\PayaConnect\Controller\Card
 */
class PayaAccountForm extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $configHelper;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magenest\PayaConnect\Model\ContactFactory
     */
    protected $contactFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Contact
     */
    protected $contactResource;

    /**
     * @var \Magenest\PayaConnect\Helper\DataHelper
     */
    protected $dataHelper;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $customerResource;

    /**
     * PayaAccountForm constructor.
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $configHelper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\PayaConnect\Model\ContactFactory $contactFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Contact $contactResource,
        \Magenest\PayaConnect\Helper\DataHelper $dataHelper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\Customer $customerResource,
        Context $context
    ) {
        $this->jsonFactory = $resultJsonFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->configHelper = $configHelper;
        $this->jsonHelper = $jsonHelper;
        $this->customerSession = $customerSession;
        $this->contactFactory = $contactFactory;
        $this->contactResource = $contactResource;
        $this->dataHelper = $dataHelper;
        $this->customerFactory = $customerFactory;
        $this->customerResource = $customerResource;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->jsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            if (!$this->formKeyValidator->validate($this->getRequest())) {
                return $result->setData([
                    'error' => true,
                    'message' => 'Payment Error'
                ]);
            }
            $user_hash_key = $this->configHelper->getUserHashKey();
            $user_id = $this->configHelper->getValueRequiredConfig(ConfigHelper::USER_ID);
            $timestamp = time();
            $salt = $user_id . $timestamp;
            $hash_key = hash_hmac('sha256', $salt, $user_hash_key);
            $developer_id = $this->configHelper->getDeveloperId();
            $domain = $this->configHelper->getCURL();
            $location_id = $this->configHelper->getValueRequiredConfig(ConfigHelper::LOCATION_ID);
            // Used for account form
            $endpoint = Constant::ENDPOINT_ACCOUNT_FORM;
            //Get random transaction id
            $account_vault_api_id = $this->configHelper->getNonces();

            //Get contact id
            $customerSession = $this->customerSession->getCustomer();
            $customerId = $customerSession->getId();
            $contactModel = $this->contactFactory->create();
            $this->contactResource->load($contactModel, $customerId, 'customer_id');
            $contact_id = null;
            if (!count($contactModel->getData())) {
                $contactData = [
                    "first_name" => $customerSession->getData('firstname'),
                    "last_name" => $customerSession->getData('lastname'),
                    "address" => '',
                    "city" => '',
                    "email" => $customerSession->getEmail(),
                    "customer_id" => $customerId,
                ];
                $response = $this->dataHelper->createContact($contactData);
                if (!isset($response['errors'])) {
                    $response_contact = $response['contact'];
                    $contact_id = $response_contact['id'];
                }
            } else {
                $contact_id = $contactModel->getData('contact_id');
            }

            $params = [
                "accountvault" => [
                    "payment_method" => "cc",
                    "location_id" => $location_id,
                    "account_vault_api_id" => $account_vault_api_id,
                    "contact_id" => $contact_id,
                    "show_title" => false,
                    "show_cvv" => true,
                    //save card in My Account
                    "title" => "My Account",
                    "show_street" => true,
                    "display_close_button" => false,
                ]
            ];

            $data = implode(unpack("H*", $this->jsonHelper->jsonEncode($params)));

            //Build the URL to retreive the form
            $url = sprintf("%s/v2/%s?developer-id=%s&hash-key=%s&user-id=%s&timestamp=%s&data=%s",
                $domain,
                $endpoint,
                $developer_id,
                $hash_key,
                $user_id,
                $timestamp,
                $data
            );

            $resultData = [
                'success' => true,
                'url' => $url,
                'contact_id' => $contact_id
            ];
            $result->setData($resultData);
            return $result;
        }
        return $result->setData([
            'error' => true,
            'message' => 'Payment Error'
        ]);
    }
}
