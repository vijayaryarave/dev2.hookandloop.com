<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_payaconnect
 */

namespace Magenest\PayaConnect\Controller\Card;


use Magenest\PayaConnect\Model\Constant;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

/**
 * Class Save
 * @package Magenest\PayaConnect\Controller\Card
 */
class Save extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Magenest\PayaConnect\Model\VaultFactory
     */
    protected $vaultFactory;

    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Vault
     */
    protected $vaultResource;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $configHelper;

    /**
     * @var \Magenest\PayaConnect\Helper\DataHelper
     */
    protected $dataHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * Save constructor.
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magenest\PayaConnect\Model\VaultFactory $vaultFactory
     * @param \Magenest\PayaConnect\Model\ResourceModel\Vault $vaultResource
     * @param \Magento\Customer\Model\Session $customerSession
     * @param Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magenest\PayaConnect\Model\VaultFactory $vaultFactory,
        \Magenest\PayaConnect\Model\ResourceModel\Vault $vaultResource,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper,
        \Magenest\PayaConnect\Helper\DataHelper $dataHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        Context $context
    ) {
        $this->formKeyValidator = $formKeyValidator;
        $this->jsonFactory = $resultJsonFactory;
        $this->jsonHelper = $jsonHelper;
        $this->vaultFactory = $vaultFactory;
        $this->vaultResource = $vaultResource;
        $this->customerSession = $customerSession;
        $this->configHelper = $configHelper;
        $this->dataHelper = $dataHelper;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->jsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            if (!$this->formKeyValidator->validate($this->getRequest())) {
                return $result->setData([
                    'error' => true,
                    'message' => 'Payment Error'
                ]);
            }
            $cardData = $this->getRequest()->getParam('card_data');
            if ($cardData) {
                //save card
                $cardData = $this->jsonHelper->jsonDecode($cardData);
                $vaultModel = $this->vaultFactory->create();

                $vaultData = [
                    'customer_id' => $this->customerSession->getId(),
                    'card_id' => $cardData['id'],
                    'card_type' => $cardData['account_type'],
                    'last_four_digits' => $cardData['last_four'],
                    'masked_number' => 'xxxxxxxxxxxx' . $cardData['last_four'],
                    'expiration_date' => $cardData['exp_date'],
                    'created_at' => time(),
                ];

                $vaultModel->setData($vaultData);
                try {
                    $this->vaultResource->save($vaultModel);

                    //save address for contact paya
                    $contactId = $this->getRequest()->getParam('contact_id');
                    $curl = $this->configHelper->getCURL() . $this->dataHelper->getEndPoint(Constant::ENDPOINT_CONTACT) . '/' . $contactId;
                    $params = [
                        "contact" => [
                            "address" => $cardData['billing_address'],
                        ]
                    ];

                    $response = $this->dataHelper->sendRequest($curl, $this->jsonHelper->jsonEncode($params), 'PUT');
                    if (!isset($response['errors'])) {
                        $resultData = [
                            'success' => true,
                        ];
                        $this->messageManager->addSuccess(__('Card saved successfully.'));
                        $result->setData($resultData);
                        return $result;
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                }
            }
        }
        $this->messageManager->addError(__('Something wrong. Please try again.'));
        return $result->setData(['success' => false,]);
    }
}
