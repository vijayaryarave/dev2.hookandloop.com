<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Controller\Card;

use Magenest\PayaConnect\Controller\Card;

/**
 * Class Index
 * @package Magenest\PayaConnect\Controller\Card
 */
class Index extends Card
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('My Card Identifiers'));
        $this->_view->renderLayout();
    }
}
