<?php
/**
 * Copyright © 2020 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Payaconnect extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package Magenest_Payaconnect
 */

namespace Magenest\PayaConnect\Controller\Card;


use Magenest\PayaConnect\Controller\Card;
use Magenest\PayaConnect\Helper\DataHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magenest\PayaConnect\Helper\ConfigHelper;

/**
 * Class Delete
 * @package Magenest\PayaConnect\Controller\Card
 */
class Delete extends Card
{
    /**
     * @var \Magenest\PayaConnect\Model\ResourceModel\Vault
     */
    protected $_vaultResourceModel;

    /**
     * @var \Magenest\PayaConnect\Model\VaultFactory
     */
    protected $_vaultFactory;

    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    /**
     * @var \Magenest\PayaConnect\Helper\ConfigHelper
     */
    protected $_configHelper;

    /**
     * Delete constructor.
     * @param \Magenest\PayaConnect\Model\ResourceModel\Vault $vaultResourceModel
     * @param \Magenest\PayaConnect\Model\VaultFactory $vaultFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param Context $context
     * @param DataHelper $dataHelper
     * @param \Magenest\PayaConnect\Helper\ConfigHelper $configHelper
     */
    public function __construct(
        \Magenest\PayaConnect\Model\ResourceModel\Vault $vaultResourceModel,
        \Magenest\PayaConnect\Model\VaultFactory $vaultFactory,
        \Magento\Customer\Model\Session $customerSession, Context $context,
        \Magenest\PayaConnect\Helper\DataHelper $dataHelper,
        \Magenest\PayaConnect\Helper\ConfigHelper $configHelper
    )
    {
        $this->_vaultResourceModel = $vaultResourceModel;
        $this->_vaultFactory = $vaultFactory;
        $this->_dataHelper = $dataHelper;
        $this->_configHelper = $configHelper;
        parent::__construct($customerSession, $context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $vaultResourceModel = $this->_vaultResourceModel;
            $vault = $this->_vaultFactory->create();
            $vaultResourceModel->load($vault, $id);

            if ($vault->isOwnCard($this->_customerSession->getCustomerId())) {
                $vaultResourceModel->delete($vault);

                //delete card in paya dashboard
                if ($this->_configHelper->getValueConfig(ConfigHelper::DELETE_ACCOUNT_VAULT)) {
                    $curl = $this->_configHelper->getCURL() . '/v2/' . 'accountvaults/' . $vault->getData('card_id');
                    $this->_dataHelper->sendRequest($curl, null, 'DELETE');
                }

                $this->messageManager->addSuccessMessage(__('Successfully deleted the card'));
            } else {
                $this->messageManager->addErrorMessage(__("Can't specify card"));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __("Error"));
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('payaconnect/card/index');
    }
}
