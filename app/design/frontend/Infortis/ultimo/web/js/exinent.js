requirejs(['jquery', 'Magento_Ui/js/modal/confirm', 'mage/url', 'Magento_Catalog/js/price-utils'], function ($, confirmation, urlBuilder, priceUtils) {
    'use strict';
    /*---------home page changes---------*/
    $(document).ready(function () {
        if ($(window).width() < 768) {
            $('.categorypath-products-cable-ties-velcro-brand-one-wrap .product-item').each(function () {
                var image = $(this).find('.product-item-info');
                $(this).find('.product-item-details .product-item-name').insertBefore(image);
            });
        }        

    });
});