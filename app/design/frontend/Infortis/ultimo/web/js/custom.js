requirejs(['jquery', 'Magento_Ui/js/modal/confirm', 'mage/url', 'Magento_Catalog/js/price-utils'], function ($, confirmation, urlBuilder, priceUtils, uiRegistry) {

    'use strict';

   /* $(window).on("load", function () {
        $('.nav-item:last-child .level-top span:first-child').text('Conversion Services');
    });*/
    $(document).ready(function () {
        $('[data-role="tocart-form"] button').prop('disabled',false);
        var clrInt =   setInterval(function(){
            if($('.checkout-index-index label').length){
                $('.checkout-index-index label').each(function(){
                    $(this).find('span').each(function(){

                        if($(this).text()=='Priority Mail 3-Day' || $(this).text()=='Priority Mail 2-Day' || $(this).text()=='Priority Mail 1-Day'){
                        // $(this).addClass('hellocm');
                        $(this).text('Priority Mail');
                        // console.log('hello');
                        clearInterval(clrInt);
                    }


                });
                });
            }
        },1000);

    });

    $(document).on('keyup',function(){
        var clrInte =   setInterval(function(){
            if($('.checkout-index-index label').length){
              $('.checkout-index-index label').each(function(){
                  $(this).find('span').each(function(){

                      if($(this).text()=='Priority Mail 3-Day' || $(this).text()=='Priority Mail 2-Day' || $(this).text()=='Priority Mail 1-Day'){
                          // $(this).addClass('hellocm');
                          $(this).text('Priority Mail');
                          // console.log('hello');
                          clearInterval(clrInte);
                      }


                  });
              });
          }
      },1000);
    });
    $(document).on('keyup',function(){
        var clrInteg =   setInterval(function(){
            if($('.checkout-cart-index label').length){
              $('.checkout-cart-index label').each(function(){
                 if($(this).text()=='Priority Mail 3-Day' || $(this).text()=='Priority Mail 2-Day' || $(this).text()=='Priority Mail 1-Day'){
                          // $(this).addClass('hellocm');
                          $(this).text('Priority Mail');
                          // console.log('hello');
                          clearInterval(clrInteg);
                      }
                  });
          }
      },1000);
    });
    $(document).ready(function () {
        var clrIntg=   setInterval(function(){
            if($('.checkout-cart-index label[for="s_method_usps_1"]').length){
                var spl = $('.checkout-cart-index label[for="s_method_usps_1"]').text();
                var sp = spl.trim();
                var ast = sp.toString();
                var sg2 = ast.indexOf('2-Day');
                var sg1 = ast.indexOf('1-Day');
                var sg3 = ast.indexOf('3-Day');
                if( ast || sg1 || sg3 ){
                    $('label[for="s_method_usps_1"]').text()
                }
                $('.checkout-cart-index label').each(function(){
                  if($(this).text()=='Priority Mail 3-Day' || $(this).text()=='Priority Mail 2-Day' || $(this).text()=='Priority Mail 1-Day'){
                      $(this).addClass('hellocm');
                      $(this).text('Priority Mail');
                              // console.log('hello');
                              clearInterval(clrIntg);
                          }
                      });}
            },1000);
    });

    $(document).ready(function () {

     $('.wishlist-index-index .actions-primary button').removeClass('tocart'); 

     $('button.action.tocart.primary').click(function () {

        var clr = setInterval(function () {
            if($('.catalogsearch-result-index #mb-ajaxcart-wrapper').length){
                $('.catalogsearch-result-index #mb-ajaxcart-wrapper .product-name').addClass('pro_name');
                if ($('.catalogsearch-result-index #mb-ajaxcart-wrapper .product-name').hasClass('pro_name')) {
                    $('.catalogsearch-result-index #mb-ajaxcart-wrapper .product-name').insertBefore('.catalogsearch-result-index #mb-ajaxcart-wrapper .fieldset:nth-child(1)');
                    $('.catalogsearch-result-index #mb-ajaxcart-wrapper .box-tocart .product-name').removeClass('pro_name').remove();
                    $('.catalogsearch-result-index #mb-ajaxcart-wrapper .swatch-opt').insertAfter('.catalogsearch-result-index #mb-ajaxcart-wrapper .product-name:nth-child(1)');

                    // clearInterval(clr);
                }
            }
        }, 1000);
    });

     if ($('body').hasClass('checkout-index-index')) {
        var creditCardExist = setInterval(function () {
            if (($('#magenest_sagepayus').length))
            {
                $('#magenest_sagepayus').trigger('click');
                clearInterval(creditCardExist);
            }
        }, 1000);
    }


    $('.industry-product .orange-button > a').addClass('btn_thme_primary');

    $('#enquiry_mirror').change(function () {
        $('#enquiry').val($(this).val());
    });

    /*menu*/
    var checkExist = setInterval(function () {
            // $('.even .price-sec:first-child ').each(function(index){

            // alert(index);
            // var y;
            // var x;
            // var z;
            // var e;
            // x=$('.product-name-dummy').text();
            // y=x.replace(/\$/g, '');
            // z=parseFloat(y,10);
            // e=z.toFixed(2);
            //   alert(e);
            // })
            if (($('#co-billing-form').length) && ($('.grand.totals th').length) && ($('.grand.totals td').length))
            {
                $('#co-billing-form label em').each(function () {
                    $(this).insertBefore($(this).prev('span'));
                });
                $('#checkout-review-submit').insertBefore('.mw-osc-threecolumns');
                $('#checkout-review-table tfoot tr.totals th, #checkout-review-table tfoot tr.totals-tax th').attr('colspan', '3');
                $('#checkout-review-table tfoot tr.totals td, #checkout-review-table tfoot tr.totals-tax td').attr('colspan', '2');
                $('#checkout-review-table .totals.sub th').text('Subtotal');
                clearInterval(checkExist);
            }
        }, 1000);

    setTimeout(function () {
        $('.customer-account-forgotpassword .actions-toolbar .secondary a').html('<small>&laquo; </small><span>Back to Login</span>');
    }, 100);

    $('.customer-account-forgotpassword .page-title-wrapper h1 span').text('Forgot Your Password?');
    $('.skip-account').addClass('skip-account-label');
    if ($('.skip-account').hasClass('skip-account-label')) {
        $('#header-account').css('display', 'block');

        $('.header-m-primary .inner-container ul li').each(function () {
            if ($(this).text() == "MY ACCOUNT") {
                $(this).addClass('myacc_wrapper');
            }
        });
    }
    $('.checkout-index-index .header-mobile .logo-wrapper').append($('.checkout-index-index .header-container-micro .logo'));
    $('.checkout-index-index .header-mobile').addClass('header-regular');
    $('.page-product-configurable .product.attribute.sku').css('display', 'none');
    $('.page-product-configurable.catalog-product-view .price-box.price-final_price').hide();
    $('.page-product-configurable.catalog-product-view .configurable-pricerange').show();
    setTimeout(function () {
        $(".product-lower-secondary-column").insertAfter(".product-lower-primary-column");
    }, 100);
    var elementcon = setInterval(function () {

        if ($('#product-options-wrapper .swatch-attribute').length) {
            $('#product-options-wrapper .swatch-attribute').each(function () {
                if ($(this).attr('attribute-code') == "wide_loop_type") {
                    $(this).css('height', '75px');
                    $(this).css('border-bottom', 'none');
                } else if ($(this).attr('attribute-code') == "color") {
                    if ($(this).find('.swatch-option').length <= 14) {
                        $(this).css('height', '90px');
                    }
                } else if ($(this).attr('attribute-code') == "configuratble_width") {
                    if ($(this).find('.swatch-option').length < 8) {
                        $(this).css('height', '100px');
                    }
                }

            });
            clearInterval(elementcon);
        }
    }, 1000);
});

$(document).ready(function () {

    $('.sales-order-view .block-order-details-view').insertAfter('.sales-order-view .page-title-wrapper');
    $('.account .account-nav-content .items .item').each(function () {
        if ($(this).text() == "My Downloadable Products") {
            $(this).remove();
        }
        if ($(this).text() == "Stored Payment Methods") {
            $(this).remove();
        }else if ($(this).text() == "Account Information")
        {
            $(this).insertAfter('.account .account-nav-content .items .item:first-child');
        } else if ($(this).text() == "Address Book") {
            $(this).insertAfter('.account .account-nav-content .items .item:nth-child(1)');
        }

    });
    $('.sales-order-view .page-title-wrapper .order-status').prepend(' - ');
    $('.sales-order-view .order-actions-toolbar .actions .action.order span').prepend(' | ');
    $('.sales-order-view .page-title-wrapper .order-date').insertAfter('.sales-order-view .block-order-details-view .block-title');

    $('<div class="block-title"><strong><span>My Account</span></strong></div>').insertBefore('ul.nav.items');
    $('.toolbar-amount').append('(S)');
    $('a.action.back').prepend("<<");
    $('.sales-order-history .table-order-items .status').prepend("order");
    $('.block-dashboard-orders').insertBefore('.block-dashboard-info');
});
$(document).on('click', '#product-addtocart-button', function () {
    var checkExist = setInterval(function () {
        if ($('.mb-ajaxsuite-popup').length) {
            $('.mb-ajaxsuite-popup .ajaxsuite-buttons').appendTo('.mb-ajaxsuite-popup .mb-ajaxsuite-popup-border');
            $('.mb-ajaxsuite-popup-wrapper').draggable({disabled: true});
            clearInterval(checkExist);
        }
    }, 1000);
});
$(document).on('click', '.page-product-configurable #product-addtocart-button', function () {
    var checkExist = setInterval(function () {
        if ($('div#mb-ajaxsuite-popup-wrapper').css('display') == 'block') {
            var simplesku = $("div.product-info-main .sku a").text();
            $("#mb-ajaxsuite-popup-wrapper .product-sku span, #mb-ajaxsuite-popup-wrapper #popuppartnmber").text("Part #: " + simplesku);
            clearInterval(checkExist);
        }
    }, 1000);
});
/* cutlenght piece count */

//    $('input[type="text"].product-custom-option').attr('type', 'number');
$(document).on('keyup', 'input[type="text"].product-custom-option', function (event) {
    var piecelen = $(this).val();
//        var piecelen = parseInt(piecelen);
var num = "";
var array = piecelen.split('');
for (var i = 0; i < array.length; i++)
{
    if (array[i] == '.') {
        num += array[i];
    } else if (array[i] == ' ')
    {

    } else if (!isNaN(array[i]))
    {
        num += array[i];
    } else
    {
        break;
    }
}
if (num === "0" || num === "" || typeof num === "undefined" || isNaN(num))
{
    $('#calculator-cut-piece').text("0");
    $('#calculator-total-pieces').text('1 Piece');
} else
{
    $('#calculator-cut-piece').text(num);
    var inches = parseInt($('#calculator-total-inches').text());
    var pieces = inches / num;
    $('#calculator-total-pieces').text(Math.round(pieces) + ' Pieces');
}
});

/*end*/
$(document).ready(function () {

    $('.copyright span').append("<span class='trademark'>&trade;</span>");
    $('.copyright span .trademark').css('font-size', '20px');
    $('.message.info').insertBefore('.category-description .row');
    $('.mini-cart-heading').on('mouseover', function () {
        $(this).find('span').css('color', 'red');
    });
    $('.mini-cart-heading').on('mouseout', function () {
        $(this).find('span').css('color', 'black');
    });
    $('.mini-cart-heading').on('mouseover', function () {
        $('.mini-cart-content').css('display', 'none');
    });
    $('.mini-cart-heading').click(function () {
        var cartUrl = urlBuilder.build('checkout/cart/');
        window.location.href = cartUrl;
    });
        //


    });


$(document).ready(function () {

        //end



        // cart-page-no-disabled
        $('.item-options').each(function () {
            $(this).find('dd').each(function () {
                var x = $(this).text();
                var trimStr = $.trim(x);
                if (trimStr == 'No') {
                    $(this).prev().remove();
                    $(this).remove();
                }
            });
        });
        //end
        //speciality-products-page
        var matchedEle;
        var textEle;
        $('<div class="your-selection"><label class="">Your selections:</label><div class="content" style="display: block;">(Please select products on the right that you would like more information about)</div><ul class="speicalty-product-list"></ul></div>').insertBefore('.pull-left .form');
        $('.big.orange.nice').click(function () {
            textEle = $(this).attr('onclick');
            matchedEle = textEle.match(/\((.*)\)/);
        });
        $('.cms-specialty-products .webforms .your-selection').insertBefore('.cms-specialty-products .webforms fieldset');
        $('.cms-specialty-products .webforms .actions-toolbar button[title="Submit"] span').text('Get More Info');
        function addSP(title) {


            $(this).find('button.big.orange.nice.btn_thme_primary.radius').text();
            $('.your-selection .content').hide();
            var already_exist = 0;
            $('.pull-left ul.speicalty-product-list li').each(function (index, element) {
                if ($(this).find('span').html() == matchedEle[1]) {
                    already_exist = 1;
                }
            });


            var currentVal = $('.selected-products').val();
            if (currentVal.indexOf(matchedEle[1]) > -1) {
              already_exist = 1;
          }


           /* if (already_exist == 0) {
                $('.selected-products').val(currentVal + "<li>"+matchedEle[1]+"</li>\n");
            }*/

            if (already_exist == 0) {
                $('.pull-left ul.speicalty-product-list').append('<li><span>' + matchedEle[1] + '</span> <a onclick="removeListItem(this)"><i class="fa fa-times"></i></a></li>');
                $('.selected-products').val(currentVal + matchedEle[1]+"\n");
            }

            
            $('.pull-left ul.speicalty-product-list li').each(function () {
                $(this).find('a').click(function () {
                    $(this).parent().remove();
                    var selectedItems = $('.selected-products').val();
                    var removedItem = $(this).parent().find('span').text();
                    var currentItems = selectedItems.replace(removedItem,'');
                    $('.selected-products').val(currentItems);

                });
            });
            specialtyList();

        }

        // function specialtyList() {
        //     var itemList = '';
        //     $('.pull-left ul.speicalty-product-list li').each(function (index, element) {
        //         itemList += $(this).find('span').html() + '\n';

        //     });

        // }


        function specialtyList() {
            var itemList = '';
            $('.pull-leftt ul.speicalty-product-list li').each(function (index, element) {
                itemList += $(this).find('span').html() + '\n';
            });
            $('textarea#specialty-products-list').val(itemList);
        }

        $('button.big.orange.nice.btn_thme_primary.radius').click(addSP);
        //     $('<div class="your-selection"><label class="">Your selections:</label><div class="content" style="display: block;">(Please select products on the right that you would like more information about)</div><ul class="speicalty-product-list"></ul></div>').insertBefore('.pull-left .form');
        //     function addSP(title){
        //         alert();
        //         // $('.your-selection .content').hide();
        //     var already_exist=0;
        //     $('.specialty-product-ul ul.speicalty-product-list li').each(function(index,element){
        //         if($(this).find('span').html()==title){already_exist=1;}
        //     });
        //         if(already_exist==0){
        //             $('.specialty-product-ul ul.speicalty-product-list').append('<li><span>'+title+'</span> <a onclick="removeListItem(this)"><i class="fa fa-times"></i></a></li>');
        //         }
        //     specialtyList();
        // }
        //     function removeListItem(listItem){
        //         $(listItem).parent('li').remove();
        //         if($('.specialty-product-ul ul.speicalty-product-list li').length<=0){
        //             $('.your-selection .content').show();
        //         }
        //     specialtyList();
        // }
        //    function specialtyList(){
        //         var itemList='';
        //         $('.specialty-product-ul ul.speicalty-product-list li').each(function(index,element){
        //             itemList+=$(this).find('span').html()+'\n';
        //         });
        //             $('textarea#specialty-products-list').val(itemList);
        //         }
        $(document).on('click', '.btn_thme_primary', function () {


            //    customArr.push($(this).parent().find('strong').text());
            //     if(customArr.length>0){

            //     for(i=0;i<customArr.length;i++){
            //         customerArrBkp.push(customArr[i]);
            //         // if(customerArrBkp[i]==)
            //         // alert(customerArrBkp);
            //     }
            // }

            //    $('.cms-specialty-products #field_aBEsXB104 textarea').val(customerArrBkp);
            //     // customerArrBkp.appendTo('.cms-specialty-products #field_aBEsXB104 textarea')
        });

    });


$(document).ready(function () {
    $('.post-entry .post-excerpt').each(function () {

        $(this).find('a').insertAfter($(this));

    });
    if ($('.post-list-wrapper .toolbar-number:contains("total")')) {
        var str = $('.post-list-wrapper .toolbar-number').text();
        $('.post-list-wrapper .toolbar-number').append('Pages');

    }
        // blog
        $('.is-blog .sidebar-main #quick_links').insertBefore('.is-blog .sidebar-main .wp-sidebar');
        $('.is-blog .blog-search .button').addClass('btn_thme_primary');
        $('.is-blog .blog-search .input-text').attr('placeholder', 'Search Posts');
        $('.is-blog .blog-search .input-text').on({'click': function () {
            $(this).attr('placeholder', '');
        },
        'mouseover': function () {
                //end
            }


        });
        if ($('.wordpress-post-view .post-content a img').hasClass('aligncenter')) {
            console.log('No class');
        } else {
            $('.wordpress-post-view .post-content a img').addClass('aligncenter');
        }

        
        
    });

$('input[type="radio"].product-custom-option').on('change', function() {    
    if ($.trim($('input[type="radio"].product-custom-option:checked').siblings('label').text()) === 'No') {
     $('#custom_cut_to_length_notice').hide();
     $('input[type="radio"].product-custom-option:checked').val('');
 } 
 else if($('input[type="radio"].product-custom-option:checked').siblings('label').text().indexOf('Yes') >= 0){
    $('#custom_cut_to_length_notice').show();
}
});
$(document).ajaxComplete(function () {
    $('input[type="radio"].product-custom-option').change(function () {
        if ($.trim($('input[type="radio"].product-custom-option:checked').siblings('label').text()) === 'No') {
            $('#custom_cut_to_length_notice').hide();       
            $('#calculator-cut-piece').text("0");
            $('#calculator-total-pieces').text('1 Piece');
            $('input[type="text"].product-custom-option').val('');
            $('input[type="radio"].product-custom-option:checked').val('');
        } 
        else if($('input[type="radio"].product-custom-option:checked').siblings('label').text().indexOf('Yes') >= 0){
            $('#custom_cut_to_length_notice').show();
        }
        var qty = parseInt($('input[name="qty"]').val());
        var price = $('.price-final_price .price-wrapper').attr('finalprice');
        var finalprice = qty * price;
        if (qty > 0) {
            finalprice = priceUtils.formatPrice(finalprice);
            $('.price-final_price .price-wrapper .price').text(finalprice);

        }
    });
    $('input[type="text"].product-custom-option').blur(function () {
        var qty = parseInt($('input[name="qty"]').val());
        var price = $('.price-final_price .price-wrapper').attr('finalprice');
        var finalprice = qty * price;
        if (qty > 0) {
            finalprice = priceUtils.formatPrice(finalprice);
            $('.price-final_price .price-wrapper .price').text(finalprice);
        }
    });
});

$(document).ajaxComplete(function () {

    $('.page-product-configurable .hook_loop').each(function () {
            // alert($(this).length);
            if ($(this).find('.swatch-attribute')) {
                var pro = $(this).find('.swatch-attribute-options .text');
                if (pro.length > 0) {
                    $(this).addClass('attrVar');
                }
            }
            if ($('.page-product-configurable .hook_loop').hasClass('attrVar')) {
                var varLen = $(this).find('.text').length;

                if (varLen <= 3) {
                    $(this).find('.text').addClass('HookLoopCustomClass');

                }
            }

        });

        //custom code for blog

        //end

    });

    // product-view-page
    $(document).ready(function () {
        $("<div class='cstm_notify'><a href='/hookandloop/custom-strap-form/'>Custom colors, logo imprinting, lengths and quantities are available.</a></div>").insertAfter(".page-product-configurable.product-hook-and-loop-straps-with-velcro-brand-fasteners .box-tocart, .page-product-configurable.product-duragrip-brand-hook-and-loop-straps .box-tocart");
        setTimeout(function () {
            $('.page-product-configurable.product-hook-and-loop-straps-with-velcro-brand-fasteners .price-container.price-final_price .price-wrapper, .page-product-configurable.product-duragrip-brand-hook-and-loop-straps .price-container.price-final_price .price-wrapper').append('<div class="f-right min_order" style="display: block;"><span> $100.00 minimum order for straps</span></div>');
        }, 1000);
        $('.product-info-price').insertAfter($('.product-add-form .reset_swatches'));
        $('#tab-label-reviews #tab-label-reviews-title').text("Product's Review");
//        setTimeout(function () {
//            $('.product-add-form form > .field:nth-child(8) > .label').append('<span class="note"><a href="/hookandloop/converting/strip-cutting">Not sure what this means?</a></span>');
//        }, 500);
$('.product.attribute.sku > strong').text('Part Number:');
$('.product.attribute.sku').insertBefore('.product.attribute.overview');
$('.magnifier-preview').css('width', 'auto');
$('.catalog-product-view .custom-sidebar-right').first().insertAfter('.catalog-product-view .product-add-form');

var bothButton = setInterval(function () { 
  var hookandloop_both ='<div class="swatch-option text HookLoopCustomClass hookloop-both" id="option-label-hook_loop-174-item" aria-checked="false" aria-describedby="option-label-hook_loop-174" tabindex="0" option-type="0" option-id="180-181" option-label="Hook" aria-label="Both" option-tooltip-thumb="" option-tooltip-value="Both" role="option">Both</div>';
  if($('#option-label-hook_loop-174-item-181').length > 0){ 
    $('#option-label-hook_loop-174-item-181').after($(hookandloop_both));
    $('#option-label-hook_loop-174-item').css("display","none");            
    clearInterval(bothButton);
}
}, 3000);

});

    /*Both button Product detail page*/   
    var option_selected = 0;
    var option_hookloop_or = 0;
    $(document).on('click', '.configuratble_width .swatch-attribute-options .swatch-option', function (e) {
        
       if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) {
         $('#option-label-hook_loop-174-item').css("display","none");
         $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');
         option_selected = 0;
     }else{ 
       option_selected = 1;
       $('#option-label-hook_loop-174-item').css("display","block");
       $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
   }
});
    $(document).on('click', '.color .swatch-attribute-options .swatch-option', function (e) {
       
        if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) { 
            $('#option-label-hook_loop-174-item').css("display","none");
            $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');
            option_selected = 0;
        }else{
            option_selected = 1;
            $('#option-label-hook_loop-174-item').css("display","block");
            $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
        }
    });   


    $(document).on('mouseover', '.configuratble_width .swatch-attribute-options .swatch-option', function (e) {
       if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) {
         $('#option-label-hook_loop-174-item').css("display","none");
         $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');
         option_hookloop_or = 0; 
     }else{ 
         $('#option-label-hook_loop-174-item').css("display","block");
         $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
         option_hookloop_or = 1;
     }
 });
    $(document).on('mouseover', '.color .swatch-attribute-options .swatch-option', function (e) {
        if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) { 
            $('#option-label-hook_loop-174-item').css("display","none");
            $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');
            option_hookloop_or = 0;
        }else{
         $('#option-label-hook_loop-174-item').css("display","block");
         $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
         option_hookloop_or = 1;
     }
 });   
    
    $(document).on('mouseout', '.configuratble_width .swatch-attribute-options .swatch-option', function (e) {
        if(option_selected == 0)
        {
            $('#option-label-hook_loop-174-item').css("display","none");
        }else{
         if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) {
            $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');  
        }else{
           $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
           $('#option-label-hook_loop-174-item').css("display","block");
       }
   }

});

    $(document).on('mouseout', '.color .swatch-attribute-options .swatch-option', function (e) {
        if(option_selected == 0)
        {
            $('#option-label-hook_loop-174-item').css("display","none");
        }else{
            if($('.swatch-option.text.HookLoopCustomClass').hasClass('disabled')) {
                $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').addClass('displaynone');  
            }else{
               $('.catalog-product-view .product-view .swatch-attribute .swatch-option.text.HookLoopCustomClass').removeClass('displaynone');
               $('#option-label-hook_loop-174-item').css("display","block");
           }
       }

   });  

    /*end Both button Product detail page*/ 



    /*customer-account-login*/
    $(document).ready(function () {
        //custom code 3/27/2019

        // $('.catalogsearch-result-index .product-options-wrapper .fieldset .swatch-opt').insertAfter('.catalogsearch-result-index .product-options-wrapper .fieldset');
        //end
        $('.customer-account-login .form-create-account .field').each(function () {
            if ($(this).hasClass('newsletter')) {
                $(this).insertAfter('.form-create-account .confirmation');
            }
        });
        $('.customer-account-login .form-create-account .info .legend span').replaceWith('<span>New Users</span>');
        $('.customer-account-login .page-title-wrapper .page-title .base').replaceWith('<span class="base" data-ui-id="page-title-wrapper">Login or Create an Account</span>');
        $('.customer-account-login .form-create-account .account .legend span').hide();
        $('.customer-account-login .form-create-account').wrap('<div class="account_create_wrapper"></div>');
        $('.customer-account-edit').find('#email').prop('disabled', true);



        $('#change-email').prop('checked', true);
        $('#change-email').prop('disabled', true);
        $('#change-email ').css('display', 'none');

        $(document).on('click', '#change-password', function () {

            if ($('#change-password').prop('checked')) {

                $('.form-edit-account').find('.password').show();

            } else {
                $('.form-edit-account').find('.password').hide();
            }
        });
        if ($('#change-email').prop('checked', true)) {
            $('.form-edit-account').find('.password').hide();
        }
        if ($('#change-password').prop('checked')) {

            $('.form-edit-account').find('.password').show();

        }


        // $('.catalog-product-view .product.attribute.overview a[href="^https"]').replaceWith("http");

        var celem = $('a[href*="https://hookandloop.com"]');
        var elembase_url = BASE_URL;
        for (var i = 0; i < celem.length; i++) {
            var ele = $(celem[i]).prop('href');
            $(celem[i]).prop('href', ele.replace('https://hookandloop.com/', elembase_url));
            ele = $(celem[i]).prop('href');
        }
        $('.form-edit-account .password').find('.legend').hide();
        $(document).on('click', '#change-password', function () {
            if ($('.form-edit-account').find('#change-password').prop('checked')) {
                $('.form-edit-account .password').find('.legend').show();
            } else {
                $('.form-edit-account .password').find('.legend').hide();
            }
        });

        $('.page.messages').insertBefore('.page-wrapper');
        // $('.page.messages .message').wrapInner('<div id="hide_message" class="test" style="" title="Hide messages"><span>&times;</span></div>');
        $('.page.messages').delay(1000).show(3000);
        $('.customer-account-edit .field.email.required').insertAfter('.customer-account-edit .field-name-lastname');
        $('.page.messages .message div').append("<a href='#' class='close'>&times;</a>");

    });
    /*end*/

    $(document).on('click', '#hide_message', function () {
        $(this).parentsUntil('.page.messages').slideToggle(2000);
    })
    /*category page-industries */
    $(document).ready(function () {
        $('.page-product-configurable .additional-attributes-wrapper table tr:first-child').hide();
        $('.page-product-configurable #product-addtocart-button').prop('disabled', true);
        $('.product-social-links').appendTo('#product_addtocart_form');
        $('.product-options-wrapper > .fieldset > .field').appendTo('#product_addtocart_form');

        setInterval(function () {
            if($('.hook_loop .swatch-attribute-options').length){
                $('.hook_loop .swatch-attribute-options').each(function () {
                    var len = $(this).children().length;
                    if (len == "1")
                    {
                        $(this).addClass('only_one');
                    }
                });
                $('.additional-attributes .brands').insertAfter('.additional-attributes .sku');
                $('.additional-attributes .configuratble_width').insertAfter('.additional-attributes .brands');
                $('.additional-attributes .measurement_sold_in_unit').insertAfter('.additional-attributes .hook_loop');
            }}, 100);
    });
    //     if($('body').hasClass('catalog-product-view')){
    //    var attrId = $('[data-price-type="finalPrice"]').attr('id');

    //     var spl = attrId.split("-");
    //             console.log(spl[2]);
    //             $.ajax({
    //                 url: BASE_URL+"partialshipping/index/skuvalue",
    //                 type: 'POST',
    //                     data: {is_ajax:1,spl:spl[2]},
    //                     success: function(result){

    //                     if(result.type="simple"){
    //                         setInterval(function(){
    //                             $('#soldunit').text('Sold On '+result.size+' Yards Per Roll');
    //                             var sld = $('#soldunit').text();
    //                             $("[data-th='Measurement Sold In Unit']").text(sld);
    //                         },1000);
    //                     }

    //                 }
    //             });
    //     }
    /*end*/
    $(document).on('click', ".reset_swatches", function () {
        $(".swatch-option").removeClass("selected");
        $(".swatch-option").attr("aria-checked", "false");
        $(".swatch-attribute").removeAttr("option-selected");
        $(".swatch-attribute-selected-option").text("");
        $('#backorder_lead_time').html('');
        $('.price-label').css('display', 'inline');
        var price = $('.product-info-price .price-wrapper').attr('data-price-amount');
        $('.price-final_price .price').text('$' + price);
//        $('.page-product-configurable #product-addtocart-button').prop('disabled', true);
$('.page-product-configurable #product-addtocart-button').css('background-color', '#999');
$(".swatch-option").each(function () {
    $(this).removeClass('disabled');
    $(this).removeAttr('disabled');
});
$('.page-product-configurable.catalog-product-view .price-box.price-final_price').hide();
$('.page-product-configurable.catalog-product-view .configurable-pricerange').show();
$('#option-label-hook_loop-174-item').css("display","none");
option_selected = 0;
option_hookloop_or == 0;
});
    $(document).on('click', '.main-container .swatch-option', function () {
        var flag = 0;
        $('.product-options-wrapper .swatch-attribute').each(function () {
            var attr = $(this).attr('option-selected');
            if (typeof attr == "undefined")
            {
                flag = 1;
            }
//            var attr = $(this).find('.swatch-attribute-selected-option').text();
//            if (typeof attr == '')
//            {
//                flag = 1;
//            }
});
        if (flag == 0)
        { 

            $('.page-product-configurable .product.attribute.sku').css('display', 'block'); 
            var checkExist = setInterval(function () {
                if ($('.additional-attributes .brands').length)
                {
                    $('.additional-attributes .brands').insertAfter('.additional-attributes .sku');
                    $('.additional-attributes .configuratble_width').insertAfter('.additional-attributes .brands');
                    $('.additional-attributes .measurement_sold_in_unit').insertAfter('.additional-attributes .hook_loop');
                    clearInterval(checkExist);
                }
            }, 1000);
            $('.page-product-configurable .additional-attributes-wrapper table tr:first-child').show();
            $('.page-product-configurable.catalog-product-view .price-box.price-final_price').show();
            $('.page-product-configurable.catalog-product-view .configurable-pricerange').hide();
            $('.page-product-configurable #product-addtocart-button').prop('disabled', false);
            var qty = parseInt($('input[name="qty"]').val());
            var price = $('.price-final_price .price-wrapper').attr('finalprice');
            if (typeof price == typeof undefined) {
                var price = $('.price-final_price .price-wrapper').attr('data-price-amount');
            }
            var finalprice = qty * price;
            if (qty > 0) {
                finalprice = priceUtils.formatPrice(finalprice);
                $('.price-final_price .price-wrapper .price').text(finalprice);
            }
        } else
        {
            $('.page-product-configurable #product-addtocart-button').prop('disabled', true);
        }
        $('.page-product-configurable.product-hook-and-loop-straps-with-velcro-brand-fasteners .price-container.price-final_price .price-wrapper, .page-product-configurable.product-duragrip-brand-hook-and-loop-straps .price-container.price-final_price .price-wrapper').append('<div class="f-right min_order" style="display: block;"><span> $100.00 minimum order for straps</span></div>');
        $('.hook_loop .swatch-option.text').each(function () {
            if ($(this).hasClass('disabled'))
            {
                $(this).parent('.swatch-attribute-options').addClass('noafter');
            } else
            {
                $(this).parent('.swatch-attribute-options').removeClass('noafter');
            }
        });
        $(".input-text.qty").removeAttr("min");
    });
    $('input#qty').on('change', function (e) {
        e.preventDefault();
        $('.page-product-configurable #product-addtocart-button').prop('disabled', false);
        var qty = parseInt($('input[name="qty"]').val());
        var price = $('.price-final_price .price-wrapper').attr('finalprice');
        if (typeof price == typeof undefined) {
            var price = $('.price-final_price .price-wrapper').attr('data-price-amount');
        }
        var finalprice = qty * price;
        if (qty > 0) {
            finalprice = priceUtils.formatPrice(finalprice);
            $('.price-final_price .price-wrapper .price').text(finalprice);
        }
    });
    $(document).on('click', '.qty-button', function (e) {
        e.preventDefault();
        var value = $('.input-text.qty').val();
        var min = parseInt($('.input-text.qty').attr('min'));
        if (value < 1)
            $('.input-text.qty').val("1");
        else if (value < min)
            $('.input-text.qty').val(min);
        $('.page-product-configurable #product-addtocart-button').prop('disabled', false);
        var qty = parseInt($('input[name="qty"]').val());
        var price = $('.price-final_price .price-wrapper').attr('finalprice');
        if (typeof price == typeof undefined) {
            var price = $('.price-final_price .price-wrapper').attr('data-price-amount');
        }
        var finalprice = qty * price;
        if (qty > 0) {
            finalprice = priceUtils.formatPrice(finalprice);
            $('.price-final_price .price-wrapper .price').text(finalprice);
        }
    });

    $(document).on('keyup', '.input-text.qty', function () {
        var value = $(this).val();
        var min = parseInt($(this).attr('min'));
        if (value < 1)
            // $(this).val("1");
        $(this).val(value);
        else if (value < min)
            $(this).val(min);
    });

    $(document).on('hover', '.product-options-wrapper .swatch-attribute .swatch-option', function () {
        var optionval = $(this).text();
        var qty = parseInt($('input[name="qty"]').val());
        $(this).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
        var price = $(this).parents('.product-add-form').find('.price-final_price .price-wrapper').attr('finalprice');
        var finalprice = qty * price;
        finalprice = priceUtils.formatPrice(finalprice);
        $(this).parents('.product-add-form').find('.price-final_price .price-wrapper > .price').text(finalprice);
    });
    $(document).on('mouseout', '.strap_type .swatch-option.text', function () {
        var optionval = "";
        $('.strap_type .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.strap_type.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.configuratble_width .swatch-option.text', function () {
        var optionval = "";
        $('.configuratble_width .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.configuratble_width.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.catalogsearch-result-index .configuratble_width .swatch-option.text', function (e) {
        e.preventDefault();
        // var optionval = "";
        // $('.configuratble_width .swatch-option.text').each(function () {
        //     if ($(this).hasClass('selected'))
        //     {
        //         optionval = $(this).text();
        //     }
        // });
        // $('.configuratble_width.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });

    $(document).on('mouseout', '.length .swatch-option.text', function () {
        var optionval = "";
        $('.length .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.length.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.hook_loop .swatch-option.text', function () {
        var optionval = "";
        $('.hook_loop .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.hook_loop.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.size .swatch-option.text', function () {
        var optionval = "";
        $('.size .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.size.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.wide_loop_type .swatch-option.text', function () {
        var optionval = "";
        $('.wide_loop_type .swatch-option.text').each(function () {
            if ($(this).hasClass('selected'))
            {
                optionval = $(this).text();
            }
        });
        $('.wide_loop_type.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('hover', '.product-options-wrapper .swatch-attribute .swatch-option.color', function () {
        var optionval = $(this).attr('aria-label');
        $(this).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute .swatch-option.color', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute .swatch-option.color').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute.adhesive .swatch-option', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute.adhesive .swatch-option').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute.webbing_weight .swatch-option', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute.webbing_weight .swatch-option').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute.ring_shape .swatch-option', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute.ring_shape .swatch-option').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute.style .swatch-option', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute.style .swatch-option').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    $(document).on('mouseout', '.swatch-attribute.mva8_type .swatch-option', function () {
        var thisevent = $(this);
        var optionval = "";
        $('.swatch-attribute.mva8_type .swatch-option').each(function () {
            if ($(this).hasClass('selected'))
                optionval = $(this).attr('aria-label');
        });
        $(thisevent).parents('.swatch-attribute').find('.swatch-attribute-selected-option').text(optionval);
    });
    var urlpath = window.location.href;
    var color = urlpath.substr(urlpath.indexOf("=") + 1);
    color = color.replace('%20', ' ');
    if (urlpath.indexOf("=") > 1) {
        var checkExist = setInterval(function () {
            if ($('.swatch-option.color').length) {
                $('.swatch-option.color').each(function () {
                    if ($(this).attr('option-label') == color) {
                        $(this).click();
                    }
                });
                clearInterval(checkExist);
            }
        }, 2000);
    }
    var checkExist1 = setInterval(function () {
        if ($('.swatch-attribute').length) {
            $('.swatch-attribute').each(function () {
                var attr = $(this).attr('attribute-code');
                if (attr == 'configuratble_width') {
                    if ($('.' + attr).find('.swatch-option').length > 8) {
                        $('.' + attr).css('max-height', '135px');
                    }
                }
                if (attr == 'color') {
                    if ($('.' + attr).find('.swatch-option').length > 14) {
                        $('.swatch-attribute.' + attr).css('height', '115px');
                    }
                }
            });
            clearInterval(checkExist1);
        }
    }, 100);
    
    $(document).on('change', '#shipping-options-details-list-wrapper input, #shipping-options-details-list-wrapper select', function () {
        var flag = 1;
        $('#shipping-options-details-list-wrapper input, #shipping-options-details-list-wrapper select').each(function () {
            if ($.trim($(this).val()).length == 0) {
                flag = 0;
            }
        });
        /*if (flag == 1)
        {
            $('input#s_method_shippingoptions_shippingoptions').addClass('ajaxcall');
            $('input#s_method_shippingoptions_shippingoptions').prop("checked", true).trigger('click');
        }*/
    });

$(document).on('click', '.product.info.detailed .data.item.title', function (e) {
    e.stopImmediatePropagation();
    var controls = $(this).attr('aria-controls');
    if ($(window).width() < 768)
    {
        $('.data.item.content').each(function () {
            var id = $(this).attr('id');
            var val = $(this).attr('aria-hidden');
            if (val === "false")
            {
                $(this).css('display', 'block !important');
            }
        });
    }
});
$(document).on('keyup', 'input[name="nrml_qty"]', function (e) {
    var qty = $(this).val();
    qty = qty * $(this).parent().find('input[name="measurementsoldin"]').val()
    $(this).parent().find('input[title="Qty"]').val(qty);
});

$(document).scroll(function () {
//        console.log($(this).scrollTop());
if ($(this).scrollTop() > 360) {
    $('#checkout-review-submit').css('position', 'fixed');
            //$('#checkout-review-submit').css('right', '12%');
            $("#checkout-review-submit").addClass("checkout-review-submit");
        } else {
            $('#checkout-review-submit').removeAttr('style');
            $("#checkout-review-submit").removeClass("checkout-review-submit");
        }
        if ($('.cms-specialty-products').length) {
            var size = $('.cms-specialty-products .pull-right').position().top + $('.cms-specialty-products .pull-right').height() - $('.cms-specialty-products .three.columns.pull-left').height();
            console.log($(this).scrollTop());
            if (($(this).scrollTop() > $('.cms-specialty-products .pull-right').position().top) && ($(this).scrollTop() < size)) {
                $('.cms-specialty-products .three.columns.pull-left').removeAttr('style');
                $('.cms-specialty-products .three.columns.pull-left').css('position', 'fixed');
                $('.cms-specialty-products .three.columns.pull-left').css('top', '0');
                $('.cms-specialty-products .three.columns.pull-left').css('width', '370px');
            } else if ($(this).scrollTop() < $('.cms-specialty-products .pull-right').position().top) {
                $('.cms-specialty-products .three.columns.pull-left').removeAttr('style');
            } else if ($(this).scrollTop()) {
                $('.cms-specialty-products .three.columns.pull-left').removeAttr('style');
                $('.cms-specialty-products .three.columns.pull-left').css('position', 'sticky');
                $('.cms-specialty-products .three.columns.pull-left').css('top', 0);
            }
        }
    });

    $(document).on('change', '#s_method_shippingoptions', function () {
        var shipping_zipcode = $("input[name=postcode]").val()?$("input[name=postcode]").val():null;
        var selected_shipping = $('.shipping-address-item').hasClass('selected-item')?'true':'false';
        if($("#s_method_shippingoptions").prop("checked") && (shipping_zipcode!=null || selected_shipping=='true')){
            $('#s_method_shippingoptions').show();
            $('#shipping-options-details-list-wrapper').show();
            $('.col-price').show();
            $('[name="shipping_option_field[shipping_options_method]"]').trigger('change');
            if(!$('[name="shipping_option_field[shipping_options_service]"]').val()){    
                
                if(window.checkoutConfig.quoteData.shipping_options_method == $('[name="shipping_option_field[shipping_options_method]"]').val()){
                   console.log(window.checkoutConfig.quoteData.shipping_options_service);
                    if(window.checkoutConfig.quoteData.shipping_options_service){
                        var shippingOptionsService =  window.checkoutConfig.quoteData.shipping_options_service;
                        $('select[name^="shipping_option_field[shipping_options_service]"] option[value="'+shippingOptionsService+'"]').attr("selected","selected");
                    }
                } else {
                    $('[name="shipping_option_field[shipping_options_method]"]').trigger('change');
                
                } 
            }    
        }
    });

    $(document).on('change', '.shipping-address-item', function(){
        console.log($(this).hasClass('selected-item'));

     });

    $(document).on('click', '.catalogsearch-result-index .tocart', function () {
        var cnt = setInterval(function () {
            if ($('.catalogsearch-result-index .admin__control-radio').length) {
                $('.product-options-bottom').siblings('.field.required').hide();
                clearInterval(cnt);

            }

        }, 100);
    });


    $(document).on('click', 'button.action.tocart.primary', function () {

        if ($('.catalogsearch-result-index').length) {
            var sold = setInterval(function () {
                if ($('div#mb-ajaxsuite-popup-wrapper').css('display') == 'block') {
                    var proDesc = $('.catalogsearch-result-index #mb-ajaxsuite-popup .product-desc .product-data:nth-child(2)').text();
                    numb = proDesc.match(/\d/g);
                    var numb = numb.join("");
                    var num = parseInt(numb, 10);
                    var n = num.toFixed(2);

                    console.log(num);
                    var te = $('.catalogsearch-result-index .product-options-wrapper [data-price-type="finalPrice"]').find('span.price').text().split('$')[1];
                    var tot = n * te;
                    var total = tot.toFixed(2);
                    $('.catalogsearch-result-index [data-price-type="finalPrice"]').attr('data-price-amount', tot);
                    var priceAmount = $('.catalogsearch-result-index [data-price-type="finalPrice"]').attr('data-price-amount');

                    $('.catalogsearch-result-index .product-options-wrapper [data-price-type="finalPrice"]').find('span.price').text('$' + total);

                    // if($('.clear').length){
                        if ($('.catalogsearch-result-index .qty-buttons-wrapper').length === 0) {
                            $('.catalogsearch-result-index .product-add-form .qty .control').append('<div class="qty-buttons-wrapper"><div class="qty-button increase"></div><div class="qty-button decrease"></div></div>');

                        }
                        clearInterval(sold);
                    }
                }, 5000);
        }

        setInterval(function () {
            if ($('.cms-price-sheet .qty-buttons-wrapper').length === 0) {
                $('.cms-price-sheet .product-add-form .qty .control').append('<div class="qty-buttons-wrapper"><div class="qty-button increase"></div><div class="qty-button decrease"></div></div>');
            }
        }, 2000);
        
    });
    $(document).ajaxComplete(function () {
        var j=0;
        var tr1 = setInterval(function () {
            if($('.catalogsearch-result-index .admin__control-radio').length){
                $('.catalogsearch-result-index .admin__control-radio').each(function () {
                    $(this).siblings().text().trim();
                    if ($(this).siblings().text() == 'No ') {
                        $(this).prop('checked', true);

                        $(this).parent('.fieldset').addClass('cntrl');
                        if ($('.cntrl').find('.field.required').length) {
                            var i=0;

                            var tr = setInterval(function () {

                                $('.cntrl').find('.field.required').hide();
                                if(i==1){
                                    clearInterval(tr);
                                }
                                i++;
                            },1000);

                        }

                    }
                });



            }
            if(j==1){
                clearInterval(tr1);
            }
            j++;
        },1000);

    });

    $(document).on('change', '.catalogsearch-result-index .admin__control-radio', function () {
            // $(this).prop('checked',false);
            var spanPrice = $(this).parents().siblings('.price-final_price').find('.price-wrapper').first().attr('data-price-amount');
            var valPrice = $(this).parents().siblings('.product-options-bottom').find('input').val();
            var fPrice = spanPrice * valPrice;
            $(this).parents().siblings('.price-final_price').find("span.price").text('$' + fPrice);

            if ($(this).siblings().text() == 'Yes ') {

                // $(this).prop('checked',true);
                $(this).parents().addClass('cntrl');
                $(this).parents().siblings('.field.required').show();


            } else if ($(this).siblings().text() == 'No ') {
                $(this).prop('checked', true);
                // $(this).addClass('cmmm');
                // $(this).parents().removeClass('cntrl');
                if ($('.cntrl').siblings('.field.required').length) {
                    $('.cntrl').siblings('.field.required').hide();
                }


            } else if ($(this).siblings().text() == 'None') {
                $(this).prop('checked', false);

            }


        });

    var attMin = setInterval(function () {
        $('.catalogsearch-result-index .product-add-form .qty input').attr('min', 1);
        if ($('.catalogsearch-result-index .product-add-form .qty input').attr('min') == 1) {
            clearInterval(attMin);
        }
    }, 2000);
    $(document).on('keyup', '.catalogsearch-result-index .product-add-form .qty input', function () {
        $(this).addClass('children');
        var orgVal = $(this).val();
        var amount = $('[data-price-type="finalPrice"]').attr('data-price-amount');
        total = orgVal * amount;
        $(this).parents().siblings('.price-box').find('[data-price-type="finalPrice"]').attr('data-price-amount', total);
        $(this).parents().siblings('.price-box').find('[data-price-type="finalPrice"]').find('.price').text(total);
    });

    // $(document)
    $(document).on('click', '.catalogsearch-result-index .qty-button', function (e) {
        e.preventDefault();
        var value = $('.catalogsearch-result-index .input-text.qty').val();
        // var min=1;
        var min = $('.catalogsearch-result-index .input-text.qty').attr('min');
        if ($(this).hasClass('increase')) {
            // var min=1;

            value = parseInt(value, 10);
            value = value + 1;
            if (value < 1) {
                $('.catalogsearch-result-index .input-text.qty').attr('value', '1');
            } else {
                min = parseInt(min, 10);
                min = min + 1;
                //       $('.catalogsearch-result-index .input-text.qty').attr('min',min);
                // $('.catalogsearch-result-index .input-text.qty').attr('value',min);
                $('.catalogsearch-result-index .input-text.qty').attr('value', value);
                var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('finalprice');
                if (typeof price == typeof undefined) {
                    var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('data-price-amount');
                    var finalprice = value * price;
                    if (value > 0) {
                        finalprice = priceUtils.formatPrice(finalprice);
                        $('.catalogsearch-result-index .price-final_price .price-wrapper .price').text(finalprice);
                    }
                }
            }
        } else {
            value = parseInt(value, 10);
            value = value - 1;
            // var min= 0;
            if (value < 1) {
                $('.catalogsearch-result-index .input-text.qty').attr('value', '1');
            } else {
                min = parseInt(min, 10);
                min = min - 1;
                //       $('.catalogsearch-result-index .input-text.qty').attr('min',min);

                // $('.catalogsearch-result-index .input-text.qty').attr('value',min);
                $('.catalogsearch-result-index .input-text.qty').attr('value', value);
                var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('finalprice');
                if (typeof price == typeof undefined) {
                    var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('data-price-amount');
                    var finalprice = value * price;
                    if (value > 0) {
                        finalprice = priceUtils.formatPrice(finalprice);
                        $('.catalogsearch-result-index .price-final_price .price-wrapper .price').text(finalprice);
                    }

                }

            }
        }
        // $('.catalogsearch-result-index .input-text.qty').val(value);

        // var value = $('.catalogsearch-result-index .input-text.qty').val();
        // var min = parseInt($('.catalogsearch-result-index .input-text.qty').attr('min'));
        // if (value < 1)
        //     $('.catalogsearch-result-index .input-text.qty').val("1");
        // else if (value < min)
        //     $('.catalogsearch-result-index .input-text.qty').val(min);
        // $('.catalogsearch-result-index .page-product-configurable #product-addtocart-button').prop('disabled', false);
        // var qty = parseInt($('input[name="qty"]').val());
        // var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('finalprice');
        // if (typeof price == typeof undefined) {
        //     var price = $('.catalogsearch-result-index .price-final_price .price-wrapper').attr('data-price-amount');
        // }
        // var finalprice = qty * price;
        // if (qty > 0) {
        //     finalprice = priceUtils.formatPrice(finalprice);
        //     $('.catalogsearch-result-index .price-final_price .price-wrapper .price').text(finalprice);
        // }
    });




    if (window.location.pathname == "/contact/" || window.location.pathname == "/contact") {
        var checkelemt = setInterval(function () {
            if ($('#department_id').length) {
                $('#department_id').remove();
                // clearInterval(checkelemt);
            }
        }, 1000)
    }
    $(document).on('click', '#product-addtocart-button', function () {
        var thisevent = $(this);
        var attr = $(thisevent).parents('.product-add-form').find('.swatch-attribute');
        var result = [];
        var flag = 0;
        $(attr).each(function () {
            var val = $(this).find('.swatch-attribute-selected-option').text();
            if (val == "")
            {
                result.push($(this).find('.swatch-attribute-label').text());
                flag = 1;
            }
        });
        if (flag == 1) {
            $('.overlay.pdp_attribute').css('display', 'block');
            $('.required_options').text(result.join(", "));
        }
        if (flag == 0) {
            $(thisevent).css('background-color', '#f7272b');
        }
    });
    $(document).on('click', '.swatch-option', function () {
        var thisevent = $(this);
        var attr = $(thisevent).parents('.product-add-form').find('.swatch-attribute');
        var flag = 0;
        $(attr).each(function () {
            var val = $(this).find('.swatch-attribute-selected-option').text();
            if (val == "")
            {
                flag = 1;
            }
        });
        if (flag == 0) {
            $(thisevent).parents('.product-add-form').find('#product-addtocart-button').css('background-color', '#f7272b');
        }
    });
    $(document).on('click', '.pdp_attribute #close_popup', function () {
        $('.overlay.pdp_attribute').css('display', 'none');
    });
    $(document).on('click', '.pdp_attribute', function () {
        $('.overlay.pdp_attribute').css('display', 'none');
    });
    $(document).on('click', '.swatch-option', function () {
        $('.page-product-configurable #product-addtocart-button').prop('disabled', false);
    });
    $(document).ready(function () {
        $('.page-product-configurable #product-addtocart-button').prop('disabled', false);
        $('.page-product-configurable #product-addtocart-button').css('background-color', '#999');
    });

    /*===================== Start Select single attribute option by default ============================*/
    $(document).ajaxComplete(function () {
        $('.adhesive').each(function () { 
            if ($('.adhesive').find('.swatch-attribute')) { 
                var adhesive_script = $('.adhesive').find('.swatch-attribute-options .text');
                if (adhesive_script.length > 0) { 
                    $('.adhesive .text').addClass('adhesive');
                }
            }
        });
        $('.configuratble_width').each(function () { 
            if ($('.configuratble_width').find('.swatch-attribute')) {
                var adhesive_script = $('.configuratble_width').find('.swatch-attribute-options .text');
                if (adhesive_script.length > 0) {
                    $('.configuratble_width .text').addClass('configuratble_width');
                }
            }
        });
        $('.wide_loop_type').each(function () { 
            if ($('.wide_loop_type').find('.swatch-attribute')) { 
                var wideloop_script = $('.wide_loop_type').find('.swatch-attribute-options .text');
                if (wideloop_script.length > 0) { 
                    $('.wide_loop_type .text').addClass('wide_loop_type');
                }
            }
        });
    });
    
    $(document).on('click', '.swatch-attribute-options .swatch-option', function (e)    
    { 
        var clickedBtnClass = $(this).attr('class');
        var array_fragment = clickedBtnClass.split(' ');
        var poppedItem = array_fragment.pop();
        var selected_attr = array_fragment.pop();

        var attr_val = jQuery('.swatch-attribute.'+selected_attr).attr('option-selected');
        if (typeof attr_val !== typeof undefined && attr_val !== false) {           

            $('.swatch-opt .swatch-attribute').each(function (){ 
                var attributeCode = $(this).attr('attribute-code'); 
                var attributeCode_text = $(this).attr('attribute-code'); 

                if(attributeCode == 'configuratble_width'){
                   attributeCode_text = 'text.configuratble_width';
               }else if(attributeCode == 'adhesive'){
                attributeCode_text = 'text.adhesive';
            }else if(attributeCode == 'wide_loop_type'){
                attributeCode_text = 'text.wide_loop_type';
            }else if(attributeCode == 'hook_loop'){
                attributeCode = 'HookLoopCustomClass';
                attributeCode_text = 'text.HookLoopCustomClass';
            }
            var checkSelectedStatus = $('.swatch-attribute.'+attributeCode).attr('option-selected');
            if(attributeCode != selected_attr && (checkSelectedStatus == undefined || checkSelectedStatus == ''))
            {
                var countDisabledOptions = $('.swatch-option.'+attributeCode+'.disabled').length;                
                var countTotalOptions = $('.swatch-option.'+attributeCode).length;    

                if(countTotalOptions - countDisabledOptions == 1)
                {   
                    $('.swatch-attribute.'+attributeCode+' .swatch-attribute-options .swatch-option.'+attributeCode_text).each(function () {
                        var check_disabled_attr = $(this).attr('disabled');
                        if (check_disabled_attr == undefined || check_disabled_attr == '') 
                        { 
                            $(this).click();
                            $(this).addClass('selected');  
                            var selected_attr_id =$(this).attr('option-id');
                            $('.swatch-attribute.'+attributeCode).attr('option-selected',selected_attr_id);
                        }
                    });
                }
            }
        }); 
        }
    });    
    /*===================== End Select single attribute option by default ============================*/
    $("#productTabSection").click(function() {
        $('html, body').animate({
            scrollTop: $("#tab-label-description").offset().top
        }, 500);
    });

    $(document).ready(function () {
        $('.item.home a').html('<i class="fa fa-home"></i>');
        $('.item.0 a').html('<i class="fa fa-home"></i>');
    });

    $(document).ready(function () {
        $('.item.home a').html('<i class="fa fa-home"></i>');
        $('.item.0 a').html('<i class="fa fa-home"></i>');
        //$( "<div id='shopping-cart-link' class='viewcart-button'><a href='"+BASE_URL+"checkout/cart' class='big orange nice btn_thme_primary'>View Cart</a></div>" ).insertAfter( '#product_addtocart_form');

        setTimeout(function () {
           if($('#mb-ajaxcart-wrapper .search-product-popup .product-add-form .product-options-wrapper .fieldset .field .options-list .field.choice:first-child .product-custom-option').attr("checked") == "checked") { $('.product-options-wrapper .field.required').css('display','none'); }
       }, 1000);
    });

    // $(document)
    $(document).on('click', '.cms-price-sheet .qty-button', function (e) {
        e.preventDefault();
        var value = $('.cms-price-sheet .input-text.qty').val();
        // var min=1;
        var min = $('.cms-price-sheet .input-text.qty').attr('min');
        if ($(this).hasClass('increase')) {
            // var min=1;

            value = parseInt(value, 10);
            value = value + 1;
            if (value < 1) {
                $('.cms-price-sheet .input-text.qty').attr('value', '1');
            } else {
                min = parseInt(min, 10);
                min = min + 1;
                $('.cms-price-sheet .input-text.qty').attr('value', value);
                var price = $('.cms-price-sheet .price-final_price .price-wrapper').attr('finalprice');
                if (typeof price == typeof undefined) {
                    var price = $('.cms-price-sheet .price-final_price .price-wrapper').attr('data-price-amount');
                    var finalprice = value * price;
                    if (value > 0) {
                        finalprice = priceUtils.formatPrice(finalprice);
                        $('.cms-price-sheet .price-final_price .price-wrapper .price').text(finalprice);
                    }
                }
            }
        } else {
            value = parseInt(value, 10);
            value = value - 1;
            // var min= 0;
            if (value < 1) {
                $('.cms-price-sheet .input-text.qty').attr('value', '1');
            } else {
                min = parseInt(min, 10);
                min = min - 1;
                $('.cms-price-sheet .input-text.qty').attr('value', value);
                var price = $('.cms-price-sheet .price-final_price .price-wrapper').attr('finalprice');
                if (typeof price == typeof undefined) {
                    var price = $('.cms-price-sheet .price-final_price .price-wrapper').attr('data-price-amount');
                    var finalprice = value * price;
                    if (value > 0) {
                        finalprice = priceUtils.formatPrice(finalprice);
                        $('.cms-price-sheet .price-final_price .price-wrapper .price').text(finalprice);
                    }

                }

            }
        }
    });

    /*--------Homepage read more---------*/

    $(document).on('click', '.home_content .read-button a.read-more', function () {
        $('.home-content-bottom .content-bottom').css({'height':'auto', 'overflow':'visible'});
        $('.home_content .read-button').insertAfter('.columns.content-bottom');
        $(".read-button .read-more").text('Read less');
        $(".read-button a").removeClass('read-more');
        $(".read-button a").addClass('read-less');
    });

    $(document).on('click', '.home_content .read-button a.read-less', function () {
        $('.home-content-bottom .content-bottom').css({'height':'0px', 'overflow':'hidden'});
        $(".read-button .read-less").text('Read more');
        $(".read-button a").removeClass('read-less');
        $(".read-button a").addClass('read-more');
    });

    /*------------end Homepage read less--------*/   
    
    /*-----------Covid mitigation--------*/
    $(document).ready(function () {
        $(".tracker-post:first").css('display','block');
        var updatedprice = setInterval(function () {
            $('.qty-button').click();
        }, 1500);
    });

    $(document).on('click', '.tracker-readmore', function (e) {
      var notVisible = 0;
      var numItems = $('.tracker-post').length;
      $('.tracker-post').each(function() {
        if ($(this).css('display') !== 'none') {
          notVisible = notVisible + 1;
          if(notVisible + 3 == numItems){
            $('.tracker-readmore').remove();
        }
    }
});
      $(".tracker-post:eq( "+(notVisible+0)+" )").show();
      $(".tracker-post:eq( "+(notVisible+1)+" )").show();
      $(".tracker-post:eq( "+(notVisible+2)+" )").show();
  });  
    /*-----------end Covid mitigation--------*/


});
